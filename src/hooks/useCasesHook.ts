import { AdvantageService } from "../api/advantage/advantage"
import { Auth } from "../api/auth/auth"
import { KvizService } from "../api/kviz/kviz"
import { PlanService } from "../api/plan/plan"
import { ProjectService } from "../api/project/project"
import { SiteService } from "../api/site/site"
import { AuthorizationUseCases } from "../useCase/authorization/authorization"
import { KvizUseCases } from "../useCase/kviz/kviz"
import { PlanUseCases } from "../useCase/plan/plan"
import { AdvantageUseCases } from "../useCase/advantage/advantage"
import { ProjectUseCases } from "../useCase/project/project"
import { SiteUseCases } from "../useCase/site/site"
import { PhotoUseCases } from "../useCase/photo/photo"
import { PhotosService } from "../api/photos/photos"
import { IntegrationUseCases } from "../useCase/integration/integration"
import { IntegrationService } from "../api/integration/integration"
import { LeadUseCases } from "../useCase/lead/lead"
import { LeadService } from "../api/lead"


export const useUseCases = (actions:any) => {
    const authUseCase = new AuthorizationUseCases(new Auth(), actions)
    const projectUseCase = new ProjectUseCases(new ProjectService(), actions)
    const siteUseCase = new SiteUseCases(new SiteService(), actions)
    const kvizUseCase = new KvizUseCases(new KvizService(), actions)
    const planUseCase = new PlanUseCases(new PlanService(), actions)
    const advantageUseCase = new AdvantageUseCases(new AdvantageService(), actions)
    const photosUseCase = new PhotoUseCases(new PhotosService(), actions)
    const integrationUseCase = new IntegrationUseCases(new IntegrationService(), actions)
    const leadUseCase = new LeadUseCases(new LeadService(), actions)

    return {authUseCase, projectUseCase, siteUseCase, kvizUseCase, planUseCase, advantageUseCase, photosUseCase, integrationUseCase, leadUseCase}
}
