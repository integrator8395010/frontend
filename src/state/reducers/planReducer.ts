import { Plan } from "../../domain/plan/plan";
import { PlanActionTypes } from "../action-types";
import { PlanActions } from "../actions";
import produce from 'immer';


interface PlanState {
    loading: boolean | null;
    plans: Plan[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: PlanState = {
    loading: null,
    plans: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: PlanState = initialState, action: PlanActions) => {
    switch (action.type) {
        case PlanActionTypes.PLAN_REQUEST_SEND:
            state.loading = true;
            return state;
        case PlanActionTypes.PLAN_SUCCESS:
            state.loading = false;
            state.plans = action.payload;
            return state;
        case PlanActionTypes.PLAN_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case PlanActionTypes.PLAN_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case PlanActionTypes.PLAN_CREATE_SUCCESS:
            state.entityLoading = false
            state.plans!.push(action.payload)
            state.entityError = ""
            return state;
        case PlanActionTypes.PLAN_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case PlanActionTypes.PLAN_DELETE_SUCCESS:
            console.log(state.plans!)
            state.plans = state.plans!.filter((plans)=>plans.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
