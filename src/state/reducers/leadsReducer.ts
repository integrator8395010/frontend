import { Lead } from "../../domain/lead";
import { LeadActionTypes } from "../action-types";
import { LeadActions } from "../actions";
import produce from 'immer';


interface LeadsState {
    loading: boolean | null;
    leads: Lead[] | null,
    page: number,
    error: string | null,
}

const initialState: LeadsState = {
    loading: null,
    leads: null,
    page: 1,
    error: null,
}



const reducer = produce((state: LeadsState = initialState, action: LeadActions) => {
    switch (action.type) {
        case LeadActionTypes.LEAD_REQUEST_SEND:
            state.loading = true;
            state.page = 1
            state.leads = []
            return state;
        case LeadActionTypes.LEAD_SUCCESS:
            state.loading = false;
            state.leads = action.payload;
            return state;
        case LeadActionTypes.LEAD_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case LeadActionTypes.LEAD_REQUEST_NEW_PAGE:
            state.loading = true
            state.error = ""
            return state;
        case LeadActionTypes.LEAD_NEW_PAGE_SUCCESS:
            state.loading = false
            state.leads?.push(...action.payload.leads)
            state.page = action.payload.page
            state.error = ""
            return state;
        case LeadActionTypes.LEAD_NEW_PAGE_ERROR:
            state.loading = false
            state.error = action.payload;
            return state;
        default:
            return state;
    }
})

export default reducer;
