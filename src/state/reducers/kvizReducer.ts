import { Kviz } from "../../domain/kviz/kviz";
import { KvizActionTypes } from "../action-types";
import { KvizActions } from "../actions";
import produce from 'immer';


interface KvizState {
    loading: boolean | null;
    kviz: Kviz[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: KvizState = {
    loading: null,
    kviz: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: KvizState = initialState, action: KvizActions) => {
    switch (action.type) {
        case KvizActionTypes.KVIZ_REQUEST_SEND:
            state.loading = true;
            return state;
        case KvizActionTypes.KVIZ_SUCCESS:
            state.loading = false;
            state.kviz = action.payload;
            return state;
        case KvizActionTypes.KVIZ_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case KvizActionTypes.KVIZ_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case KvizActionTypes.KVIZ_CREATE_SUCCESS:
            state.entityLoading = false
            state.kviz!.push(action.payload)
            state.entityError = ""
            return state;
        case KvizActionTypes.KVIZ_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case KvizActionTypes.KVIZ_DELETE_SUCCESS:
            state.kviz = state.kviz!.filter((kviz)=>kviz.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
