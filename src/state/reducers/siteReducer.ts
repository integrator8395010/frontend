import { Site } from "../../domain/site/site";
import { SiteActionTypes } from "../action-types";
import { SiteActions } from "../actions";
import produce from 'immer';


interface SiteState {
    loading: boolean | null;
    sites: Site[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: SiteState = {
    loading: null,
    sites: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: SiteState = initialState, action: SiteActions) => {
    switch (action.type) {
        case SiteActionTypes.SITE_REQUEST_SEND:
            state.loading = true;
            return state;
        case SiteActionTypes.SITE_SUCCESS:
            state.loading = false;
            state.sites = action.payload;
            return state;
        case SiteActionTypes.SITE_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case SiteActionTypes.SITE_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case SiteActionTypes.SITE_CREATE_SUCCESS:
            state.entityLoading = false
            state.sites!.push(action.payload)
            state.entityError = ""
            return state;
        case SiteActionTypes.SITE_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case SiteActionTypes.SITE_DELETE_SUCCESS:
            state.sites = state.sites!.filter((sites)=>sites.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
