import { IntegrationActionTypes } from "../action-types";
import { IntegrationActions, PlanActions } from "../actions";
import produce from 'immer';
import { AmocrmIntegration } from "../../domain/integration/amocrm-integration";
import { BitrixIntegration } from "../../domain/integration/bitrix-integration";
import { MailIntegration } from "../../domain/integration/mail-integration";
import { Site } from "../../domain/site/site";
import { ProjectAmocrmIntegration } from "../../domain/integration/project-amocrm-integration";
import { ProjectBitrixIntegration } from "../../domain/integration/project-bitrix-integration";
import { LeadactivIntegration } from "../../domain/integration/leadactiv-integration";
import { LeadactivAmoSecrets } from "../../domain/integration/leadactiv-amo-secrets";


interface IntegrationState {
    loading: boolean | null;
    amocrmIntegration: AmocrmIntegration | null,
    bitrixIntegration: BitrixIntegration | null,
    mailIntegration: MailIntegration | null,
    leadactivIntegration: LeadactivIntegration | null,
    leadactivAmoSecrets: LeadactivAmoSecrets | null;
    projectIntegration: ProjectAmocrmIntegration | ProjectBitrixIntegration | null,
    sitesWithIntegrations: Site[] | null,
    sitesWithoutIntegrations: Site[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: IntegrationState = {
    loading: null,
    amocrmIntegration: null,
    bitrixIntegration: null,
    mailIntegration: null,
    leadactivIntegration: null,
    projectIntegration: null,
    leadactivAmoSecrets: null,
    sitesWithIntegrations: null,
    sitesWithoutIntegrations: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: IntegrationState = initialState, action: IntegrationActions) => {
    switch (action.type) {
        case IntegrationActionTypes.AMOCRM_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.AMOCRM_SUCCESS:
            state.loading = false;
            state.amocrmIntegration = action.payload;
            return state;
        case IntegrationActionTypes.AMOCRM_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case IntegrationActionTypes.AMOCRM_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.AMOCRM_CREATE_SUCCESS:
            state.entityLoading = false
            state.amocrmIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.AMOCRM_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.AMOCRM_DELETE_SUCCESS:
            state.amocrmIntegration = null
            return state;

        case IntegrationActionTypes.BITRIX_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.BITRIX_SUCCESS:
            state.loading = false;
            state.bitrixIntegration = action.payload;
            return state;
        case IntegrationActionTypes.BITRIX_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case IntegrationActionTypes.BITRIX_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.BITRIX_CREATE_SUCCESS:
            state.entityLoading = false
            state.bitrixIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.BITRIX_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.BITRIX_DELETE_SUCCESS:
            state.bitrixIntegration = null
            return state;


        case IntegrationActionTypes.MAIL_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.MAIL_SUCCESS:
            state.loading = false;
            state.mailIntegration = action.payload;
            return state;
        case IntegrationActionTypes.MAIL_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case IntegrationActionTypes.MAIL_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.MAIL_CREATE_SUCCESS:
            state.entityLoading = false
            state.mailIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.MAIL_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.MAIL_UPDATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.MAIL_UPDATE_SUCCESS:
            state.entityLoading = false
            state.mailIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.MAIL_UPDATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.MAIL_DELETE_SUCCESS:
            state.mailIntegration = null
            return state;

        case IntegrationActionTypes.SITE_WITH_INTEGRATION_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.SITE_WITH_INTEGRATION_SUCCESS:
            state.loading = false;
            state.sitesWithIntegrations = action.payload;
            return state;
        case IntegrationActionTypes.SITE_WITH_INTEGRATION_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;

        case IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_SUCCESS:
            state.loading = false;
            state.sitesWithoutIntegrations = action.payload;
            return state;
        case IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;

        case IntegrationActionTypes.INTEGRATIONS_OF_SITE_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.INTEGRATIONS_OF_SITE_SUCCESS:
            state.loading = false;
            state.bitrixIntegration = action.payload.bitrix;
            state.amocrmIntegration = action.payload.amocrm;
            state.mailIntegration = action.payload.mail;
            state.leadactivIntegration = action.payload.leadactiv;
            return state;
        case IntegrationActionTypes.INTEGRATIONS_OF_SITE_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;

        case IntegrationActionTypes.PROJECT_INTEGRATION_REQUEST_SEND:
            state.entityLoading = true;
            state.projectIntegration = null;
            return state;
        case IntegrationActionTypes.PROJECT_INTEGRATION_SUCCESS:
            state.entityLoading = false;
            state.projectIntegration = action.payload;
            return state;
        case IntegrationActionTypes.PROJECT_INTEGRATION_ERROR:
            state.entityLoading = false;
            state.entityError = action.payload;
            return state;

        case IntegrationActionTypes.LEADACTIV_REQUEST_SEND:
            state.loading = true;
            return state;
        case IntegrationActionTypes.LEADACTIV_SUCCESS:
            state.loading = false;
            state.leadactivIntegration = action.payload;
            return state;
        case IntegrationActionTypes.LEADACTIV_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case IntegrationActionTypes.LEADACTIV_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.LEADACTIV_CREATE_SUCCESS:
            state.entityLoading = false
            state.leadactivIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.LEADACTIV_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.LEADACTIV_UPDATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case IntegrationActionTypes.LEADACTIV_UPDATE_SUCCESS:
            state.entityLoading = false
            state.leadactivIntegration = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.LEADACTIV_UPDATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case IntegrationActionTypes.LEADACTIV_DELETE_SUCCESS:
            state.leadactivIntegration = null
            return state;

        case IntegrationActionTypes.LEADACTIV_SECRETS_REQUEST:
            state.entityLoading = true
            return state;
        case IntegrationActionTypes.LEADACTIV_SECRETS_SUCCESS:
            state.entityLoading = false
            state.leadactivAmoSecrets = action.payload
            state.entityError = ""
            return state;
        case IntegrationActionTypes.LEADACTIV_SECRETS_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;


        default:
            return state;
    }
})

export default reducer;
