import { Project } from "../../domain/project/project";
import { ProjectActionTypes } from "../action-types";
import { ProjectActions } from "../actions";
import produce from 'immer';


interface ProjectState {
    loading: boolean | null;
    project: Project[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: ProjectState = {
    loading: null,
    project: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: ProjectState = initialState, action: ProjectActions) => {
    switch (action.type) {
        case ProjectActionTypes.PROJECT_REQUEST_SEND:
            state.loading = true;
            return state;
        case ProjectActionTypes.PROJECT_SUCCESS:
            state.loading = false;
            state.project = action.payload;
            return state;
        case ProjectActionTypes.PROJECT_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case ProjectActionTypes.PROJECT_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case ProjectActionTypes.PROJECT_CREATE_SUCCESS:
            state.entityLoading = false
            state.project!.push(action.payload)
            state.entityError = ""
            return state;
        case ProjectActionTypes.PROJECT_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case ProjectActionTypes.PROJECT_DELETE_SUCCESS:
            state.project = state.project!.filter((project)=>project.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
