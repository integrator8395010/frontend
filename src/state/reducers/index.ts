import { combineReducers } from "redux";
import loginReducer from './loginReducer';
import projectsReducer from './projectReducer';
import sitesReducer from './siteReducer';
import kvizReducer from './kvizReducer';
import planReducer from './planReducer';
import advantageReducer from './advantageReducer';
import photoReducer from './photoReducer';
import integrationReducer from './integrationReducer';
import leadsReducer from './leadsReducer';

const reducers = combineReducers({
    login: loginReducer,
    projects: projectsReducer,
    sites: sitesReducer,
    kvizes: kvizReducer,
    plans: planReducer,
    advantages: advantageReducer,
    photos: photoReducer,
    integrations: integrationReducer,
    leads: leadsReducer,
})

export default reducers;
export type RootState = ReturnType<typeof reducers>;