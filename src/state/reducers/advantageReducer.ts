import { Advantage } from "../../domain/advantage/advantage";
import { AdvantageActionTypes } from "../action-types";
import { AdvantageActions } from "../actions";
import produce from 'immer';


interface AdvantageState {
    loading: boolean | null;
    advantages: Advantage[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: AdvantageState = {
    loading: null,
    advantages: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: AdvantageState = initialState, action: AdvantageActions) => {
    switch (action.type) {
        case AdvantageActionTypes.ADVANTAGE_REQUEST_SEND:
            state.loading = true;
            return state;
        case AdvantageActionTypes.ADVANTAGE_SUCCESS:
            state.loading = false;
            state.advantages = action.payload;
            return state;
        case AdvantageActionTypes.ADVANTAGE_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case AdvantageActionTypes.ADVANTAGE_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case AdvantageActionTypes.ADVANTAGE_CREATE_SUCCESS:
            state.entityLoading = false
            state.advantages!.push(action.payload)
            state.entityError = ""
            return state;
        case AdvantageActionTypes.ADVANTAGE_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case AdvantageActionTypes.ADVANTAGE_DELETE_SUCCESS:
            state.advantages = state.advantages!.filter((advantages)=>advantages.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
