import { Photo } from "../../domain/photo/photo";
import { PhotoActionTypes } from "../action-types";
import { PhotoActions } from "../actions";
import produce from 'immer';


interface PhotoState {
    loading: boolean | null;
    photos: Photo[] | null,
    error: string | null,
    entityLoading: boolean,
    entityError: string | null,
}

const initialState: PhotoState = {
    loading: null,
    photos: null,
    error: null,
    entityLoading: false,
    entityError: null,
}



const reducer = produce((state: PhotoState = initialState, action: PhotoActions) => {
    switch (action.type) {
        case PhotoActionTypes.PHOTO_REQUEST_SEND:
            state.loading = true;
            return state;
        case PhotoActionTypes.PHOTO_SUCCESS:
            state.loading = false;
            state.photos = action.payload;
            return state;
        case PhotoActionTypes.PHOTO_ERROR:
            state.loading = false;
            state.error = action.payload;
            return state;
        case PhotoActionTypes.PHOTO_CREATE_REQUEST:
            state.entityLoading = true
            state.entityError = ""
            return state;
        case PhotoActionTypes.PHOTO_CREATE_SUCCESS:
            state.entityLoading = false
            state.photos!.push(action.payload)
            state.entityError = ""
            return state;
        case PhotoActionTypes.PHOTO_CREATE_ERROR:
            state.entityLoading = false
            state.entityError = action.payload;
            return state;
        case PhotoActionTypes.PHOTO_DELETE_SUCCESS:
            state.photos = state.photos!.filter((photos)=>photos.Id() !== action.payload)
            return state;
        default:
            return state;
    }
})

export default reducer;
