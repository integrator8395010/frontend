export enum SiteActionTypes {
    SITE_REQUEST_SEND = 'site_request_send',
    SITE_ERROR = 'site_error',
    SITE_SUCCESS = 'site_success',
    
    SITE_CREATE_REQUEST = 'site_create_request',
    SITE_CREATE_SUCCESS = 'site_create_success',
    SITE_CREATE_ERROR = 'site_create_error',

    SITE_DELETE_REQUEST = 'site_delete_request',
    SITE_DELETE_SUCCESS = 'site_delete_success',
    SITE_DELETE_ERROR = 'site_delete_error',

    SITE_UPDATE_REQUEST = 'site_update_request',
    SITE_UPDATE_SUCCESS = 'site_update_success',
    SITE_UPDATE_ERROR = 'site_update_error',
}
