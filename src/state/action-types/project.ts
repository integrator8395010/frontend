export enum ProjectActionTypes {
    PROJECT_REQUEST_SEND = 'project_request_send',
    PROJECT_ERROR = 'project_error',
    PROJECT_SUCCESS = 'project_success',
    
    PROJECT_CREATE_REQUEST = 'project_create_request',
    PROJECT_CREATE_SUCCESS = 'project_create_success',
    PROJECT_CREATE_ERROR = 'project_create_error',

    PROJECT_DELETE_REQUEST = 'project_delete_request',
    PROJECT_DELETE_SUCCESS = 'project_delete_success',
    PROJECT_DELETE_ERROR = 'project_delete_error',

    PROJECT_UPDATE_REQUEST = 'project_update_request',
    PROJECT_UPDATE_SUCCESS = 'project_update_success',
    PROJECT_UPDATE_ERROR = 'project_update_error',
}
