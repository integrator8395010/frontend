export enum AdvantageActionTypes {
    ADVANTAGE_REQUEST_SEND = 'advantage_request_send',
    ADVANTAGE_ERROR = 'advantage_error',
    ADVANTAGE_SUCCESS = 'advantage_success',
    
    ADVANTAGE_CREATE_REQUEST = 'advantage_create_request',
    ADVANTAGE_CREATE_SUCCESS = 'advantage_create_success',
    ADVANTAGE_CREATE_ERROR = 'advantage_create_error',

    ADVANTAGE_DELETE_REQUEST = 'advantage_delete_request',
    ADVANTAGE_DELETE_SUCCESS = 'advantage_delete_success',
    ADVANTAGE_DELETE_ERROR = 'advantage_delete_error',

    ADVANTAGE_UPDATE_REQUEST = 'advantage_update_request',
    ADVANTAGE_UPDATE_SUCCESS = 'advantage_update_success',
    ADVANTAGE_UPDATE_ERROR = 'advantage_update_error',
}
