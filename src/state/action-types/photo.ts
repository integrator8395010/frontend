export enum PhotoActionTypes {
    PHOTO_REQUEST_SEND = 'photo_request_send',
    PHOTO_ERROR = 'photo_error',
    PHOTO_SUCCESS = 'photo_success',
    
    PHOTO_CREATE_REQUEST = 'photo_create_request',
    PHOTO_CREATE_SUCCESS = 'photo_create_success',
    PHOTO_CREATE_ERROR = 'photo_create_error',

    PHOTO_DELETE_REQUEST = 'photo_delete_request',
    PHOTO_DELETE_SUCCESS = 'photo_delete_success',
    PHOTO_DELETE_ERROR = 'photo_delete_error',

    PHOTO_UPDATE_REQUEST = 'photo_update_request',
    PHOTO_UPDATE_SUCCESS = 'photo_update_success',
    PHOTO_UPDATE_ERROR = 'photo_update_error',
}
