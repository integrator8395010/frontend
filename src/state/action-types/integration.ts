export enum IntegrationActionTypes {
    AMOCRM_REQUEST_SEND = 'amocrm_request_send',
    AMOCRM_ERROR = 'amocrm_error',
    AMOCRM_SUCCESS = 'amocrm_success',
    
    AMOCRM_CREATE_REQUEST = 'amocrm_create_request',
    AMOCRM_CREATE_SUCCESS = 'amocrm_create_success',
    AMOCRM_CREATE_ERROR = 'amocrm_create_error',

    AMOCRM_DELETE_REQUEST = 'amocrm_delete_request',
    AMOCRM_DELETE_SUCCESS = 'amocrm_delete_success',
    AMOCRM_DELETE_ERROR = 'amocrm_delete_error',

    AMOCRM_UPDATE_REQUEST = 'amocrm_update_request',
    AMOCRM_UPDATE_SUCCESS = 'amocrm_update_success',
    AMOCRM_UPDATE_ERROR = 'amocrm_update_error',


    BITRIX_REQUEST_SEND = 'bitrix_request_send',
    BITRIX_ERROR = 'bitrix_error',
    BITRIX_SUCCESS = 'bitrix_success',
    
    BITRIX_CREATE_REQUEST = 'bitrix_create_request',
    BITRIX_CREATE_SUCCESS = 'bitrix_create_success',
    BITRIX_CREATE_ERROR = 'bitrix_create_error',

    BITRIX_DELETE_REQUEST = 'bitrix_delete_request',
    BITRIX_DELETE_SUCCESS = 'bitrix_delete_success',
    BITRIX_DELETE_ERROR = 'bitrix_delete_error',

    BITRIX_UPDATE_REQUEST = 'bitrix_update_request',
    BITRIX_UPDATE_SUCCESS = 'bitrix_update_success',
    BITRIX_UPDATE_ERROR = 'bitrix_update_error',


    MAIL_REQUEST_SEND = 'mail_request_send',
    MAIL_ERROR = 'mail_error',
    MAIL_SUCCESS = 'mail_success',
    
    MAIL_CREATE_REQUEST = 'mail_create_request',
    MAIL_CREATE_SUCCESS = 'mail_create_success',
    MAIL_CREATE_ERROR = 'mail_create_error',

    MAIL_DELETE_REQUEST = 'mail_delete_request',
    MAIL_DELETE_SUCCESS = 'mail_delete_success',
    MAIL_DELETE_ERROR = 'mail_delete_error',

    MAIL_UPDATE_REQUEST = 'mail_update_request',
    MAIL_UPDATE_SUCCESS = 'mail_update_success',
    MAIL_UPDATE_ERROR = 'mail_update_error',


    SITE_WITH_INTEGRATION_REQUEST_SEND = 'site_with_integration_request_send',
    SITE_WITH_INTEGRATION_ERROR = 'site_with_integration_error',
    SITE_WITH_INTEGRATION_SUCCESS = 'site_with_integration_success',

    SITE_WITHOUT_INTEGRATION_REQUEST_SEND = 'site_without_integration_request_send',
    SITE_WITHOUT_INTEGRATION_ERROR = 'site_without_integration_error',
    SITE_WITHOUT_INTEGRATION_SUCCESS = 'site_without_integration_success',

    PROJECT_INTEGRATION_REQUEST_SEND = 'project_integration_request_send',
    PROJECT_INTEGRATION_SUCCESS = 'project_integration_request_success',
    PROJECT_INTEGRATION_ERROR = 'project_integration_error',



    INTEGRATIONS_OF_SITE_REQUEST_SEND = 'integrations_of_site_request_send',
    INTEGRATIONS_OF_SITE_ERROR = 'integrations_of_site_error',
    INTEGRATIONS_OF_SITE_SUCCESS = 'integrations_of_site_success',

    LEADACTIV_REQUEST_SEND = 'leadactiv_request_send',
    LEADACTIV_ERROR = 'leadactiv_error',
    LEADACTIV_SUCCESS = 'leadactiv_success',
    
    LEADACTIV_CREATE_REQUEST = 'leadactiv_create_request',
    LEADACTIV_CREATE_SUCCESS = 'leadactiv_create_success',
    LEADACTIV_CREATE_ERROR = 'leadactiv_create_error',

    LEADACTIV_DELETE_REQUEST = 'leadactiv_delete_request',
    LEADACTIV_DELETE_SUCCESS = 'leadactiv_delete_success',
    LEADACTIV_DELETE_ERROR = 'leadactiv_delete_error',

    LEADACTIV_UPDATE_REQUEST = 'leadactiv_update_request',
    LEADACTIV_UPDATE_SUCCESS = 'leadactiv_update_success',
    LEADACTIV_UPDATE_ERROR = 'leadactiv_update_error',

    LEADACTIV_SECRETS_REQUEST = 'leadactiv_secrets_request',
    LEADACTIV_SECRETS_SUCCESS = 'leadactiv_secrets_success',
    LEADACTIV_SECRETS_ERROR = 'leadactiv_secrets_error',
}
