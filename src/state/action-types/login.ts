export enum LoginActionTypes {
    LOGIN_REQUEST_SEND = 'rlogin_request_send',
    LOGIN_ERROR = 'rlogin_error',
    LOGIN_SUCCESS = 'rlogin_success',
}