export enum PlanActionTypes {
    PLAN_REQUEST_SEND = 'plan_request_send',
    PLAN_ERROR = 'plan_error',
    PLAN_SUCCESS = 'plan_success',
    
    PLAN_CREATE_REQUEST = 'plan_create_request',
    PLAN_CREATE_SUCCESS = 'plan_create_success',
    PLAN_CREATE_ERROR = 'plan_create_error',

    PLAN_DELETE_REQUEST = 'plan_delete_request',
    PLAN_DELETE_SUCCESS = 'plan_delete_success',
    PLAN_DELETE_ERROR = 'plan_delete_error',

    PLAN_UPDATE_REQUEST = 'plan_update_request',
    PLAN_UPDATE_SUCCESS = 'plan_update_success',
    PLAN_UPDATE_ERROR = 'plan_update_error',
}
