export enum KvizActionTypes {
    KVIZ_REQUEST_SEND = 'kviz_request_send',
    KVIZ_ERROR = 'kviz_error',
    KVIZ_SUCCESS = 'kviz_success',
    
    KVIZ_CREATE_REQUEST = 'kviz_create_request',
    KVIZ_CREATE_SUCCESS = 'kviz_create_success',
    KVIZ_CREATE_ERROR = 'kviz_create_error',

    KVIZ_DELETE_REQUEST = 'kviz_delete_request',
    KVIZ_DELETE_SUCCESS = 'kviz_delete_success',
    KVIZ_DELETE_ERROR = 'kviz_delete_error',

    KVIZ_UPDATE_REQUEST = 'kviz_update_request',
    KVIZ_UPDATE_SUCCESS = 'kviz_update_success',
    KVIZ_UPDATE_ERROR = 'kviz_update_error',
}
