import { Site } from "../../domain/site/site"
import { SiteActionTypes } from "../action-types"

export interface SiteListRequestSend {
    type: SiteActionTypes.SITE_REQUEST_SEND,
}

export interface SiteListSuccess {
    type: SiteActionTypes.SITE_SUCCESS,
    payload: Site[]
}

export interface SiteListError {
    type: SiteActionTypes.SITE_ERROR,
    payload: string,
}

export interface SiteCreateRequest {
    type: SiteActionTypes.SITE_CREATE_REQUEST,
}

export interface SiteCreatSuccess {
    type: SiteActionTypes.SITE_CREATE_SUCCESS,
    payload: Site,
}

export interface SiteCreateError {
    type: SiteActionTypes.SITE_CREATE_ERROR,
    payload: string,
}

export interface SiteDeleteRequest {
    type: SiteActionTypes.SITE_DELETE_REQUEST,
}

export interface SiteDeleteSuccess {
    type: SiteActionTypes.SITE_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface SiteDeleteError {
    type: SiteActionTypes.SITE_DELETE_ERROR,
    payload: string,
}

export interface SiteUpdateRequest {
    type: SiteActionTypes.SITE_UPDATE_REQUEST,
}

export interface SiteUpdateSuccess {
    type: SiteActionTypes.SITE_UPDATE_SUCCESS,
    payload: Site,
}

export interface SiteUpdateError {
    type: SiteActionTypes.SITE_UPDATE_ERROR,
    payload: string,
}

export type SiteActions = 
 | SiteListRequestSend
 | SiteListSuccess
 | SiteListError
 | SiteCreateRequest
 | SiteCreatSuccess
 | SiteCreateError
 | SiteDeleteRequest
 | SiteDeleteSuccess
 | SiteDeleteError
 | SiteUpdateRequest
 | SiteUpdateSuccess
 | SiteUpdateError
