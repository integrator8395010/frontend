import { Project } from "../../domain/project/project"
import { ProjectActionTypes } from "../action-types"

export interface ProjectListRequestSend {
    type: ProjectActionTypes.PROJECT_REQUEST_SEND,
}

export interface ProjectListSuccess {
    type: ProjectActionTypes.PROJECT_SUCCESS,
    payload: Project[]
}

export interface ProjectListError {
    type: ProjectActionTypes.PROJECT_ERROR,
    payload: string,
}

export interface ProjectCreateRequest {
    type: ProjectActionTypes.PROJECT_CREATE_REQUEST,
}

export interface ProjectCreatSuccess {
    type: ProjectActionTypes.PROJECT_CREATE_SUCCESS,
    payload: Project,
}

export interface ProjectCreateError {
    type: ProjectActionTypes.PROJECT_CREATE_ERROR,
    payload: string,
}

export interface ProjectDeleteRequest {
    type: ProjectActionTypes.PROJECT_DELETE_REQUEST,
}

export interface ProjectDeleteSuccess {
    type: ProjectActionTypes.PROJECT_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface ProjectDeleteError {
    type: ProjectActionTypes.PROJECT_DELETE_ERROR,
    payload: string,
}

export interface ProjectUpdateRequest {
    type: ProjectActionTypes.PROJECT_UPDATE_REQUEST,
}

export interface ProjectUpdateSuccess {
    type: ProjectActionTypes.PROJECT_UPDATE_SUCCESS,
    payload: Project,
}

export interface ProjectUpdateError {
    type: ProjectActionTypes.PROJECT_UPDATE_ERROR,
    payload: string,
}

export type ProjectActions = 
 | ProjectListRequestSend
 | ProjectListSuccess
 | ProjectListError
 | ProjectCreateRequest
 | ProjectCreatSuccess
 | ProjectCreateError
 | ProjectDeleteRequest
 | ProjectDeleteSuccess
 | ProjectDeleteError
 | ProjectUpdateRequest
 | ProjectUpdateSuccess
 | ProjectUpdateError
