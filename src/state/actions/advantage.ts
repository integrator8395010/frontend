import { Advantage } from "../../domain/advantage/advantage"
import { AdvantageActionTypes } from "../action-types"

export interface AdvantageListRequestSend {
    type: AdvantageActionTypes.ADVANTAGE_REQUEST_SEND,
}

export interface AdvantageListSuccess {
    type: AdvantageActionTypes.ADVANTAGE_SUCCESS,
    payload: Advantage[]
}

export interface AdvantageListError {
    type: AdvantageActionTypes.ADVANTAGE_ERROR,
    payload: string,
}

export interface AdvantageCreateRequest {
    type: AdvantageActionTypes.ADVANTAGE_CREATE_REQUEST,
}

export interface AdvantageCreatSuccess {
    type: AdvantageActionTypes.ADVANTAGE_CREATE_SUCCESS,
    payload: Advantage,
}

export interface AdvantageCreateError {
    type: AdvantageActionTypes.ADVANTAGE_CREATE_ERROR,
    payload: string,
}

export interface AdvantageDeleteRequest {
    type: AdvantageActionTypes.ADVANTAGE_DELETE_REQUEST,
}

export interface AdvantageDeleteSuccess {
    type: AdvantageActionTypes.ADVANTAGE_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface AdvantageDeleteError {
    type: AdvantageActionTypes.ADVANTAGE_DELETE_ERROR,
    payload: string,
}

export interface AdvantageUpdateRequest {
    type: AdvantageActionTypes.ADVANTAGE_UPDATE_REQUEST,
}

export interface AdvantageUpdateSuccess {
    type: AdvantageActionTypes.ADVANTAGE_UPDATE_SUCCESS,
    payload: Advantage,
}

export interface AdvantageUpdateError {
    type: AdvantageActionTypes.ADVANTAGE_UPDATE_ERROR,
    payload: string,
}

export type AdvantageActions = 
 | AdvantageListRequestSend
 | AdvantageListSuccess
 | AdvantageListError
 | AdvantageCreateRequest
 | AdvantageCreatSuccess
 | AdvantageCreateError
 | AdvantageDeleteRequest
 | AdvantageDeleteSuccess
 | AdvantageDeleteError
 | AdvantageUpdateRequest
 | AdvantageUpdateSuccess
 | AdvantageUpdateError
