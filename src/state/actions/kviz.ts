import { Kviz } from "../../domain/kviz/kviz"
import { KvizActionTypes } from "../action-types"

export interface KvizListRequestSend {
    type: KvizActionTypes.KVIZ_REQUEST_SEND,
}

export interface KvizListSuccess {
    type: KvizActionTypes.KVIZ_SUCCESS,
    payload: Kviz[]
}

export interface KvizListError {
    type: KvizActionTypes.KVIZ_ERROR,
    payload: string,
}

export interface KvizCreateRequest {
    type: KvizActionTypes.KVIZ_CREATE_REQUEST,
}

export interface KvizCreatSuccess {
    type: KvizActionTypes.KVIZ_CREATE_SUCCESS,
    payload: Kviz,
}

export interface KvizCreateError {
    type: KvizActionTypes.KVIZ_CREATE_ERROR,
    payload: string,
}

export interface KvizDeleteRequest {
    type: KvizActionTypes.KVIZ_DELETE_REQUEST,
}

export interface KvizDeleteSuccess {
    type: KvizActionTypes.KVIZ_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface KvizDeleteError {
    type: KvizActionTypes.KVIZ_DELETE_ERROR,
    payload: string,
}

export interface KvizUpdateRequest {
    type: KvizActionTypes.KVIZ_UPDATE_REQUEST,
}

export interface KvizUpdateSuccess {
    type: KvizActionTypes.KVIZ_UPDATE_SUCCESS,
    payload: Kviz,
}

export interface KvizUpdateError {
    type: KvizActionTypes.KVIZ_UPDATE_ERROR,
    payload: string,
}

export type KvizActions = 
 | KvizListRequestSend
 | KvizListSuccess
 | KvizListError
 | KvizCreateRequest
 | KvizCreatSuccess
 | KvizCreateError
 | KvizDeleteRequest
 | KvizDeleteSuccess
 | KvizDeleteError
 | KvizUpdateRequest
 | KvizUpdateSuccess
 | KvizUpdateError
