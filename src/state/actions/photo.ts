import { Photo } from "../../domain/photo/photo"
import { PhotoActionTypes } from "../action-types"

export interface PhotoListRequestSend {
    type: PhotoActionTypes.PHOTO_REQUEST_SEND,
}

export interface PhotoListSuccess {
    type: PhotoActionTypes.PHOTO_SUCCESS,
    payload: Photo[]
}

export interface PhotoListError {
    type: PhotoActionTypes.PHOTO_ERROR,
    payload: string,
}

export interface PhotoCreateRequest {
    type: PhotoActionTypes.PHOTO_CREATE_REQUEST,
}

export interface PhotoCreatSuccess {
    type: PhotoActionTypes.PHOTO_CREATE_SUCCESS,
    payload: Photo,
}

export interface PhotoCreateError {
    type: PhotoActionTypes.PHOTO_CREATE_ERROR,
    payload: string,
}

export interface PhotoDeleteRequest {
    type: PhotoActionTypes.PHOTO_DELETE_REQUEST,
}

export interface PhotoDeleteSuccess {
    type: PhotoActionTypes.PHOTO_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface PhotoDeleteError {
    type: PhotoActionTypes.PHOTO_DELETE_ERROR,
    payload: string,
}

export interface PhotoUpdateRequest {
    type: PhotoActionTypes.PHOTO_UPDATE_REQUEST,
}

export interface PhotoUpdateSuccess {
    type: PhotoActionTypes.PHOTO_UPDATE_SUCCESS,
    payload: Photo,
}

export interface PhotoUpdateError {
    type: PhotoActionTypes.PHOTO_UPDATE_ERROR,
    payload: string,
}

export type PhotoActions = 
 | PhotoListRequestSend
 | PhotoListSuccess
 | PhotoListError
 | PhotoCreateRequest
 | PhotoCreatSuccess
 | PhotoCreateError
 | PhotoDeleteRequest
 | PhotoDeleteSuccess
 | PhotoDeleteError
 | PhotoUpdateRequest
 | PhotoUpdateSuccess
 | PhotoUpdateError
