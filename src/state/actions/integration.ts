import { AmocrmIntegration } from "../../domain/integration/amocrm-integration"
import { BitrixIntegration } from "../../domain/integration/bitrix-integration"
import { LeadactivAmoSecrets } from "../../domain/integration/leadactiv-amo-secrets"
import { LeadactivIntegration } from "../../domain/integration/leadactiv-integration"
import { MailIntegration } from "../../domain/integration/mail-integration"
import { ProjectAmocrmIntegration } from "../../domain/integration/project-amocrm-integration"
import { ProjectBitrixIntegration } from "../../domain/integration/project-bitrix-integration"
import { Site } from "../../domain/site/site"
import { IntegrationActionTypes } from "../action-types"

export interface AmocrmRequestSend {
    type: IntegrationActionTypes.AMOCRM_REQUEST_SEND,
}

export interface AmocrmSuccess {
    type: IntegrationActionTypes.AMOCRM_SUCCESS,
    payload: AmocrmIntegration
}

export interface AmocrmError {
    type: IntegrationActionTypes.AMOCRM_ERROR,
    payload: string,
}

export interface AmocrmCreateRequest {
    type: IntegrationActionTypes.AMOCRM_CREATE_REQUEST,
}

export interface AmocrmCreatSuccess {
    type: IntegrationActionTypes.AMOCRM_CREATE_SUCCESS,
    payload: AmocrmIntegration,
}

export interface AmocrmCreateError {
    type: IntegrationActionTypes.AMOCRM_CREATE_ERROR,
    payload: string,
}

export interface AmocrmDeleteRequest {
    type: IntegrationActionTypes.AMOCRM_DELETE_REQUEST,
}

export interface AmocrmDeleteSuccess {
    type: IntegrationActionTypes.AMOCRM_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface AmocrmDeleteError {
    type: IntegrationActionTypes.AMOCRM_DELETE_ERROR,
    payload: string,
}

export interface AmocrmUpdateRequest {
    type: IntegrationActionTypes.AMOCRM_UPDATE_REQUEST,
}

export interface AmocrmUpdateSuccess {
    type: IntegrationActionTypes.AMOCRM_UPDATE_SUCCESS,
    payload: AmocrmIntegration,
}

export interface AmocrmUpdateError {
    type: IntegrationActionTypes.AMOCRM_UPDATE_ERROR,
    payload: string,
}

export interface BitrixRequestSend {
    type: IntegrationActionTypes.BITRIX_REQUEST_SEND,
}

export interface BitrixSuccess {
    type: IntegrationActionTypes.BITRIX_SUCCESS,
    payload: BitrixIntegration
}

export interface BitrixError {
    type: IntegrationActionTypes.BITRIX_ERROR,
    payload: string,
}

export interface BitrixCreateRequest {
    type: IntegrationActionTypes.BITRIX_CREATE_REQUEST,
}

export interface BitrixCreatSuccess {
    type: IntegrationActionTypes.BITRIX_CREATE_SUCCESS,
    payload: BitrixIntegration,
}

export interface BitrixCreateError {
    type: IntegrationActionTypes.BITRIX_CREATE_ERROR,
    payload: string,
}

export interface BitrixDeleteRequest {
    type: IntegrationActionTypes.BITRIX_DELETE_REQUEST,
}

export interface BitrixDeleteSuccess {
    type: IntegrationActionTypes.BITRIX_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface BitrixDeleteError {
    type: IntegrationActionTypes.BITRIX_DELETE_ERROR,
    payload: string,
}

export interface BitrixUpdateRequest {
    type: IntegrationActionTypes.BITRIX_UPDATE_REQUEST,
}

export interface BitrixUpdateSuccess {
    type: IntegrationActionTypes.BITRIX_UPDATE_SUCCESS,
    payload: BitrixIntegration,
}

export interface BitrixUpdateError {
    type: IntegrationActionTypes.BITRIX_UPDATE_ERROR,
    payload: string,
}


export interface MailRequestSend {
    type: IntegrationActionTypes.MAIL_REQUEST_SEND,
}

export interface MailSuccess {
    type: IntegrationActionTypes.MAIL_SUCCESS,
    payload: MailIntegration
}

export interface MailError {
    type: IntegrationActionTypes.MAIL_ERROR,
    payload: string,
}

export interface MailCreateRequest {
    type: IntegrationActionTypes.MAIL_CREATE_REQUEST,
}

export interface MailCreatSuccess {
    type: IntegrationActionTypes.MAIL_CREATE_SUCCESS,
    payload: MailIntegration,
}

export interface MailCreateError {
    type: IntegrationActionTypes.MAIL_CREATE_ERROR,
    payload: string,
}

export interface MailDeleteRequest {
    type: IntegrationActionTypes.MAIL_DELETE_REQUEST,
}

export interface MailDeleteSuccess {
    type: IntegrationActionTypes.MAIL_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface MailDeleteError {
    type: IntegrationActionTypes.MAIL_DELETE_ERROR,
    payload: string,
}

export interface MailUpdateRequest {
    type: IntegrationActionTypes.MAIL_UPDATE_REQUEST,
}

export interface MailUpdateSuccess {
    type: IntegrationActionTypes.MAIL_UPDATE_SUCCESS,
    payload: MailIntegration,
}

export interface MailUpdateError {
    type: IntegrationActionTypes.MAIL_UPDATE_ERROR,
    payload: string,
}


export interface SiteWithIntegrationRequestSend {
    type: IntegrationActionTypes.SITE_WITH_INTEGRATION_REQUEST_SEND,
}

export interface SiteWithIntegrationSuccess {
    type: IntegrationActionTypes.SITE_WITH_INTEGRATION_SUCCESS,
    payload: Site[]
}

export interface SiteWithIntegrationError {
    type: IntegrationActionTypes.SITE_WITH_INTEGRATION_ERROR,
    payload: string,
}


export interface SiteWithoutIntegrationRequestSend {
    type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_REQUEST_SEND,
}

export interface SiteWithoutIntegrationSuccess {
    type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_SUCCESS,
    payload: Site[]
}

export interface SiteWithoutIntegrationError {
    type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_ERROR,
    payload: string,
}


export interface ReadIntegrationsOfSiteRequestSend {
    type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_REQUEST_SEND,
}

export interface ReadIntegrationsOfSiteSuccess {
    type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_SUCCESS,
    payload: {bitrix: BitrixIntegration | null, amocrm: AmocrmIntegration | null, mail: MailIntegration | null, leadactiv: LeadactivIntegration | null}
}

export interface ReadIntegrationsOfSiteError {
    type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_ERROR,
    payload: string,
}


export interface ProjectIntegrationRequestSend {
    type: IntegrationActionTypes.PROJECT_INTEGRATION_REQUEST_SEND,
}

export interface ProjectIntegrationSuccess {
    type: IntegrationActionTypes.PROJECT_INTEGRATION_SUCCESS,
    payload: ProjectAmocrmIntegration | ProjectBitrixIntegration
}

export interface ProjectIntegrationError {
    type: IntegrationActionTypes.PROJECT_INTEGRATION_ERROR,
    payload: string,
}









export interface LeadactivRequestSend {
    type: IntegrationActionTypes.LEADACTIV_REQUEST_SEND,
}

export interface LeadactivSuccess {
    type: IntegrationActionTypes.LEADACTIV_SUCCESS,
    payload: LeadactivIntegration
}

export interface LeadactivError {
    type: IntegrationActionTypes.LEADACTIV_ERROR,
    payload: string,
}

export interface LeadactivCreateRequest {
    type: IntegrationActionTypes.LEADACTIV_CREATE_REQUEST,
}

export interface LeadactivCreatSuccess {
    type: IntegrationActionTypes.LEADACTIV_CREATE_SUCCESS,
    payload: LeadactivIntegration,
}

export interface LeadactivCreateError {
    type: IntegrationActionTypes.LEADACTIV_CREATE_ERROR,
    payload: string,
}

export interface LeadactivDeleteRequest {
    type: IntegrationActionTypes.LEADACTIV_DELETE_REQUEST,
}

export interface LeadactivDeleteSuccess {
    type: IntegrationActionTypes.LEADACTIV_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface LeadactivDeleteError {
    type: IntegrationActionTypes.LEADACTIV_DELETE_ERROR,
    payload: string,
}

export interface LeadactivUpdateRequest {
    type: IntegrationActionTypes.LEADACTIV_UPDATE_REQUEST,
}

export interface LeadactivUpdateSuccess {
    type: IntegrationActionTypes.LEADACTIV_UPDATE_SUCCESS,
    payload: LeadactivIntegration,
}

export interface LeadactivUpdateError {
    type: IntegrationActionTypes.LEADACTIV_UPDATE_ERROR,
    payload: string,
}

export interface LeadactivSecretsRequestSend {
    type: IntegrationActionTypes.LEADACTIV_SECRETS_REQUEST,
}

export interface LeadactivSecretsSuccess {
    type: IntegrationActionTypes.LEADACTIV_SECRETS_SUCCESS,
    payload: LeadactivAmoSecrets
}

export interface LeadactivSecretsError {
    type: IntegrationActionTypes.LEADACTIV_SECRETS_ERROR,
    payload: string,
}


export type IntegrationActions = 
 | AmocrmRequestSend
 | AmocrmSuccess
 | AmocrmError
 | AmocrmCreateRequest
 | AmocrmCreatSuccess
 | AmocrmCreateError
 | AmocrmDeleteRequest
 | AmocrmDeleteSuccess
 | AmocrmDeleteError
 | AmocrmUpdateRequest
 | AmocrmUpdateSuccess
 | AmocrmUpdateError

 | BitrixRequestSend
 | BitrixSuccess
 | BitrixError
 | BitrixCreateRequest
 | BitrixCreatSuccess
 | BitrixCreateError
 | BitrixDeleteRequest
 | BitrixDeleteSuccess
 | BitrixDeleteError
 | BitrixUpdateRequest
 | BitrixUpdateSuccess
 | BitrixUpdateError

 | MailRequestSend
 | MailSuccess
 | MailError
 | MailCreateRequest
 | MailCreatSuccess
 | MailCreateError
 | MailDeleteRequest
 | MailDeleteSuccess
 | MailDeleteError
 | MailUpdateRequest
 | MailUpdateSuccess
 | MailUpdateError

 | LeadactivRequestSend
 | LeadactivSuccess
 | LeadactivError
 | LeadactivCreateRequest
 | LeadactivCreatSuccess
 | LeadactivCreateError
 | LeadactivDeleteRequest
 | LeadactivDeleteSuccess
 | LeadactivDeleteError
 | LeadactivUpdateRequest
 | LeadactivUpdateSuccess
 | LeadactivUpdateError

 | SiteWithIntegrationRequestSend
 | SiteWithIntegrationSuccess
 | SiteWithIntegrationError
 | SiteWithoutIntegrationRequestSend
 | SiteWithoutIntegrationSuccess
 | SiteWithoutIntegrationError

 | ReadIntegrationsOfSiteRequestSend
 | ReadIntegrationsOfSiteSuccess
 | ReadIntegrationsOfSiteError

 | ProjectIntegrationRequestSend
 | ProjectIntegrationSuccess
 | ProjectIntegrationError

 | LeadactivSecretsRequestSend
 | LeadactivSecretsSuccess
 | LeadactivSecretsError
