import { Lead } from "../../domain/lead"
import { LeadActionTypes } from "../action-types"

export interface LeadListRequestSend {
    type: LeadActionTypes.LEAD_REQUEST_SEND,
}

export interface LeadListSuccess {
    type: LeadActionTypes.LEAD_SUCCESS,
    payload: Lead[]
}

export interface LeadListError {
    type: LeadActionTypes.LEAD_ERROR,
    payload: string,
}

export interface LeadCreateRequest {
    type: LeadActionTypes.LEAD_CREATE_REQUEST,
}

export interface LeadCreatSuccess {
    type: LeadActionTypes.LEAD_CREATE_SUCCESS,
    payload: Lead,
}

export interface LeadCreateError {
    type: LeadActionTypes.LEAD_CREATE_ERROR,
    payload: string,
}

export interface LeadDeleteRequest {
    type: LeadActionTypes.LEAD_DELETE_REQUEST,
}

export interface LeadDeleteSuccess {
    type: LeadActionTypes.LEAD_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface LeadDeleteError {
    type: LeadActionTypes.LEAD_DELETE_ERROR,
    payload: string,
}

export interface LeadUpdateRequest {
    type: LeadActionTypes.LEAD_UPDATE_REQUEST,
}

export interface LeadUpdateSuccess {
    type: LeadActionTypes.LEAD_UPDATE_SUCCESS,
    payload: Lead,
}

export interface LeadUpdateError {
    type: LeadActionTypes.LEAD_UPDATE_ERROR,
    payload: string,
}

export interface LeadRequestNewPage {
    type: LeadActionTypes.LEAD_REQUEST_NEW_PAGE,
}

export interface LeadNewPageSuccess {
    type: LeadActionTypes.LEAD_NEW_PAGE_SUCCESS,
    payload: {leads: Lead[], page: number},
}

export interface LeadNewPageError {
    type: LeadActionTypes.LEAD_NEW_PAGE_ERROR,
    payload: string,
}

export type LeadActions = 
 | LeadListRequestSend
 | LeadListSuccess
 | LeadListError
 | LeadCreateRequest
 | LeadCreatSuccess
 | LeadCreateError
 | LeadDeleteRequest
 | LeadDeleteSuccess
 | LeadDeleteError
 | LeadUpdateRequest
 | LeadUpdateSuccess
 | LeadUpdateError
 | LeadRequestNewPage
 | LeadNewPageSuccess
 | LeadNewPageError
