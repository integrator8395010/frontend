import { Plan } from "../../domain/plan/plan"
import { PlanActionTypes } from "../action-types"

export interface PlanListRequestSend {
    type: PlanActionTypes.PLAN_REQUEST_SEND,
}

export interface PlanListSuccess {
    type: PlanActionTypes.PLAN_SUCCESS,
    payload: Plan[]
}

export interface PlanListError {
    type: PlanActionTypes.PLAN_ERROR,
    payload: string,
}

export interface PlanCreateRequest {
    type: PlanActionTypes.PLAN_CREATE_REQUEST,
}

export interface PlanCreatSuccess {
    type: PlanActionTypes.PLAN_CREATE_SUCCESS,
    payload: Plan,
}

export interface PlanCreateError {
    type: PlanActionTypes.PLAN_CREATE_ERROR,
    payload: string,
}

export interface PlanDeleteRequest {
    type: PlanActionTypes.PLAN_DELETE_REQUEST,
}

export interface PlanDeleteSuccess {
    type: PlanActionTypes.PLAN_DELETE_SUCCESS,
    payload: UniqueId,
}

export interface PlanDeleteError {
    type: PlanActionTypes.PLAN_DELETE_ERROR,
    payload: string,
}

export interface PlanUpdateRequest {
    type: PlanActionTypes.PLAN_UPDATE_REQUEST,
}

export interface PlanUpdateSuccess {
    type: PlanActionTypes.PLAN_UPDATE_SUCCESS,
    payload: Plan,
}

export interface PlanUpdateError {
    type: PlanActionTypes.PLAN_UPDATE_ERROR,
    payload: string,
}

export type PlanActions = 
 | PlanListRequestSend
 | PlanListSuccess
 | PlanListError
 | PlanCreateRequest
 | PlanCreatSuccess
 | PlanCreateError
 | PlanDeleteRequest
 | PlanDeleteSuccess
 | PlanDeleteError
 | PlanUpdateRequest
 | PlanUpdateSuccess
 | PlanUpdateError
