import { Dispatch } from "react"
import { PlanActionTypes } from "../action-types";
import { PlanActions } from "../actions"
import { Plan } from "../../domain/plan/plan";

export const SendPlanListRequest = () => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_REQUEST_SEND,
          });
    }
}

export const PlanListSuccess = (kvizes: Plan[]) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_SUCCESS,
            payload: kvizes,
        });
    }
}

export const PlanListError = (error:string) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_ERROR,
            payload: error,
        });
    }
}

export const PlanCreateRequest = () => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_CREATE_REQUEST,
          });
    }
}

export const PlanCreateSuccess = (kviz: Plan) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_CREATE_SUCCESS,
            payload: kviz,
          });
    }
}

export const PlanCreateError = (message: string) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_CREATE_ERROR,
            payload: message,
          });
    }
}


export const PlanUpdateRequest = () => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_UPDATE_REQUEST,
          });
    }
}

export const PlanUpdateSuccess = (category: Plan) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const PlanUpdateError = (message: string) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const PlanDeleteRequest = () => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_DELETE_REQUEST,
          });
    }
}

export const PlanDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const PlanDeleteError = (message: string) => {
    return async (dispatch: Dispatch<PlanActions>) => {
        dispatch({
            type: PlanActionTypes.PLAN_DELETE_ERROR,
            payload: message,
          });
    }
}
