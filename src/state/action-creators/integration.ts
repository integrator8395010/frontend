import { Dispatch } from "react"
import { IntegrationActionTypes } from "../action-types";
import { IntegrationActions } from "../actions/integration";
import { AmocrmIntegration } from "../../domain/integration/amocrm-integration";
import { BitrixIntegration } from "../../domain/integration/bitrix-integration";
import { MailIntegration } from "../../domain/integration/mail-integration";
import { Site } from "../../domain/site/site";
import { ProjectAmocrmIntegration } from "../../domain/integration/project-amocrm-integration";
import { ProjectBitrixIntegration } from "../../domain/integration/project-bitrix-integration";
import { LeadactivIntegration } from "../../domain/integration/leadactiv-integration";
import { LeadactivAmoSecrets } from "../../domain/integration/leadactiv-amo-secrets";

export const SendAmocrmIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_REQUEST_SEND,
          });
    }
}

export const AmocrmIntegrationSuccess = (integration: AmocrmIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_SUCCESS,
            payload: integration,
        });
    }
}

export const AmocrmIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_ERROR,
            payload: error,
        });
    }
}

export const AmocrmIntegrationCreateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_CREATE_REQUEST,
          });
    }
}

export const AmocrmIntegrationCreateSuccess = (integration: AmocrmIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_CREATE_SUCCESS,
            payload: integration,
          });
    }
}

export const AmocrmIntegrationCreateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_CREATE_ERROR,
            payload: message,
          });
    }
}


export const AmocrmIntegrationUpdateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_UPDATE_REQUEST,
          });
    }
}

export const AmocrmIntegrationUpdateSuccess = (integration: AmocrmIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_UPDATE_SUCCESS,
            payload: integration,
          });
    }
}

export const AmocrmIntegrationUpdateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const AmocrmIntegrationDeleteRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_DELETE_REQUEST,
          });
    }
}

export const AmocrmIntegrationDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const AmocrmIntegrationDeleteError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.AMOCRM_DELETE_ERROR,
            payload: message,
          });
    }
}


export const SendBitrixIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_REQUEST_SEND,
          });
    }
}

export const BitrixIntegrationSuccess = (integration: BitrixIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_SUCCESS,
            payload: integration,
        });
    }
}

export const BitrixIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_ERROR,
            payload: error,
        });
    }
}

export const BitrixIntegrationCreateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_CREATE_REQUEST,
          });
    }
}

export const BitrixIntegrationCreateSuccess = (integration: BitrixIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_CREATE_SUCCESS,
            payload: integration,
          });
    }
}

export const BitrixIntegrationCreateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_CREATE_ERROR,
            payload: message,
          });
    }
}


export const BitrixIntegrationUpdateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_UPDATE_REQUEST,
          });
    }
}

export const BitrixIntegrationUpdateSuccess = (integration: BitrixIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_UPDATE_SUCCESS,
            payload: integration,
          });
    }
}

export const BitrixIntegrationUpdateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const BitrixIntegrationDeleteRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_DELETE_REQUEST,
          });
    }
}

export const BitrixIntegrationDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const BitrixIntegrationDeleteError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.BITRIX_DELETE_ERROR,
            payload: message,
          });
    }
}


export const SendMailIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_REQUEST_SEND,
          });
    }
}

export const MailIntegrationSuccess = (integration: MailIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_SUCCESS,
            payload: integration,
        });
    }
}

export const MailIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_ERROR,
            payload: error,
        });
    }
}

export const MailIntegrationCreateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_CREATE_REQUEST,
          });
    }
}

export const MailIntegrationCreateSuccess = (integration: MailIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_CREATE_SUCCESS,
            payload: integration,
          });
    }
}

export const MailIntegrationCreateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_CREATE_ERROR,
            payload: message,
          });
    }
}


export const MailIntegrationUpdateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_UPDATE_REQUEST,
          });
    }
}

export const MailIntegrationUpdateSuccess = (integration: MailIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_UPDATE_SUCCESS,
            payload: integration,
          });
    }
}

export const MailIntegrationUpdateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const MailIntegrationDeleteRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_DELETE_REQUEST,
          });
    }
}

export const MailIntegrationDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const MailIntegrationDeleteError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.MAIL_DELETE_ERROR,
            payload: message,
          });
    }
}



export const SitesWithIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITH_INTEGRATION_REQUEST_SEND,
          });
    }
}

export const SitesWithIntegrationSuccess = (sites: Site[]) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITH_INTEGRATION_SUCCESS,
            payload: sites,
        });
    }
}

export const SitesWithIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITH_INTEGRATION_ERROR,
            payload: error,
        });
    }
}


export const SitesWithOutIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_REQUEST_SEND,
          });
    }
}

export const SitesWithOutIntegrationSuccess = (sites: Site[]) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_SUCCESS,
            payload: sites,
        });
    }
}

export const SitesWithOutIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.SITE_WITHOUT_INTEGRATION_ERROR,
            payload: error,
        });
    }
}



export const IntegrationsOfSiteRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_REQUEST_SEND,
          });
    }
}

export const IntegrationsOfSiteSuccess = (integrations: {bitrix: BitrixIntegration | null, amocrm: AmocrmIntegration | null, mail: MailIntegration | null, leadactiv: LeadactivIntegration | null}) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_SUCCESS,
            payload: integrations,
        });
    }
}

export const IntegrationsOfSiteError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.INTEGRATIONS_OF_SITE_ERROR,
            payload: error,
        });
    }
}


export const ProjectIntegrationRequestSend = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.PROJECT_INTEGRATION_REQUEST_SEND,
          });
    }
}

export const ProjectIntegrationSuccess = (integration: ProjectAmocrmIntegration | ProjectBitrixIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.PROJECT_INTEGRATION_SUCCESS,
            payload: integration,
        });
    }
}

export const ProjectIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.PROJECT_INTEGRATION_ERROR,
            payload: error,
        });
    }
}


export const SendLeadactivIntegrationRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_REQUEST_SEND,
          });
    }
}

export const LeadactivIntegrationSuccess = (integration: LeadactivIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_SUCCESS,
            payload: integration,
        });
    }
}

export const LeadactivIntegrationError = (error:string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_ERROR,
            payload: error,
        });
    }
}

export const LeadactivIntegrationCreateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_CREATE_REQUEST,
          });
    }
}

export const LeadactivIntegrationCreateSuccess = (integration: LeadactivIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_CREATE_SUCCESS,
            payload: integration,
          });
    }
}

export const LeadactivIntegrationCreateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_CREATE_ERROR,
            payload: message,
          });
    }
}


export const LeadactivIntegrationUpdateRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_UPDATE_REQUEST,
          });
    }
}

export const LeadactivIntegrationUpdateSuccess = (integration: LeadactivIntegration) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_UPDATE_SUCCESS,
            payload: integration,
          });
    }
}

export const LeadactivIntegrationUpdateError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const LeadactivIntegrationDeleteRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_DELETE_REQUEST,
          });
    }
}

export const LeadactivIntegrationDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const LeadactivIntegrationDeleteError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_DELETE_ERROR,
            payload: message,
          });
    }
}


export const LeadactivSecretsRequest = () => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_SECRETS_REQUEST,
          });
    }
}

export const LeadactivSecretsSuccess = (integration: LeadactivAmoSecrets) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_SECRETS_SUCCESS,
            payload: integration,
          });
    }
}

export const LeadactivSecretsError = (message: string) => {
    return async (dispatch: Dispatch<IntegrationActions>) => {
        dispatch({
            type: IntegrationActionTypes.LEADACTIV_SECRETS_ERROR,
            payload: message,
          });
    }
}