import { Dispatch } from "react"
import { PhotoActionTypes } from "../action-types";
import { PhotoActions } from "../actions"
import { Photo } from "../../domain/photo/photo";

export const SendPhotoListRequest = () => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_REQUEST_SEND,
          });
    }
}

export const PhotoListSuccess = (kvizes: Photo[]) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_SUCCESS,
            payload: kvizes,
        });
    }
}

export const PhotoListError = (error:string) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_ERROR,
            payload: error,
        });
    }
}

export const PhotoCreateRequest = () => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_CREATE_REQUEST,
          });
    }
}

export const PhotoCreateSuccess = (kviz: Photo) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_CREATE_SUCCESS,
            payload: kviz,
          });
    }
}

export const PhotoCreateError = (message: string) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_CREATE_ERROR,
            payload: message,
          });
    }
}


export const PhotoUpdateRequest = () => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_UPDATE_REQUEST,
          });
    }
}

export const PhotoUpdateSuccess = (category: Photo) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const PhotoUpdateError = (message: string) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const PhotoDeleteRequest = () => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_DELETE_REQUEST,
          });
    }
}

export const PhotoDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const PhotoDeleteError = (message: string) => {
    return async (dispatch: Dispatch<PhotoActions>) => {
        dispatch({
            type: PhotoActionTypes.PHOTO_DELETE_ERROR,
            payload: message,
          });
    }
}
