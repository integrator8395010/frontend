import { Dispatch } from "react"
import { AdvantageActionTypes } from "../action-types";
import { AdvantageActions } from "../actions"
import { Advantage } from "../../domain/advantage/advantage";

export const SendAdvantageListRequest = () => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_REQUEST_SEND,
          });
    }
}

export const AdvantageListSuccess = (kvizes: Advantage[]) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_SUCCESS,
            payload: kvizes,
        });
    }
}

export const AdvantageListError = (error:string) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_ERROR,
            payload: error,
        });
    }
}

export const AdvantageCreateRequest = () => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_CREATE_REQUEST,
          });
    }
}

export const AdvantageCreateSuccess = (kviz: Advantage) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_CREATE_SUCCESS,
            payload: kviz,
          });
    }
}

export const AdvantageCreateError = (message: string) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_CREATE_ERROR,
            payload: message,
          });
    }
}


export const AdvantageUpdateRequest = () => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_UPDATE_REQUEST,
          });
    }
}

export const AdvantageUpdateSuccess = (category: Advantage) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const AdvantageUpdateError = (message: string) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const AdvantageDeleteRequest = () => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_DELETE_REQUEST,
          });
    }
}

export const AdvantageDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const AdvantageDeleteError = (message: string) => {
    return async (dispatch: Dispatch<AdvantageActions>) => {
        dispatch({
            type: AdvantageActionTypes.ADVANTAGE_DELETE_ERROR,
            payload: message,
          });
    }
}
