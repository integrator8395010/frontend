import { Dispatch } from "react"
import { ProjectActionTypes } from "../action-types";
import { ProjectActions } from "../actions"
import { Project } from "../../domain/project/project";

export const SendProjectListRequest = () => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_REQUEST_SEND,
          });
    }
}

export const ProjectListSuccess = (projects: Project[]) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_SUCCESS,
            payload: projects,
        });
    }
}

export const ProjectListError = (error:string) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_ERROR,
            payload: error,
        });
    }
}




export const ProjectCreateRequest = () => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_CREATE_REQUEST,
          });
    }
}

export const ProjectCreateSuccess = (project: Project) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_CREATE_SUCCESS,
            payload: project,
          });
    }
}

export const ProjectCreateError = (message: string) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_CREATE_ERROR,
            payload: message,
          });
    }
}


export const ProjectUpdateRequest = () => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_UPDATE_REQUEST,
          });
    }
}

export const ProjectUpdateSuccess = (category: Project) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const ProjectUpdateError = (message: string) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const ProjectDeleteRequest = () => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_DELETE_REQUEST,
          });
    }
}

export const ProjectDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const ProjectDeleteError = (message: string) => {
    return async (dispatch: Dispatch<ProjectActions>) => {
        dispatch({
            type: ProjectActionTypes.PROJECT_DELETE_ERROR,
            payload: message,
          });
    }
}
