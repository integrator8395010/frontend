import { Dispatch } from "react"
import { KvizActionTypes } from "../action-types";
import { KvizActions } from "../actions"
import { Kviz } from "../../domain/kviz/kviz";

export const SendKvizListRequest = () => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_REQUEST_SEND,
          });
    }
}

export const KvizListSuccess = (kvizes: Kviz[]) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_SUCCESS,
            payload: kvizes,
        });
    }
}

export const KvizListError = (error:string) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_ERROR,
            payload: error,
        });
    }
}

export const KvizCreateRequest = () => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_CREATE_REQUEST,
          });
    }
}

export const KvizCreateSuccess = (kviz: Kviz) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_CREATE_SUCCESS,
            payload: kviz,
          });
    }
}

export const KvizCreateError = (message: string) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_CREATE_ERROR,
            payload: message,
          });
    }
}


export const KvizUpdateRequest = () => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_UPDATE_REQUEST,
          });
    }
}

export const KvizUpdateSuccess = (category: Kviz) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const KvizUpdateError = (message: string) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const KvizDeleteRequest = () => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_DELETE_REQUEST,
          });
    }
}

export const KvizDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const KvizDeleteError = (message: string) => {
    return async (dispatch: Dispatch<KvizActions>) => {
        dispatch({
            type: KvizActionTypes.KVIZ_DELETE_ERROR,
            payload: message,
          });
    }
}
