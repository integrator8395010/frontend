import { Dispatch } from "react"
import { SiteActionTypes } from "../action-types";
import { SiteActions } from "../actions"
import { Site } from "../../domain/site/site";

export const SendSiteListRequest = () => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_REQUEST_SEND,
          });
    }
}

export const SiteListSuccess = (sites: Site[]) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_SUCCESS,
            payload: sites,
        });
    }
}

export const SiteListError = (error:string) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_ERROR,
            payload: error,
        });
    }
}




export const SiteCreateRequest = () => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_CREATE_REQUEST,
          });
    }
}

export const SiteCreateSuccess = (site: Site) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_CREATE_SUCCESS,
            payload: site,
          });
    }
}

export const SiteCreateError = (message: string) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_CREATE_ERROR,
            payload: message,
          });
    }
}


export const SiteUpdateRequest = () => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_UPDATE_REQUEST,
          });
    }
}

export const SiteUpdateSuccess = (category: Site) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_UPDATE_SUCCESS,
            payload: category,
          });
    }
}

export const SiteUpdateError = (message: string) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_UPDATE_ERROR,
            payload: message,
          });
    }
}


export const SiteDeleteRequest = () => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_DELETE_REQUEST,
          });
    }
}

export const SiteDeleteSuccess = (id: UniqueId) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_DELETE_SUCCESS,
            payload: id,
          });
    }
}

export const SiteDeleteError = (message: string) => {
    return async (dispatch: Dispatch<SiteActions>) => {
        dispatch({
            type: SiteActionTypes.SITE_DELETE_ERROR,
            payload: message,
          });
    }
}
