import { Advantage } from "../../../domain/advantage/advantage"


export interface AdvantageInterface {
    AdvantagesOfKviz(kvizId: UniqueId): Promise<Advantage[] | Error>
    AdvantageList(): Promise<Advantage[] | Error>
    CreateAdvantage(file: File, kvizId?: string, title?: string, ): Promise<Advantage | Error>
    UpdateAdvantage(id: string, photo?: string, title?: string,): Promise<Advantage | Error>
    DeleteAdvantage(id: UniqueId): Promise<string | Error>
}