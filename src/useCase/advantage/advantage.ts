import { AdvantageService } from "../../api/advantage/advantage";
import { actionCreators } from "../../state";

export class AdvantageUseCases {
    private api: AdvantageService;
    private stateActions: typeof actionCreators;

    constructor(api: AdvantageService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public AdvantagesOfKviz = async (projectId: UniqueId) => {
        this.stateActions.SendAdvantageListRequest()
        let response = await this.api.AdvantagesOfKviz(projectId)
        if (response instanceof Error) {
            this.stateActions.AdvantageListError(response.message)
            return
        } else {
            this.stateActions.AdvantageListSuccess(response)
        }
    }

    public CreateAdvantage = async (file: File, kvizId?: string, title?: string, callback?: ()=>void) => {
        this.stateActions.AdvantageCreateRequest()
        let response = await this.api.CreateAdvantage(file, kvizId, title)
        if (response instanceof Error) {
            this.stateActions.AdvantageDeleteError(response.message)
            return
        } else {
            this.stateActions.AdvantageCreateSuccess(response)
            if (callback) {
                callback()
            }
        }
    }

    public DeleteAdvantage = async (id: UniqueId) => {
        this.stateActions.AdvantageCreateRequest()
        let response = await this.api.DeleteAdvantage(id)
        if (response instanceof Error) {
            this.stateActions.AdvantageDeleteError(response.message)
            return
        } else {
            this.stateActions.AdvantageDeleteSuccess(id)
        }
    }

}