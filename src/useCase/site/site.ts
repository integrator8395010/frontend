import { SiteService } from "../../api/site/site";
import { Site } from "../../domain/site/site";
import { actionCreators } from "../../state";

export class SiteUseCases {
    private api: SiteService;
    private stateActions: typeof actionCreators;

    constructor(api: SiteService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public SiteById = async (siteId: UniqueId) : Promise<Site | Error> => {
        let response = await this.api.SiteById(siteId)
        return response
    }

    public SitesOfProject = async (projectId: UniqueId) => {
        this.stateActions.SendSiteListRequest()
        let response = await this.api.SitesOfProject(projectId)
        if (response instanceof Error) {
            this.stateActions.SiteListError(response.message)
            return
        } else {
            this.stateActions.SiteListSuccess(response)
        }
    }

    public GetSiteList = async () => {
        this.stateActions.SendSiteListRequest()
        let response = await this.api.SiteList()
        if (response instanceof Error) {
            this.stateActions.SiteListError(response.message)
            return
        } else {
            this.stateActions.SiteListSuccess(response)
        }
    }

    public CreateSite = async (name: string, projectId: UniqueId, url: string, callback: () => void) => {
        this.stateActions.SiteCreateRequest()
        let response = await this.api.CreateSite(name, projectId, url)
        if (response instanceof Error) {
            this.stateActions.SiteDeleteError(response.message)
            return
        } else {
            this.stateActions.SiteCreateSuccess(response)
            callback()
        }
    }

    public DeleteSite = async (id: UniqueId) => {
        this.stateActions.SiteCreateRequest()
        let response = await this.api.DeleteSite(id)
        if (response instanceof Error) {
            this.stateActions.SiteDeleteError(response.message)
            return
        } else {
            this.stateActions.SiteDeleteSuccess(id)
        }
    }

    public UpdateSite = async (id: UniqueId, name: string, projectId: UniqueId, url: string, callback: ()=>void) => {
        this.stateActions.SiteUpdateRequest()
        let response = await this.api.UpdateSite(id, name, projectId, url)
        if (response instanceof Error) {
            this.stateActions.SiteUpdateError(response.message)
            return
        } else {
            this.stateActions.SiteUpdateSuccess(response)
            callback()
        }
    }

}