import { Site } from "../../../../domain/site/site";

export interface SiteInterface {
    SiteById(siteId: UniqueId): Promise<Site | Error>
    SiteList(): Promise<Site[] | Error>
    SitesOfProject(projectId: UniqueId): Promise<Site[] | Error>
    CreateSite(name: string, projectId: UniqueId, url: string,): Promise<Site | Error>
    UpdateSite(id: UniqueId, name: string, projectId: UniqueId, url: string,): Promise<Site | Error>
    DeleteSite(id: UniqueId): Promise<string | Error>
}