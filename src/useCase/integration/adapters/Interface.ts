import { AmocrmIntegration } from "../../../domain/integration/amocrm-integration";
import { BitrixIntegration } from "../../../domain/integration/bitrix-integration";
import { MailIntegration } from "../../../domain/integration/mail-integration";
import { AmocrmAssociation, AmocrmDefaultValue, BitrixAssociation, BitrixDefaultValue } from "../../../domain/integration/types";
import { Site } from "../../../domain/site/site";

export interface IntegrationInterface {
    CreateMailIntegration(recipients: string[], siteId: UniqueId ): Promise<MailIntegration | Error>
    UpdateMailIntegration(id: UniqueId, recipients: string[], siteId: UniqueId): Promise<MailIntegration | Error>
    DeleteMailIntegration(id: UniqueId): Promise<string | Error>
    ReadMailIntegration(id: UniqueId ): Promise<MailIntegration | Error>

    CreateAmocrmIntegration(siteId: UniqueId, pipelineId: number, statusId: number, responsible: number, unsorted: boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[]): Promise<AmocrmIntegration | Error>
    UpdateAmocrmIntegration(id: UniqueId, siteId: UniqueId, pipelineId: number, statusId: number, responsible: number, unsorted: boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[]): Promise<AmocrmIntegration | Error>
    DeleteAmocrmIntegration(id: UniqueId): Promise<string | Error>
    ReadAmocrmIntegration(id: UniqueId ): Promise<AmocrmIntegration | Error>
    ReadFieldsOfAmoCrmLead(integrationId: UniqueId, page: number): Promise<any | Error>

    CreateBitrixIntegration(siteId: UniqueId, responsible: number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[] ): Promise<BitrixIntegration | Error>
    UpdateBitrixIntegration(id: UniqueId, siteId: UniqueId, responsible: number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[] ): Promise<BitrixIntegration | Error>
    DeleteBitrixIntegration(id: UniqueId): Promise<string | Error>
    ReadBitrixIntegration(id: UniqueId ): Promise<BitrixIntegration | Error>

    ReadIntegrationsOfSite(id: UniqueId): Promise<{bitrix: BitrixIntegration | null, amocrm: AmocrmIntegration | null, mail: MailIntegration | null } | Error>
    SitesWithIntegrations(projectId: UniqueId): Promise<Site[] | Error>
    SitesWithoutIntegrations(projectId: UniqueId): Promise<Site[] | Error>
}