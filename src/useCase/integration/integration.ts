import { IntegrationService } from "../../api/integration/integration";
import { AmocrmAssociation, AmocrmDefaultValue, BitrixAssociation, BitrixDefaultValue } from "../../domain/integration/types";
import { actionCreators } from "../../state";

export class IntegrationUseCases {
    private api: IntegrationService;
    private stateActions: typeof actionCreators;

    constructor(api: IntegrationService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public ReadProjectIntegration = async (projectId: UniqueId) => {
       
        let response = await this.ReadProjectAmoIntegration(projectId)
        if (!response) {
            this.ReadProjectBitrixIntegration(projectId)
        }
    }

    public ReadProjectAmoIntegration = async (projectId: UniqueId) => {
        this.stateActions.ProjectIntegrationRequestSend()
        let response = await this.api.ReadProjectAmocrmIntegration(projectId)
        if (response instanceof Error) {
            this.stateActions.ProjectIntegrationError(response.message)
            //try to read bitrix
            return false
        } else {
            this.stateActions.ProjectIntegrationSuccess(response)
            return true
        }
    }

    public ReadProjectBitrixIntegration = async (projectId: UniqueId) => {
        this.stateActions.ProjectIntegrationRequestSend()
        let response = await this.api.ReadProjectBitrixIntegration(projectId)
        if (response instanceof Error) {
            this.stateActions.ProjectIntegrationError(response.message)
            return
        } else {
            this.stateActions.ProjectIntegrationSuccess(response)
        }
    }

    public ReadSitesWithIntegrationsOfProject = async (projectId: UniqueId) => {
        this.stateActions.SitesWithIntegrationRequest()
        let response = await this.api.SitesWithIntegrations(projectId)
        if (response instanceof Error) {
            this.stateActions.SitesWithIntegrationError(response.message)
            return
        } else {
            this.stateActions.SitesWithIntegrationSuccess(response)
        }
    }

    public ReadSitesWithoutIntegrationsOfProject = async (projectId: UniqueId) => {
        this.stateActions.SitesWithOutIntegrationRequest()
        let response = await this.api.SitesWithoutIntegrations(projectId)
        if (response instanceof Error) {
            this.stateActions.SitesWithOutIntegrationError(response.message)
            return
        } else {
            this.stateActions.SitesWithOutIntegrationSuccess(response)
        }
    }

    public ReadIntegrationsOfSite = async (siteId: UniqueId) => {
        this.stateActions.IntegrationsOfSiteRequest()
        let response = await this.api.ReadIntegrationsOfSite(siteId)
        console.log(response)
        if (response instanceof Error) {
            this.stateActions.IntegrationsOfSiteError(response.message)
            return
        } else {
            this.stateActions.IntegrationsOfSiteSuccess(response)
        }
    }

    public CreateMailIntegration = async (siteId: UniqueId, recipients: string[], callback: ()=>void) => {
        this.stateActions.MailIntegrationCreateRequest()
        let response = await this.api.CreateMailIntegration(recipients, siteId)
        if (response instanceof Error) {
            this.stateActions.MailIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.MailIntegrationCreateSuccess(response)
            callback()
        }
    }

    public UpdateMailIntegration = async (id: UniqueId, siteId: UniqueId, recipients: string[], callback: ()=>void) => {
        this.stateActions.MailIntegrationUpdateRequest()
        let response = await this.api.UpdateMailIntegration(id, recipients, siteId)
        if (response instanceof Error) {
            this.stateActions.MailIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.MailIntegrationUpdateSuccess(response)
            callback()
        }
    }

    public CreateProjectBitrixIntegration = async (projectId: UniqueId, baseUrl: string, callback: ()=>void) => {
        this.stateActions.ProjectIntegrationRequestSend()
        let response = await this.api.CreateProjectBitrixIntegration(projectId, baseUrl)
        console.log(response)
        if (response instanceof Error) {
            this.stateActions.ProjectIntegrationError(response.message)
            return
        } else {
            this.stateActions.ProjectIntegrationSuccess(response)
            callback()
        }
    }

    public UpdateProjectBitrixIntegration = async (id: UniqueId, projectId: UniqueId, baseUrl: string, callback: ()=>void) => {
        this.stateActions.ProjectIntegrationRequestSend()
        let response = await this.api.UpdateProjectBitrixIntegration(id, projectId, baseUrl)
        if (response instanceof Error) {
            this.stateActions.ProjectIntegrationError(response.message)
            return
        } else {
            this.stateActions.ProjectIntegrationSuccess(response)
            callback()
        }
    }

    public DeleteProjectBitrixIntegration = async (id: UniqueId, callback: ()=>void) => {
        this.stateActions.ProjectIntegrationRequestSend()
        let response = await this.api.DeleteProjectBitrixIntegration(id)
        if (response instanceof Error) {
            this.stateActions.ProjectIntegrationError(response.message)
            return
        } else {
            //this.stateActions.ProjectIntegrationSuccess(response)
            callback()
        }
    }

    public CreateAmocrmIntegration = async (siteId: UniqueId, pipelineId:number, statusId:number, responsible:number, unsorted:boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[], callback:()=>void) => {
        this.stateActions.AmocrmIntegrationCreateRequest()
        let response = await this.api.CreateAmocrmIntegration(siteId, pipelineId, statusId, responsible, unsorted, associations, defaultValues)
        if (response instanceof Error) {
            this.stateActions.AmocrmIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.AmocrmIntegrationCreateSuccess(response)
            callback()
        }
    }

    public DeleteAmocrmIntegrationOfSite = async (id: UniqueId, callback: ()=>void) => {
        this.stateActions.AmocrmIntegrationCreateRequest()
        let response = await this.api.DeleteAmocrmIntegration(id)
        if (response instanceof Error) {
            this.stateActions.AmocrmIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.AmocrmIntegrationDeleteSuccess(response)
            callback()
        }
    }

    public UpdateAmocrmIntegration = async (id: UniqueId, siteId: UniqueId, pipelineId:number, statusId:number, responsible:number, unsorted:boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[], callback: ()=>void) => {
        this.stateActions.AmocrmIntegrationUpdateRequest()
        let response = await this.api.UpdateAmocrmIntegration(id, siteId, pipelineId, statusId, responsible, unsorted, associations, defaultValues)
        console.log(response)
        if (response instanceof Error) {
            this.stateActions.AmocrmIntegrationUpdateError(response.message)
            return
        } else {
            this.stateActions.AmocrmIntegrationUpdateSuccess(response)
            callback()
        }
    }

    public ReadFieldsOfAmoCrmLead = async (integrationId: UniqueId, page: number) => {
        let response = this.api.ReadFieldsOfAmoCrmLead(integrationId, page)
        if (response instanceof Error) {
            return Error("something wrong")
        }
        return response 
    }

    public ReadUsersOfAmoCrm = async (integrationId: UniqueId) => {
        let response = this.api.ReadUsersOfAmoCrm(integrationId)
        if (response instanceof Error) {
            return Error("something wrong")
        }
        return response 
    }

    public ReadPipelinesOfAmoCrm = async (integrationId: UniqueId) => {
        let response = this.api.ReadPipelinesOfAmoCrm(integrationId)
        if (response instanceof Error) {
            return Error("something wrong")
        }
        return response 
    }

    public ReadStatusesOfPipelineAmoCrm = async (integrationId: UniqueId, pipelineId: number) => {
        let response = this.api.ReadStatusesOfPipelineAmoCrm(integrationId, pipelineId)
        if (response instanceof Error) {
            return Error("something wrong")
        }
        return response 
    }

    public CreateBitrixIntegration = async (siteId: UniqueId, responsible:number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[], callback:()=>void) => {
        this.stateActions.BitrixIntegrationCreateRequest()
        let response = await this.api.CreateBitrixIntegration(siteId, responsible, sendType, associations, defaultValues)
        if (response instanceof Error) {
            this.stateActions.BitrixIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.BitrixIntegrationCreateSuccess(response)
            callback()
        }
    }

    public DeleteBitrixIntegrationOfSite = async (id: UniqueId, callback: ()=>void) => {
        this.stateActions.BitrixIntegrationCreateRequest()
        let response = await this.api.DeleteBitrixIntegration(id)
        if (response instanceof Error) {
            this.stateActions.BitrixIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.BitrixIntegrationDeleteSuccess(response)
            callback()
        }
    }

    public UpdateBitrixIntegration = async (id: UniqueId, siteId: UniqueId, responsible:number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[], callback: ()=>void) => {
        this.stateActions.BitrixIntegrationUpdateRequest()
        let response = await this.api.UpdateBitrixIntegration(id, siteId, responsible, sendType, associations, defaultValues)
        if (response instanceof Error) {
            this.stateActions.BitrixIntegrationUpdateError(response.message)
            return
        } else {
            this.stateActions.BitrixIntegrationUpdateSuccess(response)
            callback()
        }
    }

    public CreateLeadactivIntegration = async (siteId: UniqueId, responsible:number, callback:()=>void) => {
        this.stateActions.LeadactivIntegrationCreateRequest()
        let response = await this.api.CreateLeadactivIntegration(siteId, responsible)
        if (response instanceof Error) {
            this.stateActions.LeadactivIntegrationCreateError(response.message)
            return
        } else {
            this.stateActions.LeadactivIntegrationCreateSuccess(response)
            callback()
        }
    }

    public UpdateLeadactivIntegration = async (id: UniqueId, siteId: UniqueId, responsible:number, callback:()=>void) => {
        this.stateActions.LeadactivIntegrationUpdateRequest()
        let response = await this.api.UpdateLeadactivIntegration(id, siteId, responsible)
        if (response instanceof Error) {
            this.stateActions.LeadactivIntegrationUpdateError(response.message)
            return
        } else {
            this.stateActions.LeadactivIntegrationUpdateSuccess(response)
            callback()
        }
    }

    public DeleteLeadactivIntegration = async (id: UniqueId, callback:()=>void) => {
        this.stateActions.LeadactivIntegrationDeleteRequest()
        let response = await this.api.DeleteLeadactivIntegration(id)
        if (response instanceof Error) {
            this.stateActions.LeadactivIntegrationDeleteError(response.message)
            return
        } else {
            this.stateActions.LeadactivIntegrationDeleteSuccess(response)
            callback()
        }
    }

    public ReadLeadactivIntegration = async (siteId: UniqueId) => {
        this.stateActions.SendLeadactivIntegrationRequest()
        let response = await this.api.ReadLeadactivIntegration(siteId)
        if (response instanceof Error) {
            this.stateActions.LeadactivIntegrationError(response.message)
            return
        } else {
            this.stateActions.LeadactivIntegrationSuccess(response)
        }
    }

    public ReadUsersOfLeadactiv = async (page: number) => {
        let response = this.api.ReadUsersOfLeadactiv(page)
        if (response instanceof Error) {
            return Error("something wrong")
        }
        return response 
    }

    public ReadLeadactivAmoSecrets = async () => {
        this.stateActions.LeadactivSecretsRequest()
        let response = await this.api.ReadLeadactivAmoSecrets()
        if (response instanceof Error) {
            this.stateActions.LeadactivSecretsError(response.message)
            return Error("something wrong")
        } else {
            this.stateActions.LeadactivSecretsSuccess(response)
        }
    }

}