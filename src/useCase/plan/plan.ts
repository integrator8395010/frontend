import { PlanService } from "../../api/plan/plan";
import { actionCreators } from "../../state";

export class PlanUseCases {
    private api: PlanService;
    private stateActions: typeof actionCreators;

    constructor(api: PlanService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public PlansOfKviz = async (kvizId: UniqueId) => {
        this.stateActions.SendPlanListRequest()
        let response = await this.api.PlansOfKviz(kvizId)
        if (response instanceof Error) {
            this.stateActions.PlanListError(response.message)
            return
        } else {
            this.stateActions.PlanListSuccess(response)
        }
    }

    public CreatePlan = async (file: File, bathRoomArea?: number, bedRoomArea?: number, kitchenArea?: number, kvizId?: string, livingArea?: number, photo?: string, price?: number, rooms?: number, title?: string, totalArea?: number, callback?:()=>void) => {
        this.stateActions.PlanCreateRequest()
        let response = await this.api.CreatePlan(file, bathRoomArea, bedRoomArea, kitchenArea, kvizId, livingArea, photo, price, rooms, title, totalArea)
        if (response instanceof Error) {
            this.stateActions.PlanDeleteError(response.message)
            return
        } else {
            this.stateActions.PlanCreateSuccess(response)
            if (callback) {
                callback()
            }
        }
    }

    public DeletePlan = async (planId: UniqueId) => {
        this.stateActions.PlanCreateRequest()
        let response = await this.api.DeletePlan(planId)
        if (response instanceof Error) {
            this.stateActions.PlanDeleteError(response.message)
            return
        } else {
            this.stateActions.PlanDeleteSuccess(planId)
        }
    }

}