import { Plan } from "../../../../domain/plan/plan";

export interface PlanInterface {
    PlansOfKviz(kvizId: UniqueId): Promise<Plan[] | Error>
    PlanList(): Promise<Plan[] | Error>
    CreatePlan(file: File, bathRoomArea?: number, bedRoomArea?: number, kitchenArea?: number, kvizId?: string, livingArea?: number, photo?: string, price?: number, rooms?: number, title?: string, totalArea?: number,): Promise<Plan | Error>
    UpdatePlan(bathRoomArea?: number, bedRoomArea?: number, kitchenArea?: number, kvizId?: string, livingArea?: number, photo?: string, price?: number, rooms?: number, title?: string, totalArea?: number,): Promise<Plan | Error>
    DeletePlan(id: UniqueId): Promise<string | Error>
}