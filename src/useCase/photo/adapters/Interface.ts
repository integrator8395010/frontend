import { Photo } from "../../../domain/photo/photo"


export interface PhotoInterface {
    PhotosOfKviz(kvizId: UniqueId): Promise<Photo[] | Error>
    PhotoList(): Promise<Photo[] | Error>
    CreatePhoto(file: File, kvizId?: string, ): Promise<Photo | Error>
    DeletePhoto(id: UniqueId): Promise<string | Error>
}