import { PhotosService } from "../../api/photos/photos";
import { actionCreators } from "../../state";

export class PhotoUseCases {
    private api: PhotosService;
    private stateActions: typeof actionCreators;

    constructor(api: PhotosService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public PhotosOfKviz = async (projectId: UniqueId) => {
        this.stateActions.SendPhotoListRequest()
        let response = await this.api.PhotosOfKviz(projectId)
        if (response instanceof Error) {
            this.stateActions.PhotoListError(response.message)
            return
        } else {
            this.stateActions.PhotoListSuccess(response)
        }
    }

    public CreatePhoto = async (file: File, kvizId: string, callback: ()=>void) => {
        this.stateActions.PhotoCreateRequest()
        let response = await this.api.CreatePhoto(file, kvizId)
        if (response instanceof Error) {
            this.stateActions.PhotoDeleteError(response.message)
            return
        } else {
            this.stateActions.PhotoCreateSuccess(response)
            callback()
        }
    }

    public DeletePhoto = async (id: UniqueId) => {
        this.stateActions.PhotoCreateRequest()
        let response = await this.api.DeletePhoto(id)
        if (response instanceof Error) {
            this.stateActions.PhotoDeleteError(response.message)
            return
        } else {
            this.stateActions.PhotoDeleteSuccess(id)
        }
    }

}