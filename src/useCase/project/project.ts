import { ProjectService } from "../../api/project/project";
import { actionCreators } from "../../state";

export class ProjectUseCases {
    private api: ProjectService;
    private stateActions: typeof actionCreators;

    constructor(api: ProjectService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public GetProjectList = async () => {
        this.stateActions.SendProjectListRequest()
        let response = await this.api.ProjectList()
        if (response instanceof Error) {
            this.stateActions.ProjectListError(response.message)
            return
        } else {
            this.stateActions.ProjectListSuccess(response)
        }
    }

    public CreateProject = async (name: string, file: File, callback: ()=>void) => {
        this.stateActions.ProjectCreateRequest()
        let response = await this.api.CreateProject(name, file)
        if (response instanceof Error) {
            this.stateActions.ProjectDeleteError(response.message)
            return
        } else {
            this.stateActions.ProjectCreateSuccess(response)
            callback()
        }
    }

}