import { Project } from "../../../../domain/project/project";

export interface ProjectInterface {
    ProjectList(): Promise<Project[] | Error>
    CreateProject(name: string, file: File): Promise<Project | Error>
    UpdateProject(id: UniqueId, name: string, file: File): Promise<Project | Error>
    DeleteProject(id: UniqueId): Promise<string | Error>
}