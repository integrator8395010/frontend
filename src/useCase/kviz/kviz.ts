import { KvizService } from "../../api/kviz/kviz";
import { actionCreators } from "../../state";

export class KvizUseCases {
    private api: KvizService;
    private stateActions: typeof actionCreators;

    constructor(api: KvizService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public KvizesOfProject = async (projectId: UniqueId) => {
        this.stateActions.SendKvizListRequest()
        let response = await this.api.KvizesOfProject(projectId)
        if (response instanceof Error) {
            this.stateActions.KvizListError(response.message)
            return
        } else {
            this.stateActions.KvizListSuccess(response)
        }
    }

    public CreateKviz = async (siteId: UniqueId, name: string, templateId: UniqueId, background: File, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean) => {
        this.stateActions.KvizCreateRequest()
        let response = await this.api.CreateKviz(siteId, name, templateId, background, mainColor, secondaryColor, subTitle, subTitleItems, phoneStepTitle, footerTitle, phone, politics, roistat, advantagesTitle, photosTitle, plansTitle, resultStepText, qoopler, dmpOne, validatePhone)
        if (response instanceof Error) {
            this.stateActions.KvizDeleteError(response.message)
            return
        } else {
            this.stateActions.KvizCreateSuccess(response)
        }
    }

    public CreateKvizComplex = async (kviz: { siteId: UniqueId,name: string,templateId: UniqueId,background: number[],mainColor: string,secondaryColor: string,subTitle: string,subTitleItems: string,phoneStepTitle: string,footerTitle: string,phone: string,politics: boolean,roistat: string,advantagesTitle: string,photosTitle: string,plansTitle: string,resultStepText: string,qoopler: boolean,yandex: string,google: string,mail: string,vk: string,dmpOne: string,validatePhone: boolean }, advantages: { title: string, photo: number[] }[], photos: number[][], plans: { title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number }[]) => {
        this.stateActions.KvizCreateRequest()
        let response = await this.api.CreateKvizComplex(kviz, advantages, photos, plans)
        if (response instanceof Error) {
            this.stateActions.KvizDeleteError(response.message)
            return
        } else {
            this.stateActions.KvizCreateSuccess(response)
        }
    }

    public DeleteKviz = async (id: UniqueId) => {
        this.stateActions.KvizCreateRequest()
        let response = await this.api.DeleteKviz(id)
        if (response instanceof Error) {
            this.stateActions.KvizDeleteError(response.message)
            return
        } else {
            this.stateActions.KvizDeleteSuccess(response)
        }
    }

    public UpdateKviz = async (id: UniqueId, name: string, siteId: UniqueId, templateId: UniqueId, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean, background?: File,) => {
        this.stateActions.KvizUpdateRequest()
        let response = await this.api.UpdateKviz(id,  siteId, name, templateId,  mainColor, secondaryColor, subTitle, subTitleItems, phoneStepTitle, footerTitle, phone, politics, roistat, advantagesTitle, photosTitle, plansTitle, resultStepText, qoopler, dmpOne, validatePhone, background)
        if (response instanceof Error) {
            this.stateActions.KvizUpdateError(response.message)
            return
        } else {
            this.stateActions.KvizUpdateSuccess(response)
        }
    }

}