import { Kviz } from "../../../domain/kviz/kviz"


export interface KvizInterface {
    KvizesOfProject(projectId: UniqueId): Promise<Kviz[] | Error>
    CreateKviz(siteId: UniqueId, name: string, templateId: UniqueId, background: File, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean): Promise<Kviz | Error>
    UpdateKviz(id: UniqueId, siteId: UniqueId, name: string, templateId: UniqueId, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean, background?: File,): Promise<Kviz | Error>
    DeleteKviz(id: UniqueId): Promise<string | Error>
}