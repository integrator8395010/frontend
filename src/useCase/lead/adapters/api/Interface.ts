import { Lead, LeadFilter } from "../../../../domain/lead";
import { LeadsStatistics } from "../../../../domain/lead/statistics";

export interface LeadInterface {
    LeadsOfProject(filter: LeadFilter, limit: number, offset: number): Promise<{totalCount: number, leads: Lead[]} | Error>
    GetLeadsStatisticsOfProject(projectId: UniqueId, siteUrl:string, from: string, to:string): Promise<LeadsStatistics | Error>
}