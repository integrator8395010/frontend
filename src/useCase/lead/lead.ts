import { LeadService } from "../../api/lead";
import { LeadFilter } from "../../domain/lead";
import { LeadsStatistics } from "../../domain/lead/statistics";
import { actionCreators } from "../../state";

export class LeadUseCases {
    private api: LeadService;
    private stateActions: typeof actionCreators;

    constructor(api: LeadService, stateActions: typeof actionCreators) {
        this.api = api;
        this.stateActions = stateActions;
    }

    public LeadsOfProject = async (filter: LeadFilter, offset: number, limit: number) => {
        this.stateActions.SendLeadListRequest()
        let response = await this.api.LeadsOfProject(filter, offset, limit)
        if (response instanceof Error) {
            this.stateActions.LeadListError(response.message)
            return
        } else {
            this.stateActions.LeadListSuccess(response.leads)
        }
    }

    public LeadsNextPage = async (filter: LeadFilter, offset: number, limit: number, page: number) => {
        this.stateActions.LeadRequestNewPage()
        let response = await this.api.LeadsOfProject(filter, offset, limit)
        if (response instanceof Error) {
            this.stateActions.LeadNewPageError(response.message)
            return
        } else {
            this.stateActions.LeadNewPageSuccess(response.leads, page)
        }
    }

    public GetLeadsStatisticsOfProject = async (projectId: UniqueId, siteUrl:string, from: string, to:string): Promise<LeadsStatistics | Error> => {
        this.stateActions.LeadRequestNewPage()
        let response = await this.api.GetLeadsStatisticsOfProject(projectId, siteUrl, from, to)
        /*if (response instanceof Error) {
            this.stateActions.LeadNewPageError(response.message)
            return
        } else {
            this.stateActions.LeadNewPageSuccess(response.leads, page)
        }*/
        return response
    }
}