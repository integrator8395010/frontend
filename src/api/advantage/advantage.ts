import axios from "axios";
import { AdvantageApi, Configuration } from "../generated";
import { AdvantageInterface } from "../../useCase/advantage/adapters/Interface";
import { Advantage } from "../../domain/advantage/advantage";

export class AdvantageService implements AdvantageInterface {
    private service: AdvantageApi;

    constructor() {
        this.service = new AdvantageApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async AdvantagesOfKviz(kvizId: UniqueId): Promise<Advantage[] | Error> {
        try {
            let response = await this.service.advantageIdGet( kvizId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let advantageList: Advantage[] = []
                response.data.advantageList?.forEach((advantage) => {
                    let item = new Advantage(advantage.id!, advantage.kvizId!, advantage.title!, advantage.photo!, advantage.createdAt!, advantage.modifiedAt!)
                    advantageList.push(item)
                })
                return advantageList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async AdvantageList(): Promise<Advantage[] | Error> {
        try {
            let response = await this.service.advantageGet({ headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let advantageList: Advantage[] = []
                response.data.advantageList?.forEach((advantage) => {
                    let item = new Advantage(advantage.id!, advantage.kvizId!, advantage.title!, advantage.photo!, advantage.createdAt!, advantage.modifiedAt!)
                    advantageList.push(item)
                })
                return advantageList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateAdvantage(file: File, kvizId?: string, title?: string, ): Promise<Advantage | Error> {
        try {
            let response = await this.service.advantagePut(file, kvizId, title,   { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let advantage = new Advantage(response.data.id!, response.data.kvizId!, response.data.title!, response.data.photo!, response.data.createdAt!, response.data.modifiedAt!)
                return advantage
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateAdvantage(id: string, photo?: string, title?: string,): Promise<Advantage | Error> {
        try {
            let response = await this.service.advantagePost({ id: id, photo: photo, title: title }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let advantage = new Advantage(response.data.id!, response.data.kvizId!, response.data.title!, response.data.photo!, response.data.createdAt!, response.data.modifiedAt!)
                return advantage
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteAdvantage(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.advantageIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}