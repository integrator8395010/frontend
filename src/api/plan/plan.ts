import axios from "axios";
import { PlanApi, Configuration } from "../generated";
import { PlanInterface } from "../../useCase/plan/adapters/api/Interface";
import { Plan } from "../../domain/plan/plan";

export class PlanService implements PlanInterface {
    private service: PlanApi;

    constructor() {
        this.service = new PlanApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async PlansOfKviz(kvizId: UniqueId): Promise<Plan[] | Error> {
        try {
            let response = await this.service.planIdGet( kvizId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let planList: Plan[] = []
                response.data.planList?.forEach((plan) => {
                    let item = new Plan(plan.id!, plan.kvizId!, plan.title!, plan.totalArea!, plan.livingArea!, plan.bedRoomArea!, plan.bathRoomArea!, plan.kitchenArea!, plan.rooms!, plan.price!, plan.photo!, plan.createdAt!, plan.modifiedAt!)
                    planList.push(item)
                })
                return planList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async PlanList(): Promise<Plan[] | Error> {
        try {
            let response = await this.service.planGet({ headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let planList: Plan[] = []
                response.data.planList?.forEach((plan) => {
                    let item = new Plan(plan.id!, plan.kvizId!, plan.title!, plan.totalArea!, plan.livingArea!, plan.bedRoomArea!, plan.bathRoomArea!, plan.kitchenArea!, plan.rooms!, plan.price!, plan.photo!, plan.createdAt!, plan.modifiedAt!)
                    planList.push(item)
                })
                return planList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreatePlan(file: File, bathRoomArea?: number, bedRoomArea?: number, kitchenArea?: number, kvizId?: string, livingArea?: number, photo?: string, price?: number, rooms?: number, title?: string, totalArea?: number,): Promise<Plan | Error> {
        try {
            let response = await this.service.planPut(file, bathRoomArea, bedRoomArea, kitchenArea, kvizId, livingArea, photo, price, rooms, title, totalArea,   { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let plan = new Plan(response.data.id!, response.data.kvizId!, response.data.title!, response.data.totalArea!, response.data.livingArea!, response.data.bedRoomArea!, response.data.bathRoomArea!, response.data.kitchenArea!, response.data.rooms!, response.data.price!, response.data.photo!, response.data.createdAt!, response.data.modifiedAt!)
                return plan
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdatePlan(bathRoomArea?: number, bedRoomArea?: number, kitchenArea?: number, kvizId?: string, livingArea?: number, photo?: string, price?: number, rooms?: number, title?: string, totalArea?: number,): Promise<Plan | Error> {
        try {
            let response = await this.service.planPost({ bathRoomArea: bathRoomArea, bedRoomArea: bedRoomArea, kitchenArea: kitchenArea, kvizId:kvizId, livingArea:livingArea, photo:photo, price: price, rooms:rooms, title:title, totalArea:totalArea }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let plan = new Plan(response.data.id!, response.data.kvizId!, response.data.title!, response.data.totalArea!, response.data.livingArea!, response.data.bedRoomArea!, response.data.bathRoomArea!, response.data.kitchenArea!, response.data.rooms!, response.data.price!, response.data.photo!, response.data.createdAt!, response.data.modifiedAt!)
                return plan
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeletePlan(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.planIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}