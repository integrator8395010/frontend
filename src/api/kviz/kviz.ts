import axios from "axios";
import { Configuration, KvizApi } from "../generated";
import { KvizInterface } from "../../useCase/kviz/adapters/Interface";
import { Kviz } from "../../domain/kviz/kviz";

export class KvizService implements KvizInterface {
    private service: KvizApi;

    constructor() {
        this.service = new KvizApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async KvizesOfProject(projectId: UniqueId): Promise<Kviz[] | Error> {
        try {
            let response = await this.service.kvizIdGet(projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let kvizList: Kviz[] = []
                response.data.kvizList?.forEach((kviz) => {
                    let item = new Kviz(kviz.id!, kviz.siteId!, kviz.name!, kviz.templateId!, kviz.background!, kviz.mainColor!, kviz.secondaryColor!, kviz.subTitle!, kviz.subTitleItems!, kviz.phoneStepTitle!, kviz.footerTitle!, kviz.phone!, kviz.politics!, kviz.roistat!, kviz.advantagesTitle!, kviz.photosTitle!, kviz.plansTitle!, kviz.resultStepText!, kviz.qoopler!, kviz.dmpOne!, kviz.validatePhone!, kviz.createdAt!, kviz.modifiedAt!)
                    kvizList.push(item)
                })
                return kvizList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateKviz(siteId: UniqueId, name: string, templateId: UniqueId, background: File, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean): Promise<Kviz | Error> {
        try {
            let response = await this.service.kvizPut(background, advantagesTitle, dmpOne, footerTitle, mainColor, name, phone, phoneStepTitle, photosTitle, plansTitle, politics, qoopler, resultStepText, roistat, secondaryColor, siteId, subTitle, subTitleItems, templateId, validatePhone, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let kviz = new Kviz(response.data.id!, response.data.siteId!, response.data.name!, response.data.templateId!, response.data.background!, response.data.mainColor!, response.data.secondaryColor!, response.data.subTitle!, response.data.subTitleItems!, response.data.phoneStepTitle!, response.data.footerTitle!, response.data.phone!, response.data.politics!, response.data.roistat!, response.data.advantagesTitle!, response.data.photosTitle!, response.data.plansTitle!, response.data.resultStepText!, response.data.qoopler!, response.data.dmpOne!, response.data.validatePhone!, response.data.createdAt!, response.data.modifiedAt!)

                return kviz
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateKvizComplex(kviz: { siteId: UniqueId,name: string,templateId: UniqueId,background: number[],mainColor: string,secondaryColor: string,subTitle: string,subTitleItems: string,phoneStepTitle: string,footerTitle: string,phone: string,politics: boolean,roistat: string,advantagesTitle: string,photosTitle: string,plansTitle: string,resultStepText: string,qoopler: boolean,yandex: string,google: string,mail: string,vk: string,dmpOne: string,validatePhone: boolean }, advantages: { title: string, photo: number[] }[], photos: number[][], plans: { title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number }[]): Promise<Kviz | Error> {
        console.log(kviz)
        try {
            let response = await this.service.kvizComplexPut({ site: {name: kviz.name, siteId: kviz.siteId, templateId: kviz.templateId, background: kviz.background, mainColor: kviz.mainColor, secondaryColor: kviz.secondaryColor, subTitle: kviz.subTitle, subTitleItems: kviz.subTitleItems, phoneStepTitle: kviz.phoneStepTitle, footerTitle: kviz.footerTitle, phone: kviz.phone, politics: kviz.politics, roistat: kviz.roistat, advantagesTitle: kviz.advantagesTitle, photosTitle: kviz.photosTitle, plansTitle: kviz.plansTitle, resultStepText: kviz.resultStepText, qoopler: kviz.qoopler, dmpOne: kviz.dmpOne, validatePhone: kviz.validatePhone, }, advantages: advantages, photos: photos, plans: plans, }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let kviz = new Kviz(response.data.id!, response.data.siteId!, response.data.name!, response.data.templateId!, response.data.background!, response.data.mainColor!, response.data.secondaryColor!, response.data.subTitle!, response.data.subTitleItems!, response.data.phoneStepTitle!, response.data.footerTitle!, response.data.phone!, response.data.politics!, response.data.roistat!, response.data.advantagesTitle!, response.data.photosTitle!, response.data.plansTitle!, response.data.resultStepText!, response.data.qoopler!, response.data.dmpOne!, response.data.validatePhone!, response.data.createdAt!, response.data.modifiedAt!)

                return kviz
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateKviz(id: UniqueId, siteId: UniqueId, name: string, templateId: UniqueId, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean, background?: File,): Promise<Kviz | Error> {
        try {
            let response = await this.service.kvizPost(advantagesTitle, background, dmpOne, footerTitle, id, mainColor, name, phone, phoneStepTitle, photosTitle, plansTitle, politics, qoopler, resultStepText, roistat, secondaryColor, siteId, subTitle, subTitleItems, templateId, validatePhone, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let kviz = new Kviz(response.data.id!, response.data.siteId!, response.data.name!, response.data.templateId!, response.data.background!, response.data.mainColor!, response.data.secondaryColor!, response.data.subTitle!, response.data.subTitleItems!, response.data.phoneStepTitle!, response.data.footerTitle!, response.data.phone!, response.data.politics!, response.data.roistat!, response.data.advantagesTitle!, response.data.photosTitle!, response.data.plansTitle!, response.data.resultStepText!, response.data.qoopler!, response.data.dmpOne!, response.data.validatePhone!, response.data.createdAt!, response.data.modifiedAt!)

                return kviz
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteKviz(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.kvizIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}