import { BitrixIntegration } from "../../domain/integration/bitrix-integration";
import { IntegrationAssociation, IntegrationBitrixAssociation, IntegrationBitrixDefaultValue, IntegrationDefaultValue } from "../generated";

export class AmocrmIntegrationAssociation implements IntegrationAssociation {
	public amocrm_field: number = 0;
    public lead_field: string = "";
}

export class AmocrmIntegrationDefaultValue implements IntegrationDefaultValue {
	public filed: number = 0;
    public value: any = "";
}


export class BitrixIntegrationAssociation implements IntegrationBitrixAssociation {
	public bitrix_field: string = "";
    public lead_field: string = "";
}

export class BitrixIntegrationDefaultValue implements IntegrationBitrixDefaultValue {
	public filed: string = "";
    public value: any = "";
}