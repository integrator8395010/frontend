import axios from "axios";
import { Configuration, IntegrationApi } from "../generated";
import { IntegrationInterface } from "../../useCase/integration/adapters/Interface";
import { MailIntegration } from "../../domain/integration/mail-integration";
import { AmocrmIntegration } from "../../domain/integration/amocrm-integration";
import { AmocrmAssociation, AmocrmDefaultValue, BitrixAssociation, BitrixDefaultValue } from "../../domain/integration/types";
import { AmocrmIntegrationAssociation, AmocrmIntegrationDefaultValue, BitrixIntegrationAssociation, BitrixIntegrationDefaultValue } from "./type";
import { BitrixIntegration } from "../../domain/integration/bitrix-integration";
import { Site } from "../../domain/site/site";
import { ProjectAmocrmIntegration } from "../../domain/integration/project-amocrm-integration";
import { ProjectBitrixIntegration } from "../../domain/integration/project-bitrix-integration";
import { LeadactivIntegration } from "../../domain/integration/leadactiv-integration";
import { LeadactivAmoSecrets } from "../../domain/integration/leadactiv-amo-secrets";

export class IntegrationService implements IntegrationInterface {
    private service: IntegrationApi;

    constructor() {
        this.service = new IntegrationApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async CreateMailIntegration(recipients: string[], siteId: UniqueId ): Promise<MailIntegration | Error> {
        try {
            let response = await this.service.integrationMailPut({recipients:recipients, siteId:siteId}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200 || response.status === 201) {
                let kviz = new MailIntegration(response.data.id!, response.data.siteId!, response.data.recipients!, response.data.createdAt!, response.data.modifiedAt!)

                return kviz
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateMailIntegration(id: UniqueId, recipients: string[], siteId: UniqueId): Promise<MailIntegration | Error> {
        try {
            let response = await this.service.integrationMailPost({id:id, recipients:recipients, siteId:siteId}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 201) {
                let kviz = new MailIntegration(response.data.id!, response.data.siteId!, response.data.recipients!, response.data.createdAt!, response.data.modifiedAt!)

                return kviz
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteMailIntegration(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.integrationMailIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadMailIntegration(id: UniqueId ): Promise<MailIntegration | Error> {
        try {
            let response = await this.service.integrationMailIdGet(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let mail = new MailIntegration(response.data.id!, response.data.siteId!, response.data.recipients!, response.data.createdAt!, response.data.modifiedAt!)

                return mail
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateAmocrmIntegration( siteId: UniqueId, pipelineId: number, statusId: number, responsible: number, unsorted: boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[]): Promise<AmocrmIntegration | Error> {
        try {
            let integrationAssociation: AmocrmIntegrationAssociation[] = []
            associations.forEach((association) => {
                let newAssociation = new AmocrmIntegrationAssociation()
                newAssociation.amocrm_field = association.AmocrmField()
                newAssociation.lead_field = association.LeadField()
                integrationAssociation.push(newAssociation)
            })

            let integrationDefaultValue: AmocrmIntegrationDefaultValue[] = []
            defaultValues.forEach((defaultValue) => {
                let newAssociation = new AmocrmIntegrationDefaultValue()
                newAssociation.filed = defaultValue.Field()
                newAssociation.value = defaultValue.Value()
                integrationDefaultValue.push(newAssociation)
            })


            let response = await this.service.integrationAmocrmPut({siteId:siteId, pipeline_id: pipelineId, status_id: statusId, responsible: responsible, unsorted: unsorted, associations: integrationAssociation, defaultValues: integrationDefaultValue, }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 201) {
                let associations: AmocrmAssociation[]
                response.data.associations!.forEach((association) => {
                    let newAssociation = new AmocrmAssociation(association.lead_field!, association.amocrm_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: AmocrmDefaultValue[]
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new AmocrmDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })

                let integration = new AmocrmIntegration(response.data.id!, response.data.siteId!, response.data.pipeline_id!, response.data.status_id!, response.data.responsible!, response.data.unsorted!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateAmocrmIntegration(id: UniqueId, siteId: UniqueId, pipelineId: number, statusId: number, responsible: number, unsorted: boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[]): Promise<AmocrmIntegration | Error> {
        try {
            let integrationAssociation: AmocrmIntegrationAssociation[] = []
            associations.forEach((association) => {
                let newAssociation = new AmocrmIntegrationAssociation()
                newAssociation.amocrm_field = association.AmocrmField()
                newAssociation.lead_field = association.LeadField()
                integrationAssociation.push(newAssociation)
            })

            let integrationDefaultValue: AmocrmIntegrationDefaultValue[] = []
            defaultValues.forEach((defaultValue) => {
                let newAssociation = new AmocrmIntegrationDefaultValue()
                newAssociation.filed = defaultValue.Field()
                newAssociation.value = defaultValue.Value()
                integrationDefaultValue.push(newAssociation)
            })


            let response = await this.service.integrationAmocrmPost({id:id, siteId:siteId, pipeline_id: pipelineId, status_id: statusId, responsible: responsible, unsorted: unsorted, associations: integrationAssociation, defaultValues: integrationDefaultValue, }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let associations: AmocrmAssociation[] = []
                response.data.associations!.forEach((association) => {
                    let newAssociation = new AmocrmAssociation(association.lead_field!, association.amocrm_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: AmocrmDefaultValue[] = []
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new AmocrmDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })

                let integration = new AmocrmIntegration(response.data.id!, response.data.siteId!, response.data.pipeline_id!, response.data.status_id!, response.data.responsible!, response.data.unsorted!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            console.log(e)
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteAmocrmIntegration(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.integrationAmocrmIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadProjectAmocrmIntegration(projectId: UniqueId ): Promise<ProjectAmocrmIntegration | Error> {
        try {
            let response = await this.service.integrationAmocrmProjectIdGet(projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new ProjectAmocrmIntegration(response.data.id!, response.data.projectId!, response.data.baseUrl!, response.data.clientId!, response.data.clientSecret!, response.data.redirectUri!, response.data.token!, response.data.refreshToken!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }


    async ReadAmocrmIntegration(id: UniqueId ): Promise<AmocrmIntegration | Error> {
        try {
            let response = await this.service.integrationAmocrmIdGet(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let associations: AmocrmAssociation[]
                response.data.associations!.forEach((association) => {
                    let newAssociation = new AmocrmAssociation(association.lead_field!, association.amocrm_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: AmocrmDefaultValue[]
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new AmocrmDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })

                let integration = new AmocrmIntegration(response.data.id!, response.data.siteId!, response.data.pipeline_id!, response.data.status_id!, response.data.responsible!, response.data.unsorted!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateProjectBitrixIntegration(projectId: UniqueId, baseUrl: string ): Promise<ProjectBitrixIntegration | Error> {
        try {

            let response = await this.service.integrationBitrixProjectPut({projectId:projectId, baseUrl:baseUrl}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 201) {

                let integration = new ProjectBitrixIntegration(response.data.id!, response.data.projectId!, response.data.baseUrl!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
           
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadProjectBitrixIntegration(projectId: UniqueId ): Promise<ProjectBitrixIntegration | Error> {
        try {
            let response = await this.service.integrationBitrixProjectIdGet(projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new ProjectBitrixIntegration(response.data.id!, response.data.projectId!, response.data.baseUrl!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateProjectBitrixIntegration(id: UniqueId, projectId: UniqueId, baseUrl: string ): Promise<ProjectBitrixIntegration | Error> {
        try {

            let response = await this.service.integrationBitrixProjectPost({id: id, projectId:projectId, baseUrl:baseUrl}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new ProjectBitrixIntegration(response.data.id!, response.data.projectId!, response.data.baseUrl!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteProjectBitrixIntegration(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.integrationBitrixProjectIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateBitrixIntegration(siteId: UniqueId, responsible: number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[] ): Promise<BitrixIntegration | Error> {
        try {
            let integrationAssociation: BitrixIntegrationAssociation[] = []
            associations.forEach((association) => {
                let newAssociation = new BitrixIntegrationAssociation()
                newAssociation.bitrix_field = association.BitrixField()
                newAssociation.lead_field = association.LeadField()
                integrationAssociation.push(newAssociation)
            })

            let integrationDefaultValue: BitrixIntegrationDefaultValue[] = []
            defaultValues.forEach((defaultValue) => {
                let newAssociation = new BitrixIntegrationDefaultValue()
                newAssociation.filed = defaultValue.Field()
                newAssociation.value = defaultValue.Field()
                integrationDefaultValue.push(newAssociation)
            })

            let response = await this.service.integrationBitrixPut({siteId:siteId, responsible: responsible, sendType:sendType, associations:integrationAssociation, defaultValues: integrationDefaultValue,  }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200 || response.status === 201) {
                let associations: BitrixAssociation[]
                response.data.associations!.forEach((association) => {
                    let newAssociation = new BitrixAssociation(association.lead_field!, association.bitrix_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: BitrixDefaultValue[]
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new BitrixDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })


                let integration = new BitrixIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.sendType!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateBitrixIntegration(id: UniqueId, siteId: UniqueId, responsible: number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[] ): Promise<BitrixIntegration | Error> {
        try {
            let integrationAssociation: BitrixIntegrationAssociation[] = []
            associations.forEach((association) => {
                let newAssociation = new BitrixIntegrationAssociation()
                newAssociation.bitrix_field = association.BitrixField()
                newAssociation.lead_field = association.LeadField()
                integrationAssociation.push(newAssociation)
            })

            let integrationDefaultValue: BitrixIntegrationDefaultValue[] = []
            defaultValues.forEach((defaultValue) => {
                let newDefault = new BitrixIntegrationDefaultValue()
                newDefault.filed = defaultValue.Field()
                newDefault.value = defaultValue.Value()
                integrationDefaultValue.push(newDefault)
            })

            let response = await this.service.integrationBitrixPost({id: id, siteId:siteId, responsible: responsible, sendType: sendType, associations:integrationAssociation, defaultValues: integrationDefaultValue,  }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let associations: BitrixAssociation[] = []
                response.data.associations!.forEach((association) => {
                    let newAssociation = new BitrixAssociation(association.lead_field!, association.bitrix_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: BitrixDefaultValue[] = []
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new BitrixDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })


                let integration = new BitrixIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.sendType!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            console.log(e)
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteBitrixIntegration(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.integrationBitrixIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadBitrixIntegration(id: UniqueId ): Promise<BitrixIntegration | Error> {
        try {
            let response = await this.service.integrationBitrixIdGet(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let associations: BitrixAssociation[]
                response.data.associations!.forEach((association) => {
                    let newAssociation = new BitrixAssociation(association.lead_field!, association.bitrix_field!)
                    associations.push(newAssociation)
                })

                let defaultValues: BitrixDefaultValue[]
                response.data.defaultValues!.forEach((item) => {
                    let newAssociation = new BitrixDefaultValue(item.filed!, item.value!)
                    defaultValues.push(newAssociation)
                })


                let integration = new BitrixIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.sendType!, associations!, defaultValues!, response.data.createdAt!, response.data.modifiedAt!)
                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadIntegrationsOfSite(id: UniqueId): Promise<{bitrix: BitrixIntegration | null, amocrm: AmocrmIntegration | null, mail: MailIntegration | null, leadactiv: LeadactivIntegration | null } | Error> {
        try {
            let response = await this.service.integrationListIdGet(id, { headers: { "Authorization": localStorage.getItem("token") } })
            if (response.status === 200) {
                let bitrix: BitrixIntegration | null = null
                if (response.data.bitrix) {
                    let associations: BitrixAssociation[] = []
                    response.data.bitrix.associations!.forEach((association) => {
                        let newAssociation = new BitrixAssociation(association.lead_field!, association.bitrix_field!)
                        associations.push(newAssociation)
                    })

                    let defaultValues: BitrixDefaultValue[] = []
                    response.data.bitrix.defaultValues!.forEach((item) => {
                        let newAssociation = new BitrixDefaultValue(item.filed!, item.value!)
                        defaultValues.push(newAssociation)
                    })


                    bitrix = new BitrixIntegration(response.data.bitrix.id!, response.data.bitrix.siteId!, response.data.bitrix.responsible!, response.data.bitrix.sendType!, associations!, defaultValues!, response.data.bitrix.createdAt!, response.data.bitrix.modifiedAt!)
                }

                let leadactiv: LeadactivIntegration | null = null
                if (response.data.leadactiv) {
                    leadactiv = new LeadactivIntegration(response.data.leadactiv.id!, response.data.leadactiv.siteId!, response.data.leadactiv.responsible!, response.data.leadactiv.createdAt!, response.data.leadactiv.modifiedAt!)
                }

                let amocrm: AmocrmIntegration | null = null
                if (response.data.amocrm) {
                    let associations: AmocrmAssociation[] = []
                    response.data.amocrm.associations!.forEach((association) => {
                        let newAssociation = new AmocrmAssociation(association.lead_field!, association.amocrm_field!)
                        associations.push(newAssociation)
                    })
    
                    let defaultValues: AmocrmDefaultValue[] = []
                    response.data.amocrm.defaultValues!.forEach((item) => {
                        let newAssociation = new AmocrmDefaultValue(item.filed!, item.value!)
                        defaultValues.push(newAssociation)
                    })
    
                    amocrm = new AmocrmIntegration(response.data.amocrm.id!, response.data.amocrm.siteId!, response.data.amocrm.pipeline_id!, response.data.amocrm.status_id!, response.data.amocrm.responsible!, response.data.amocrm.unsorted!, associations!, defaultValues!, response.data.amocrm.createdAt!, response.data.amocrm.modifiedAt!)
                }

                let mail: MailIntegration | null = null

                if (response.data.mail) {
                    mail = new MailIntegration(response.data.mail.id!, response.data.mail.siteId!, response.data.mail.recipients!, response.data.mail.createdAt!, response.data.mail.modifiedAt!)
                }

                return {bitrix: bitrix, amocrm: amocrm, mail: mail, leadactiv: leadactiv, }
            } else {
                console.log('error')
                if (axios.isAxiosError(response)) {
                    
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

           
            return Error(error)
        }
    }

    async SitesWithIntegrations(projectId: UniqueId): Promise<Site[] | Error> {
        try {
            let response = await this.service.integrationSitesTrueIdGet( projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let siteList: Site[] = []
                response.data.list?.forEach((site) => {
                    let item = new Site(site.id!, site.name!, site.url!, site.projectId!, site.createdBy!, site.createdAt!, site.modifiedAt!)
                    siteList.push(item)
                })
                return siteList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async SitesWithoutIntegrations(projectId: UniqueId): Promise<Site[] | Error> {
        try {
            let response = await this.service.integrationSitesFalseIdGet( projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let siteList: Site[] = []
                response.data.list?.forEach((site) => {
                    let item = new Site(site.id!, site.name!, site.url!, site.projectId!, site.createdBy!, site.createdAt!, site.modifiedAt!)
                    siteList.push(item)
                })
                return siteList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadFieldsOfAmoCrmLead(integrationId: UniqueId, page: number): Promise<any | Error> {
        try {
            let response = await this.service.integrationAmocrmFieldsIdPageGet( integrationId, page.toString(), { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return response.data
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadUsersOfAmoCrm(integrationId: UniqueId,): Promise<any | Error> {
        try {
            let response = await this.service.integrationAmocrmUsersIdGet( integrationId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return response.data
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadPipelinesOfAmoCrm(integrationId: UniqueId,): Promise<any | Error> {
        try {
            let response = await this.service.integrationAmocrmPipelinesIdGet( integrationId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return response.data
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadStatusesOfPipelineAmoCrm(integrationId: UniqueId, pipelineId: number): Promise<any | Error> {
        try {
            console.log(pipelineId.toString())
            let response = await this.service.integrationAmocrmStatusesIdPipelineIdGet( integrationId, pipelineId.toString(), { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return response.data
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateLeadactivIntegration(siteId: UniqueId, responsible: number ): Promise<LeadactivIntegration | Error> {
        try {

            let response = await this.service.integrationLeadactivPut({siteId:siteId, responsible:responsible}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 201) {

                let integration = new LeadactivIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
           
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateLeadactivIntegration(id: UniqueId, siteId: UniqueId, responsible: number ): Promise<LeadactivIntegration | Error> {
        try {

            let response = await this.service.integrationLeadactivPost({id: id, siteId:siteId, responsible:responsible}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new LeadactivIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadLeadactivIntegration(id: UniqueId ): Promise<LeadactivIntegration | Error> {
        try {
            let response = await this.service.integrationLeadactivIdGet(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new LeadactivIntegration(response.data.id!, response.data.siteId!, response.data.responsible!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteLeadactivIntegration(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.integrationLeadactivIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadUsersOfLeadactiv(page: number): Promise<any | Error> {
        try {
            let response = await this.service.integrationLeadactivUsersPageGet(page.toString(), { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                return response.data
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async ReadLeadactivAmoSecrets(): Promise<LeadactivAmoSecrets | Error> {
        try {
            let response = await this.service.integrationAmocrmLeadactivGet({ headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {

                let integration = new LeadactivAmoSecrets(response.data.id!, response.data.baseUrl!, response.data.clientId!, response.data.clientSecret!, response.data.redirectUri!, response.data.token!, response.data.refreshToken!, response.data.createdAt!, response.data.modifiedAt!)

                return integration
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}