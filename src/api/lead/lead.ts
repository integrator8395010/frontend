import axios from "axios";
import { LeadApi, Configuration } from "../generated";
import { Lead, LeadFilter } from "../../domain/lead";
import { LeadInterface } from "../../useCase/lead/adapters/api/Interface";
import { DayStatistic, LeadsStatistics } from "../../domain/lead/statistics";

export class LeadService implements LeadInterface {
    private service: LeadApi;

    constructor() {
        this.service = new LeadApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async LeadsOfProject(filter: LeadFilter, offset: number, limit: number,): Promise<{totalCount: number, leads: Lead[]} | Error> {
        try {
            let response = await this.service.leadListPost( {filter: {from: filter.From(), to: filter.To(), phone: filter.Phone(), projectId: filter.ProjectId(), source: filter.Source(), url: filter.Url()}, limit: limit, offset: offset}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200 || response.status === 201) {
                let leadList: Lead[] = []
                response.data.leads?.forEach((lead) => {
                    let item = new Lead(lead.id!, lead.url!, lead.name!, lead.phone!, lead.ip!, lead.comment!, lead.userAgent!, lead.utmCampaign!, lead.utmContent!, lead.utmMedium!, lead.utmSource!, lead.utmTerm!, lead.roistat!, lead.yclid!, lead.fbid!,  lead.gclid!, lead.source!, lead.siteId!, lead.createdAt!, lead.modifiedAt! )
                    leadList.push(item)
                })
                return {totalCount: response.data.totalItems!, leads: leadList}
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async GetLeadsStatisticsOfProject(projectId: UniqueId, siteUrl:string, from: string, to:string): Promise<LeadsStatistics | Error> {
        try {
            let response = await this.service.leadStatisticsPost( {projectId: projectId, siteUrl:siteUrl, from:from, to: to}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200 || response.status === 201) {
                let days = new Map<Date, DayStatistic>()
                Object.keys(response.data.days!).forEach((date)=>{
                    let dateFromServer = new Date(date)
                    let dayStat = response.data.days![date]
                    days.set(dateFromServer, new DayStatistic(dayStat.leadsCount!, dayStat.siteLeadsCount!, dayStat.marquizLeadsCount!, dayStat.tildaLeadsCount!, dayStat.flexbeLeadsCount!, dayStat.spamLeadsCount!)) 
                })
            
                let leadsStatistics: LeadsStatistics = new LeadsStatistics(response.data.projectId!, response.data.siteUrl!, days, response.data.from!, response.data.to!)
               
                return leadsStatistics
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}