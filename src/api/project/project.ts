import axios from "axios";
import { ProjectApi, Configuration } from "../generated";
import { ProjectInterface } from "../../useCase/project/adapters/api/Interface";
import { Project } from "../../domain/project/project";

export class ProjectService implements ProjectInterface {
    private service: ProjectApi;

    constructor () {
        this.service = new ProjectApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async ProjectList(): Promise<Project[] | Error> {
        try {
            let response = await this.service.projectGet({headers: {"Authorization": localStorage.getItem("token")}})
           
            if (response.status === 200) {
                let projectList:Project[] = []
                response.data.projectList?.forEach((project)=>{
                    let item = new Project(project.id!, project.name!, project.photo!, project.createdBy!, project.createdAt!, project.modifiedAt!)
                    projectList.push(item)
                })
                return projectList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch(e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }
            
            return Error(error)
        }
    }

    async CreateProject(name: string, file: File): Promise<Project | Error> {
        try {
            let response = await this.service.projectPut(file, name, {headers: {"Authorization": localStorage.getItem("token")}})
           
            if (response.status === 200) {
                let project = new Project(response.data.id!, response.data.name!, response.data.photo!, response.data.createdBy!, response.data.createdAt!, response.data.modifiedAt!)
                    
                return project
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch(e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }
            
            return Error(error)
        }
    }

    async UpdateProject(id: UniqueId, name: string, file: File): Promise<Project | Error> {
        try {
            let response = await this.service.projectPost({id: id, name: name}, {headers: {"Authorization": localStorage.getItem("token")}})
           
            if (response.status === 200) {
                let project = new Project(response.data.id!, response.data.name!, response.data.photo!, response.data.createdBy!, response.data.createdAt!, response.data.modifiedAt!)
                    
                return project
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch(e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }
            
            return Error(error)
        }
    }

    async DeleteProject(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.projectIdDelete(id, {headers: {"Authorization": localStorage.getItem("token")}})
           
            if (response.status === 200) {    
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch(e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }
            
            return Error(error)
        }
    }
}