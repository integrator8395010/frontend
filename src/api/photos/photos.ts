import axios from "axios";
import { PhotoApi, Configuration } from "../generated";
import { PhotoInterface } from "../../useCase/photo/adapters/Interface";
import { Photo } from "../../domain/photo/photo";

export class PhotosService implements PhotoInterface {
    private service: PhotoApi;

    constructor() {
        this.service = new PhotoApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async PhotosOfKviz(kvizId: UniqueId): Promise<Photo[] | Error> {
        try {
            let response = await this.service.photoIdGet( kvizId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let photoList: Photo[] = []
                response.data.photosList?.forEach((photo) => {
                    let item = new Photo(photo.id!, photo.kvizId!, photo.photo!, photo.createdAt!, photo.modifiedAt!)
                    photoList.push(item)
                })
                return photoList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async PhotoList(): Promise<Photo[] | Error> {
        try {
            let response = await this.service.photoGet({ headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let photoList: Photo[] = []
                response.data.photosList?.forEach((photo) => {
                    let item = new Photo(photo.id!, photo.kvizId!, photo.photo!, photo.createdAt!, photo.modifiedAt!)
                    photoList.push(item)
                })
                return photoList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreatePhoto(file: File, kvizId?: string, ): Promise<Photo | Error> {
        try {
            let response = await this.service.photoPut(file, kvizId,   { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let photo = new Photo(response.data.id!, response.data.kvizId!, response.data.photo!, response.data.createdAt!, response.data.modifiedAt!)
                return photo
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeletePhoto(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.photoIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}