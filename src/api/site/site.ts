import axios from "axios";
import { SiteApi, Configuration } from "../generated";
import { SiteInterface } from "../../useCase/site/adapters/api/Interface";
import { Site } from "../../domain/site/site";

export class SiteService implements SiteInterface {
    private service: SiteApi;

    constructor() {
        this.service = new SiteApi(new Configuration(), process.env.REACT_APP_BACKEND_URL)
    }

    async SiteById(siteId: UniqueId): Promise<Site | Error> {
        try {
            let response = await this.service.siteIdGet( siteId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let site = new Site( response.data.id!,  response.data.name!,  response.data.url!,  response.data.projectId!,  response.data.createdBy!,  response.data.createdAt!,  response.data.modifiedAt!)
                return site
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async SitesOfProject(projectId: UniqueId): Promise<Site[] | Error> {
        try {
            let response = await this.service.siteProjectIdGet( projectId, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let siteList: Site[] = []
                response.data.siteList?.forEach((site) => {
                    let item = new Site(site.id!, site.name!, site.url!, site.projectId!, site.createdBy!, site.createdAt!, site.modifiedAt!)
                    siteList.push(item)
                })
                return siteList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async SiteList(): Promise<Site[] | Error> {
        try {
            let response = await this.service.siteGet({ headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let siteList: Site[] = []
                response.data.siteList?.forEach((site) => {
                    let item = new Site(site.id!, site.name!, site.url!, site.projectId!, site.createdBy!, site.createdAt!, site.modifiedAt!)
                    siteList.push(item)
                })
                return siteList
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async CreateSite(name: string, projectId: UniqueId, url: string,): Promise<Site | Error> {
        try {
            let response = await this.service.sitePut({name:name, projectId:projectId, url:url}, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let site = new Site(response.data.id!, response.data.name!, response.data.url!, response.data.projectId!, response.data.createdBy!, response.data.createdAt!, response.data.modifiedAt!)

                return site
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async UpdateSite(id: UniqueId, name: string, projectId: UniqueId, url: string,): Promise<Site | Error> {
        try {
            let response = await this.service.sitePost({ id: id, name: name, projectId: projectId, url:url, }, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                let site = new Site(response.data.id!, response.data.name!, response.data.url!, response.data.projectId!, response.data.createdBy!, response.data.createdAt!, response.data.modifiedAt!)

                return site
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }

    async DeleteSite(id: UniqueId): Promise<string | Error> {
        try {
            let response = await this.service.siteIdDelete(id, { headers: { "Authorization": localStorage.getItem("token") } })

            if (response.status === 200) {
                return "success"
            } else {
                if (axios.isAxiosError(response)) {
                    return Error(response?.message)
                }
                return Error("something went wrong")
            }
        } catch (e) {
            let error: string = ""
            if (axios.isAxiosError(e)) {
                error = e.response?.data.message
            }

            return Error(error)
        }
    }
}