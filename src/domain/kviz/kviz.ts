import { KvizName } from "./types";

export class Kviz {
    private id: UniqueId;
    private name: KvizName;
    private siteId: UniqueId;
    private templateId: UniqueId;
    private background: string;
    private mainColor: string;
    private secondaryColor: string;
    private subTitle: string;
    private subTitleItems: string;
    private phoneStepTitle: string;
    private footerTitle: string;
    private phone: string;
    private politics: boolean;
    private roistat: string;
    private advantagesTitle: string;
    private photosTitle: string;
    private plansTitle: string;
    private resultStepText: string;
    private qoopler: boolean;
    private dmpOne: string;
    private validatePhone: boolean;
    //private createdBy: UniqueId;
    private createdAt: string;
    private modifiedAt: string;




 constructor(id: UniqueId, siteId: UniqueId, name: KvizName, templateId: UniqueId, background: string, mainColor: string, secondaryColor: string, subTitle: string, subTitleItems: string, phoneStepTitle: string, footerTitle: string, phone: string, politics: boolean, roistat: string, advantagesTitle: string, photosTitle: string, plansTitle: string, resultStepText: string, qoopler: boolean, dmpOne: string, validatePhone: boolean, createdAt: string, modifiedAt: string) {
        this.id = id
        this.siteId = siteId
        this.name = name
        this.templateId = templateId
        this.background = background
        this.mainColor = mainColor
        this.secondaryColor = secondaryColor
        this.subTitle = subTitle
        this.subTitleItems = subTitleItems
        this.phoneStepTitle = phoneStepTitle
        this.footerTitle = footerTitle
        this.phone = phone
        this.politics = politics
        this.roistat = roistat
        this.advantagesTitle = advantagesTitle
        this.photosTitle = photosTitle
        this.plansTitle = plansTitle
        this.resultStepText = resultStepText
        this.qoopler = qoopler
        this.dmpOne = dmpOne
        this.validatePhone = validatePhone
        /*this.createdBy = createdBy*/
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public SiteId = (): string => {
        return this.siteId
    }

    public Name = (): string => {
        return this.name
    }

    public TemplateId = (): string => {
        return this.templateId
    }

    public Background = (): string => {
        return this.background;
    }

    public MainColor = (): string => {
        return this.mainColor;
    }

    public SecondaryColor = (): string => {
        return this.secondaryColor;
    }

    public SubTitle = (): string => {
        return this.subTitle;
    }

    public SubTitleItems = (): string => {
        return this.subTitleItems;
    }

    public PhoneStepTitle = (): string => {
        return this.phoneStepTitle;
    }

    public FooterTitle = (): string => {
        return this.footerTitle;
    }

    public Phone = (): string => {
        return this.phone;
    }

    public Politics = (): boolean => {
        return this.politics;
    }

    public Roistat = (): string => {
        return this.roistat;
    }

    public AdvantagesTitle = (): string => {
        return this.advantagesTitle;
    }

    public PhotosTitle = (): string => {
        return this.photosTitle;
    }

    public PlansTitle = (): string => {
        return this.plansTitle;
    }

    public ResultStepText = (): string => {
        return this.resultStepText;
    }

    public Qoopler = (): boolean => {
        return this.qoopler;
    }

    public DmpOne = (): string => {
        return this.dmpOne;
    }

    public ValidatePhone = (): boolean => {
        return this.validatePhone;
    }


    /*public CreatedBy = (): string => {
        return this.createdBy
    }*/

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}