import { Token } from "./types";

export class Authorization {
    private token: Token | null;
    private refreshToken: Token | null;

    constructor() {
        this.token = null
        this.refreshToken = null
    }

    public Token = (): Token | null => {
        return this.token
    }

    public Refreshtoken = (): Token | null => {
        return this.refreshToken
    }

    public IsAuthorized = (): boolean => {
        return this.token !== null;
    }

    private isTokenExpired = (): boolean => {
        return true
    }
}