export const LeadFieldNamesList = [
    "id",
    "url",
    "name",
    "phone",
    "ip",
    "comment",
    "userAgent",
    "utmCampaign",
    "utmContent",
    "utmMedium",
    "utmSource",
    "utmTerm",
    "roistat",
    "yclid",
    "gclid",
]

export class Lead {
    private id: UniqueId;
    private url: string;
    private name: string;
    private phone: string;
    private ip: string;
    private comment: string;
    private userAgent: string;
    private utmCampaign: string;
    private utmContent: string;
    private utmMedium: string;
    private utmSource: string;
    private utmTerm: string;
    private roistat: number;
    private yclid: string;
    private fbid: string;
    private gclid: string;
    private source: string;
    private siteId: string;
    private createdAt: string;
    private modifiedAt: string;

    


     
    constructor(id: UniqueId, url: string, name: string, phone: string, ip: string, comment: string, userAgent: string, utmCampaign: string, utmContent: string, utmMedium: string, utmSource: string, utmTerm: string, roistat: number, yclid: string, fbid: string, gclid: string, source: string, siteId: string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.url = url
        this.name = name
        this.phone = phone
        this.ip = ip
        this.comment = comment
        this.userAgent = userAgent
        this.utmCampaign = utmCampaign
        this.utmContent = utmContent
        this.utmMedium = utmMedium
        this.utmSource = utmSource
        this.utmTerm = utmTerm
        this.roistat = roistat
        this.yclid = yclid
        this.fbid = fbid
        this.gclid = gclid
        this.source = source
        this.siteId = siteId
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public Url = (): string => {
        return this.url
    }

    public Name = (): string => {
        return this.name
    }

    public Phone = (): string => {
        return this.phone
    }

    public Ip = (): string => {
        return this.ip
    }

    public Comment = (): string => {
        return this.comment
    }

    public UserAgent = (): string => {
        return this.userAgent
    }

    public UtmCampaign = (): string => {
        return this.utmCampaign
    }

    public UtmContent = (): string => {
        return this.utmContent
    }

    public UtmMedium = (): string => {
        return this.utmMedium
    }

    public UtmSource = (): string => {
        return this.utmSource
    }

    public UtmTerm = (): string => {
        return this.utmTerm
    }

    public Roistat = (): number => {
        return this.roistat
    }

    public Yclid = (): string => {
        return this.yclid
    }

    public Fbid = (): string => {
        return this.fbid
    }

    public Gclid = (): string => {
        return this.gclid
    }

    public Source = (): string => {
        return this.source
    }

    public SiteId = (): string => {
        return this.siteId
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}