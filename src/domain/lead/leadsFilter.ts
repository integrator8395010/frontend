export class LeadFilter {
    private projectId: UniqueId;
    private url: string;
    private phone: string;
    private source: string;
    private from: string;
    private to: string;

    


     
    constructor(projectId: UniqueId, url: string, phone: string, source: string, from: string, to: string) {
        this.projectId = projectId
        this.url = url
        this.phone = phone
        this.source = source
        this.from = from
        this.to = to
    }

    public ProjectId = (): UniqueId => {
        return this.projectId
    }

    public Url = (): string => {
        return this.url
    }

    public Phone = (): string => {
        return this.phone
    }

    public Source = (): string => {
        return this.source
    }

    public From = (): string => {
        return this.from
    }

    public To = (): string => {
        return this.to
    }
}