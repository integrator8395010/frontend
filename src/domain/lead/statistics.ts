export class LeadsStatistics {
    private projectId: UniqueId;
    private url: string;
    private from: string;
    private days: Map<Date, DayStatistic>
    private to: string;

    


     
    constructor(projectId: UniqueId, url: string, days: Map<Date, DayStatistic>, from: string, to: string) {
        this.projectId = projectId
        this.url = url
        this.days = days
        this.from = from
        this.to = to
    }

    public ProjectId = (): UniqueId => {
        return this.projectId
    }

    public Url = (): string => {
        return this.url
    }

    public Days = (): Map<Date, DayStatistic> => {
        return this.days
    }

    public From = (): string => {
        return this.from
    }

    public To = (): string => {
        return this.to
    }
}

export class DayStatistic {
	private leadsCount: number;
	private siteLeadsCount: number;
	private marquizLeadsCount: number;
	private tildaLeadsCount: number;
	private flexbeLeadsCount: number;
	private spamLeadsCount: number;

    constructor(leadsCount: number, siteLeadsCount: number, marquizLeadsCount: number, tildaLeadsCount: number, flexbeLeadsCount: number, spamLeadsCount: number,) {
        this.leadsCount = leadsCount
        this.siteLeadsCount = siteLeadsCount
        this.marquizLeadsCount = marquizLeadsCount
        this.tildaLeadsCount = tildaLeadsCount
        this.flexbeLeadsCount = flexbeLeadsCount
        this.spamLeadsCount = spamLeadsCount
    }

    public LeadsCount = (): number => {
        return this.leadsCount
    }

    public SiteLeadsCount = (): number => {
        return this.siteLeadsCount
    }

    public MarquizLeadsCount = (): number => {
        return this.marquizLeadsCount
    }

    public TildaLeadsCount = (): number => {
        return this.tildaLeadsCount
    }

    public FlexbeLeadsCount = (): number => {
        return this.flexbeLeadsCount
    }

    public SpamLeadsCount = (): number => {
        return this.spamLeadsCount
    }
}