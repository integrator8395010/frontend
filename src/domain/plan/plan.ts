export class Plan {
    private id: UniqueId;
    private kvizId: string;
    private title: string;
    private totalArea: number;
    private livingArea: number;
    private bedRoomArea: number;
    private bathRoomArea: number;
    private kitchenArea: number;
    
    private photo: string;
    private rooms: number;
    private price: number;

    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, kvizId: string, title: string, totalArea: number, livingArea: number, bedRoomArea: number, bathRoomArea: number, kitchenArea: number, rooms: number, price: number, photo: string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.kvizId = kvizId
        this.photo = photo
        this.title = title
        this.totalArea = totalArea
        this.livingArea = livingArea
        this.bedRoomArea = bedRoomArea
        this.bathRoomArea = bathRoomArea
        this.kitchenArea = kitchenArea
        this.rooms = rooms
        this.price = price
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Title = (): string => {
        return this.title
    }

    public TotalArea = (): number => {
        return this.totalArea
    }

    public LivingArea = (): number => {
        return this.livingArea
    }

    public BedRoomArea = (): number => {
        return this.bedRoomArea
    }

    public BathRoomArea = (): number => {
        return this.bathRoomArea
    }

    public KitchenArea = (): number => {
        return this.kitchenArea
    }

    public Rooms = (): number => {
        return this.rooms
    }

    public Price = (): number => {
        return this.price
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public KvizId = (): string => {
        return this.kvizId
    }

    public Photo = (): string => {
        return this.photo
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}