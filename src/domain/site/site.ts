export class Site {
    private id: UniqueId;
    private name: string;
    private url: string;
    private projectId: string;
    private createdBy: string;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, name: string, url: string, projectId:string, createdBy:string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.name = name
        this.url = url
        this.projectId = projectId
        this.createdBy = createdBy
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public Name = (): string => {
        return this.name
    }

    public Url = (): string => {
        return this.url
    }

    public ProjectId = (): string => {
        return this.projectId;
    }

    public CreatedBy = (): string => {
        return this.createdBy
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}