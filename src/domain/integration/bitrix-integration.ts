import { BitrixAssociation, BitrixDefaultValue } from "./types";

export class BitrixIntegration {
    private id: UniqueId;
    private siteId: string;
    private responsible: number;
    private sendType: string;
    private associations: BitrixAssociation[];
    private defaultValues: BitrixDefaultValue[];
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, siteId: UniqueId, responsible:number, sendType: string, associations: BitrixAssociation[], defaultValues: BitrixDefaultValue[], createdAt: string, modifiedAt: string) {
        this.id = id
        this.siteId = siteId
        this.responsible = responsible
        this.sendType = sendType
        this.associations = associations
        this.defaultValues = defaultValues
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public SiteId = (): string => {
        return this.siteId
    }

    public Responsible = (): number => {
        return this.responsible
    }

    public SendType = (): string => {
        return this.sendType
    }

    public Associations = (): BitrixAssociation[] => {
        return this.associations
    }

    public DefaultValues = (): BitrixDefaultValue[] => {
        return this.defaultValues
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}