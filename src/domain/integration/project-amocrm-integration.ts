import { AmocrmAssociation, AmocrmDefaultValue } from "./types";

export class ProjectAmocrmIntegration {
    private id: UniqueId;
    private projectId: string;
    private baseUrl: string;
    private clientId: string;
    private clientSecret: string;
    private redirectUri: string;
    private token: string;
    private refreshToken: string;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, projectId: UniqueId, baseUrl: string, clientId: string, clientSecret: string, redirectUri:string, token:string, refreshToken: string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.projectId = projectId
        this.baseUrl = baseUrl
        this.clientId = clientId
        this.clientSecret = clientSecret
        this.redirectUri = redirectUri
        this.token = token
        this.refreshToken = refreshToken
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public ProjectId = (): string => {
        return this.projectId
    }

    public BaseUrl = (): string => {
        return this.baseUrl
    }
    
    public ClientId = (): string => {
        return this.clientId
    }

    public ClientSecret = (): string => {
        return this.clientSecret
    }

    public RedirectUri = (): string => {
        return this.redirectUri
    }

    public Token = (): string => {
        return this.token
    }

    public RefreshToken = (): string => {
        return this.refreshToken
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}