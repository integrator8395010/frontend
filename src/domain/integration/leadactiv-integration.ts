export class LeadactivIntegration {
    private id: UniqueId;
    private siteId: string;
    private responsible: number;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, siteId: UniqueId, responsible:number, createdAt: string, modifiedAt: string) {
        this.id = id
        this.siteId = siteId
        this.responsible = responsible
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public SiteId = (): string => {
        return this.siteId
    }

    public Responsible = (): number => {
        return this.responsible
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}