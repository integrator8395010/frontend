export class MailIntegration {
    private id: UniqueId;
    private siteId: string;
    private recipients: string[];
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, siteId: string, recipients: string[], createdAt: string, modifiedAt: string) {
        this.id = id
        this.siteId = siteId
        this.recipients = recipients
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public SiteId = (): string => {
        return this.siteId
    }

    public Recipients = (): string[] => {
        return this.recipients
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}