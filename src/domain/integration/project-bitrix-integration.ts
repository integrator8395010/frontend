export class ProjectBitrixIntegration {
    private id: UniqueId;
    private projectId: string;
    private baseUrl: string;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, projectId: UniqueId, baseUrl: string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.projectId = projectId
        this.baseUrl = baseUrl
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public ProjectId = (): UniqueId => {
        return this.projectId
    }

    public BaseUrl = (): string => {
        return this.baseUrl
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}