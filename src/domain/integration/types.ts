export class AmocrmAssociation {
    private leadField: string;
    private amocrmField: number;
    constructor(leadField: string, amocrmField: number) {
        this.leadField = leadField
        this.amocrmField = amocrmField
    }

    public LeadField = (): string => {
        return this.leadField
    }

    public AmocrmField = (): number => {
        return this.amocrmField
    }
}

export class BitrixAssociation {
    private leadField: string;
    private bitrixField: string;

    constructor(leadField: string, bitrixField: string) {
        this.leadField = leadField
        this.bitrixField = bitrixField
    }

    public LeadField = (): string => {
        return this.leadField
    }

    public BitrixField = (): string => {
        return this.bitrixField
    }
}

export class AmocrmDefaultValue {
    private field: number;
    private value: any;
    constructor(field: number, value: any) {
        this.field = field
        this.value = value
    }

    public Field = (): number => {
        return this.field
    }

    public Value = (): any => {
        return this.value
    }
}

export class BitrixDefaultValue {
    private field: string;
    private value: any;
    constructor(field: string, value: any) {
        this.field = field
        this.value = value
    }

    public Field = (): string => {
        return this.field
    }

    public Value = (): any => {
        return this.value
    }
}