import { AmocrmAssociation, AmocrmDefaultValue } from "./types";

export class AmocrmIntegration {
    private id: UniqueId;
    private siteId: string;
    private pipelineId: number;
    private statusId: number;
    private responsible: number;
    private unsorted: boolean;
    private associations: AmocrmAssociation[];
    private defaultValues: AmocrmDefaultValue[];
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, siteId: UniqueId, pipelineId:number, statusId: number, responsible: number, unsorted: boolean, associations: AmocrmAssociation[], defaultValues: AmocrmDefaultValue[], createdAt: string, modifiedAt: string) {
        this.id = id
        this.siteId = siteId
        this.pipelineId = pipelineId
        this.statusId = statusId
        this.responsible = responsible
        this.unsorted = unsorted
        this.associations = associations
        this.defaultValues = defaultValues
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public SiteId = (): string => {
        return this.siteId
    }
    
    public PipelineId = (): number => {
        return this.pipelineId
    }

    public StatusId = (): number => {
        return this.statusId
    }

    public Responsible = (): number => {
        return this.responsible
    }

    public Unsorted = (): boolean => {
        return this.unsorted
    }

    public Associations = (): AmocrmAssociation[] => {
        return this.associations
    }

    public DefaultValues = (): AmocrmDefaultValue[] => {
        return this.defaultValues
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}