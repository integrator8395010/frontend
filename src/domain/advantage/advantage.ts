export class Advantage {
    private id: UniqueId;
    private kvizId: string;
    private title: string;
    private photo: string;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, kvizId: string, title: string, photo:string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.kvizId = kvizId
        this.title = title
        this.photo = photo
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public KvizId = (): string => {
        return this.kvizId
    }

    public Title = (): string => {
        return this.title
    }

    public Photo = (): string => {
        return this.photo;
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}