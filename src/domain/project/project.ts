export class Project {
    private id: UniqueId;
    private name: string;
    private photo: string;
    private createdBy: string;
    private createdAt: string;
    private modifiedAt: string;

    constructor(id: UniqueId, name: string, photo: string, createdBy:string, createdAt: string, modifiedAt: string) {
        this.id = id
        this.name = name
        this.photo = photo
        this.createdBy = createdBy
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public Id = (): UniqueId => {
        return this.id
    }

    public Name = (): string => {
        return this.name
    }

    public Photo = (): string => {
        return this.photo;
    }

    public CreatedBy = (): string => {
        return this.createdBy
    }

    public CreatedAt = (): string => {
        return this.createdAt
    }

    public ModifiedAt = (): string => {
        return this.modifiedAt
    }
}