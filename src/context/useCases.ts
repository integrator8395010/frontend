import { createContext } from 'react'
import { AdvantageUseCases } from '../useCase/advantage/advantage';
import { AuthorizationUseCases } from '../useCase/authorization/authorization';
import { IntegrationUseCases } from '../useCase/integration/integration';
import { KvizUseCases } from '../useCase/kviz/kviz';
import { LeadUseCases } from '../useCase/lead/lead';
import { PhotoUseCases } from '../useCase/photo/photo';
import { PlanUseCases } from '../useCase/plan/plan';
import { ProjectUseCases } from '../useCase/project/project';
import { SiteUseCases } from '../useCase/site/site';

interface UseCasesContextInterface {
    authUseCase: AuthorizationUseCases;
    projectsUseCase: ProjectUseCases;
    siteUseCase: SiteUseCases;
    kvizUseCase: KvizUseCases;
    planUseCase: PlanUseCases;
    advantageUseCase: AdvantageUseCases;
    photosUseCase: PhotoUseCases;
    integrationUseCase: IntegrationUseCases;
    leadUseCase: LeadUseCases;
}

export const UseCasesContext = createContext<UseCasesContextInterface | null>(null)
