import { Items } from "./items"
import { MenuLink } from "./menu-link"
import { CircleDot, Circle } from 'tabler-icons-react';
import { useState } from "react";

export const Menu = () => {
    const [closed, setClosed] = useState(false)

    const collapseClick = () => {
        setClosed(!closed)
        if (!closed) {
            document.querySelector('html')?.classList.add('layout-menu-collapsed')
        } else {
            document.querySelector('html')?.classList.remove('layout-menu-collapsed')
        }
    }

    const menuHover = () => {
        document.querySelector('html')?.classList.add('layout-menu-hover')
    }
    return (
        <aside id="layout-menu" className="layout-menu menu-vertical menu bg-menu-theme" onMouseEnter={()=>{document.querySelector('html')?.classList.add('layout-menu-hover');}} onMouseLeave={()=>{document.querySelector('html')?.classList.remove('layout-menu-hover')}}  style={{ touchAction: "none", zIndex:1000, userSelect: "none" }}>

            <div className="app-brand demo">
                <a href="index.html" className="app-brand-link">
                    <span className="app-brand-logo demo">
                        <svg width="32" height="22" viewBox="0 0 32 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path fillRule="evenodd" clipRule="evenodd" d="M0.00172773 0V6.85398C0.00172773 6.85398 -0.133178 9.01207 1.98092 10.8388L13.6912 21.9964L19.7809 21.9181L18.8042 9.88248L16.4951 7.17289L9.23799 0H0.00172773Z" fill="#7367F0"></path>
                            <path opacity="0.06" fillRule="evenodd" clipRule="evenodd" d="M7.69824 16.4364L12.5199 3.23696L16.5541 7.25596L7.69824 16.4364Z" fill="#161616"></path>
                            <path opacity="0.06" fillRule="evenodd" clipRule="evenodd" d="M8.07751 15.9175L13.9419 4.63989L16.5849 7.28475L8.07751 15.9175Z" fill="#161616"></path>
                            <path fillRule="evenodd" clipRule="evenodd" d="M7.77295 16.3566L23.6563 0H32V6.88383C32 6.88383 31.8262 9.17836 30.6591 10.4057L19.7824 22H13.6938L7.77295 16.3566Z" fill="#7367F0"></path>
                        </svg>
                    </span>
                    <span className="app-brand-text demo menu-text fw-bold">Vuexy</span>
                </a>

                {!closed?<CircleDot onClick={collapseClick} className="layout-menu-toggle menu-link text-large ms-auto" size={20} />:<Circle onClick={collapseClick} className="layout-menu-toggle menu-link text-large ms-auto" size={20} />}
            </div>

            <ul className="menu-inner py-1 ps ps--active-y">
                {Items.map((item, index) => {
                    if (index === 0) {
                        return <>
                            <MenuLink {...item} />
                            <div style={{marginBottom:"10px"}}></div>
                        </>
                    }

                    if (index === (Items.length - 1)) {
                        return <>
                            <div style={{height:"100%"}}></div>
                            <MenuLink {...item} />
                            <div style={{marginBottom:"20px"}}></div>
                        </>
                    }
                    return <MenuLink {...item} />
                })}
                <div className="ps__rail-x" style={{ left: "0px", bottom: "-524px" }}>
                    <div className="ps__thumb-x" tabIndex={0} style={{ left: "0px", width: "0px" }}></div>
                </div><div className="ps__rail-y" style={{ top: "524px", height: "521px", right: "4px" }}>
                    <div className="ps__thumb-y" tabIndex={0} style={{ top: "191px", height: "190px" }}></div>
                </div>
            </ul>
        </aside>
    )
}
