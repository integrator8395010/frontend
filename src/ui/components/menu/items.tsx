import { ArrowBackUp, Logout, ChartArcs3, BrandFirefox, PlugConnected, ListDetails, CoinBitcoin } from 'tabler-icons-react';

export const Items = [
    {
        title: "К списку проектов",
        icon: <ArrowBackUp size={24} className="menu-icon"  />,
        href: "/",
    },
    {
        title: "Статистика",
        icon: <ChartArcs3 size={24} className="menu-icon"  />,
        href: "/dashboard/:id",
    },

    {
        title: "Домены",
        icon: <BrandFirefox size={24} className="menu-icon"  />,
        href: "/domains/:id",
    },
    {
        title: "Квизы",
        icon: <ListDetails size={24} className="menu-icon"  />,
        href: "/quiz/:id",
    },
    {
        title: "Лиды",
        icon: <CoinBitcoin size={24} className="menu-icon"  />,
        href: "/leads/:id",
    },
    {
        title: "Интеграции",
        icon: <PlugConnected size={24} className="menu-icon"  />,
        subItems: [
            {
                title: "Crm клиента",
                href: "/integration/project/:id"
            },
            {
                title: "Сайты",
                href: "/integration/site/list/:id"
            },
        ],
    },

    
    {
        title: "Выход",
        icon: <Logout size={24} className="menu-icon" />,
        href: "/exit",
    },

]
