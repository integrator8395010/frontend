import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import { Login } from "../../pages/auth/login/login";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { useContext, useEffect } from "react";
import { UseCasesContext } from "../../../context/useCases";
import { ProjectList } from "../../pages/project-list/project-list";
import { ProjectAdd } from "../../pages/project-add/project-add";
import { Dashboard } from "../../pages/dashboard";
import { SiteList } from "../../pages/site-list";
import { SiteAdd } from "../../pages/site-add";
import { KvizList } from "../../pages/kviz-list";
import { KvizAdd } from "../../pages/kviz-add/kviz-add";
import { KvizEdit } from "../../pages/kviz-edit/kviz-edit";
import { PlanAdd } from "../../pages/plan-add/plan-add";
import { AdvantageAdd } from "../../pages/advantage-add/advantage-add";
import { PhotoAdd } from "../../pages/photo-add/photo-add";
import { IntegrationSiteList } from "../../pages/integration-site-list/integration-stie-list";
import { IntegrationSiteAdd } from "../../pages/integration-site-add";
import { IntegrationSiteEdit } from "../../pages/integration-site-edit";
import { IntegrationSiteMail } from "../../pages/integration-site-mail";
import { ProjectIntegration, ProjectIntegrationAmocrm, ProjectIntegrationBitrix } from "../../pages/project-integration";
import { IntegrationSiteAmocrm } from "../../pages/integration-site-amocrm";
import { IntegrationSiteBitrix } from "../../pages/integration-site-bitrix";
import { IntegrationSiteLeadactiv } from "../../pages/integration-site-leadactiv";
import { LeadactivIntegrationAmocrm } from "../../pages/leadactiv-integration";
import { LeadListPage } from "../../pages/leads-list";
import { SiteEdit } from "../../pages/site-edit";

export const RouterComponent = () => {
  let useCases = useContext(UseCasesContext)

  const login = useTypedSelector(({ login }) => {
    return login
  })

  useEffect(() => {
    useCases?.authUseCase.CheckAuthorization()
    setInterval(()=>{
      useCases?.authUseCase.CheckAuthorization()
    },10000)
  }, [])

  if (login?.loading === undefined) {
    return <div>loading</div>
  }

 
  return (<Router basename="/admin">
    <Routes>
      {login?.authorized ? <>
        {/* leads routes */}
        <Route path="/leads/:id" element={<LeadListPage />} />
        
        {/*project integrations routes*/}
        <Route path="/integration/project/:id" element={<ProjectIntegration />}/>
        <Route path="/integration/add/project/:id/amocrm/:siteId" element={<ProjectIntegrationAmocrm />} />
        <Route path="/integration/add/project/:id/bitrix/:siteId" element={<ProjectIntegrationBitrix />} />
        <Route path="/integration/leadactiv" element={<LeadactivIntegrationAmocrm />} />

        {/*site integrations routes*/}
        <Route path="/integration/site/add/:id" element={<IntegrationSiteAdd />} />
        <Route path="/integration/site/edit/:id/:siteId" element={<IntegrationSiteEdit />} />
        <Route path="/integration/site/list/:id" element={<IntegrationSiteList />} />
        
        <Route path="/integration/site/add/:id/mail/:siteId" element={<IntegrationSiteMail />} />
        <Route path="/integration/site/add/:id/amocrm/:siteId" element={<IntegrationSiteAmocrm />} />
        <Route path="/integration/site/add/:id/bitrix/:siteId" element={<IntegrationSiteBitrix />} />
        <Route path="/integration/site/add/:id/leadactiv/:siteId" element={<IntegrationSiteLeadactiv />} />

        
        {/* kivz routes */}
        <Route path="/photo/add/:id/:kvizId" element={<PhotoAdd />} />
        <Route path="/advantage/add/:id/:kvizId" element={<AdvantageAdd />} />
        <Route path="/plan/add/:id/:kvizId" element={<PlanAdd />} />
        <Route path="/quiz/:id/edit/:kvizId" element={<KvizEdit />} />
        <Route path="/quiz/:id/edit/:kvizId/:stageId" element={<KvizEdit />} />
        <Route path="/quiz/add/:id" element={<KvizAdd />} />
        <Route path="/quiz/:id" element={<KvizList />} />


        <Route path="/domains/edit/:id/:siteId" element={<SiteEdit />} />
        <Route path="/domains/add/:id" element={<SiteAdd />} />
        <Route path="/domains/:id" element={<SiteList />} />

        <Route path="/project/add" element={<ProjectAdd />} />
        <Route path="/dashboard/:id" element={<Dashboard />} />
        <Route path="*" element={<ProjectList />} />
      </> : <>
        <Route path="*" element={<Login />} />
      </>}
    </Routes>
  </Router>
  )
}