import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { Plus, Trash } from "tabler-icons-react"
import { UseCasesContext } from "../../../context/useCases"
import { AmocrmAssociation, AmocrmDefaultValue } from "../../../domain/integration/types"
import { LeadFieldNamesList } from "../../../domain/lead/lead"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"
import { AmoButton } from "./components"

export const IntegrationSiteAmocrm = () => {
    let { id, siteId } = useParams();
    let useCases = useContext(UseCasesContext)
    const [state, setState] = useState(Math.random().toString(36).substring(2))
    const [amocrmFields, setAmocrmFields] = useState<{ id: number, name: string, code: string, type: string, enums: null | { id: number, value: string }[] }[]>([])
    const [amocrmUsers, setAmocrmUsers] = useState<{ id: number, name: string }[]>([])
    const [amocrmPipelines, setAmocrmPipelines] = useState<{ id: number, name: string }[]>([])
    const [amocrmStatuses, setAmocrmStatuses] = useState<{ id: number, name: string }[]>([])
    const navigate = useNavigate()
    const [form, setForm] = useState<{
        id: string,
        associations: { leadField: string, amocrmField: number }[],
        defaultValues: { field: number, value: any }[],
        responsible: number,
        pipelineId: number,
        statusId: number,
        unsorted: boolean,
    }>({
        id: "",
        associations: [],
        defaultValues: [],
        responsible: 0,
        pipelineId: 0,
        statusId: 0,
        unsorted: false,
    })

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    const submit = () => {
        let associations: AmocrmAssociation[] = []
        form.associations.forEach((association) => {
            associations.push(new AmocrmAssociation(association.leadField, association.amocrmField))
        })

        let defaultValues: AmocrmDefaultValue[] = []
        form.defaultValues.forEach((defaultValue) => {
            defaultValues.push(new AmocrmDefaultValue(defaultValue.field, defaultValue.value))
        })
        if (form.id) {

            useCases?.integrationUseCase.UpdateAmocrmIntegration(form.id, siteId!, form.pipelineId, form.statusId, form.responsible, form.unsorted, associations, defaultValues, ()=>{{navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })}})
        } else {
            useCases?.integrationUseCase.CreateAmocrmIntegration(siteId!, form.pipelineId, form.statusId, form.responsible, form.unsorted, associations, defaultValues, ()=>{{navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })}})
        }
    }

    useEffect(() => {
        useCases?.integrationUseCase.ReadProjectIntegration(id!)
        useCases?.integrationUseCase.ReadIntegrationsOfSite(siteId!)
    }, [siteId])

    useEffect(() => {
        if (integrations?.projectIntegration) {
            useCases?.integrationUseCase.ReadFieldsOfAmoCrmLead(id!, 1).then((response) => {
                if (response instanceof Error) {
                    return
                }

                try {
                    let fields = JSON.parse(response)
                    setAmocrmFields(fields._embedded.custom_fields)
                } catch (e) {
                    console.log("ошибка получения полей")
                }
            })

            useCases?.integrationUseCase.ReadUsersOfAmoCrm(id!).then((response) => {
                let users = JSON.parse(response)
                setAmocrmUsers(users._embedded.users)
            })
            useCases?.integrationUseCase.ReadPipelinesOfAmoCrm(id!).then((response) => {
                let pipelines = JSON.parse(response)
                setAmocrmPipelines(pipelines._embedded.pipelines)
            })

        }
    }, [integrations?.projectIntegration])

    useEffect(() => {
        useCases?.integrationUseCase.ReadStatusesOfPipelineAmoCrm(id!, form.pipelineId).then((response) => {
            try {
                let statuses = JSON.parse(response)
                setAmocrmStatuses(statuses._embedded.statuses)
            } catch (e) { }
        })
    }, [form.pipelineId])

    const leadValueOptionClick = (index: number, value: string) => {
        let newValues = form.associations
        newValues[index].leadField = value

        setForm({ ...form, associations: [...newValues] })
    }

    const amocrmValueOptionClick = (index: number, value: number) => {
        let newValues = form.associations
        newValues[index].amocrmField = value

        setForm({ ...form, associations: [...newValues] })
    }

    const defaultValueOptionClick = (index: number, value: number) => {
        let newValues = form.defaultValues
        newValues[index].field = value

        setForm({ ...form, defaultValues: [...newValues] })
    }

    const defaultValueChangeValue = (index: number, value: any) => {
        let newValues = form.defaultValues
        newValues[index].value = value

        setForm({ ...form, defaultValues: [...newValues] })
    }

    const getDefaultFieldType = (index: number) => {
        let field = amocrmFields.filter((field) => { return field.id == form.defaultValues[index].field })[0]
        if (!field) {
            return "text"
        }
        return field.type
    }

    const getStatusName = (statusId: number): string => {
        let name = ""
        amocrmStatuses.forEach((status) => {
            if (statusId === status.id) {
                name = status.name
            }
        })

        return name
    }

    const getDefaultFieldEnums = (id: number): any => {
        let value
        let name = ""
        amocrmFields.forEach((field) => {
            if (id === field.id) {
                value = field.enums
            }
        })

        return value
    }

    useEffect(() => {
        if (integrations?.amocrmIntegration) {
            let associations: { leadField: string, amocrmField: number }[] = [];
            let defaultValues: { field: number, value: any }[] = [];
            integrations.amocrmIntegration.Associations() && integrations.amocrmIntegration.Associations().forEach((item) => {
                associations.push({
                    leadField: item.LeadField(),
                    amocrmField: item.AmocrmField(),
                })
            })
            integrations.amocrmIntegration.DefaultValues() && integrations.amocrmIntegration.DefaultValues().forEach((item) => {
                defaultValues.push({
                    field: item.Field(),
                    value: item.Value(),
                })
            })
            setForm({
                ...form,
                id: integrations.amocrmIntegration.Id(),
                pipelineId: integrations.amocrmIntegration.PipelineId(),
                statusId: integrations.amocrmIntegration.StatusId(),
                responsible: integrations.amocrmIntegration.Responsible(),
                unsorted: integrations.amocrmIntegration.Unsorted(),
                associations: associations,
                defaultValues: defaultValues,
            })
        }
    }, [integrations?.amocrmIntegration])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция amocrm</h4>
                        </div>
                        <div className="card">
                            <div className="row mx-1 mt-3">
                                <div className="col-sm-12 col-md-8 mt-4">
                                    <h5 className="mb-0">{form.id === "" ?"Редактирование":"Добавление"} интеграции amocrm</h5>
                                </div>
                                {form.id !== "" ?<div className="col-sm-12 col-md-4">
                                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                        <div className="dt-buttons py-3 mb-4">
                                            <button className="dt-button add-new btn btn-danger mb-3 mb-md-0" onClick={(e)=>{e.preventDefault();  useCases?.integrationUseCase.DeleteAmocrmIntegrationOfSite(form.id, ()=>{navigate(-1)})}} >
                                                <span>Удалить интеграцию</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>:<></>}
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="form-label" htmlFor={"responsible"}>Ответственный за сделки</label>
                                        <select value={form.responsible} onChange={(e) => {setForm({ ...form, responsible: parseInt(e.target.value) })}} name={"responsible"} id={"responsible"} className="form-select" key={"responsible"}>
                                            <option disabled value="0">{amocrmUsers.length == 0 ? "Загрузка..." : "Выберите ответственного за сделки"}</option>
                                            {amocrmUsers.map((user) => {
                                                return <option value={user.id}>{user.name}</option>
                                            })}
                                        </select>
                                    </div>
                                    <div className="col-md-6">
                                        <label className="form-label" htmlFor={"pipelineId"}>Воронка / Статус</label>
                                        <div className="input-group input-group-merge">
                                            <select value={form.pipelineId} onChange={(e) => {  setForm({ ...form, pipelineId: parseInt(e.target.value), statusId: 0, }) }} name={"pipelineId"} id={"pipelineId"} className="form-select" key={"pipelineId"}>
                                                <option disabled value="0">{amocrmPipelines.length == 0 ? "Загрузка..." : "Выберите воронку"}</option>
                                                {amocrmPipelines.map((pipeline) => {
                                                    return <option value={pipeline.id}>{pipeline.name}</option>
                                                })}
                                            </select>
                                            <select value={form.statusId} onChange={(e) => { let statusId = parseInt(e.target.value); setForm({ ...form, statusId: statusId, unsorted: getStatusName(statusId) === "Неразобранное" }) }} name={"responsible"} id={"responsible"} className="form-select" key={"responsible"}>
                                                <option disabled value="0">{form.pipelineId == 0 ? "Выберите сначала воронку" : amocrmStatuses.length == 0 ? "Загрузка..." : "Выберите статус"}</option>
                                                {amocrmStatuses.map((status) => {
                                                    return <option value={status.id}>{status.name}</option>
                                                })}
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div className="row mx-1 mt-4">
                                    <div className="col-sm-12 col-md-4" style={{ alignItems: "center", display: "flex", }}>
                                        <h6>1.Передача полей из лида</h6>
                                    </div>

                                </div>
                                <div className="row">
                                    {form.associations.map((association, index) => {
                                        return <div key={"associations_" + index}><div className="row" style={{ position: "relative" }} key={"associations_" + index}>
                                            <div className="col-md-6">
                                                <label className="form-label" htmlFor={"leadField_" + index}>Поле Лида</label>
                                                <select value={form.associations[index].leadField} onChange={(e) => { leadValueOptionClick(index, e.target.value) }} name={"leadField_" + index} id={"leadField_" + index} className="form-select" key={"leadField_" + index}>
                                                    <option disabled value="">{"Выберите поле лида"}</option>
                                                    {LeadFieldNamesList.map((name) => {
                                                        return <option value={name}>{name}</option>
                                                    })}
                                                </select>
                                            </div>
                                            <div className="col-md-6">
                                                <label className="form-label" htmlFor={"leadField_" + index}>Поле в Amocrm</label>
                                                <select value={form.associations[index].amocrmField} onChange={(e) => { amocrmValueOptionClick(index, getDefaultFieldEnums(parseInt(e.target.value))) }} name={"leadField_" + index} id={"leadField_" + index} className="form-select" key={"leadField_" + index}>
                                                    <option disabled value="0">{"Выберите поле в amocrm"}</option>
                                                    {amocrmFields.map((field) => {
                                                        if (field.enums) {
                                                            return
                                                        }
                                                        return <option value={field.id}>{field.name} ({field.id})</option>
                                                    })}
                                                </select>
                                                <Trash style={{ position: "absolute", right: 0, top: 30, color: "red" }} size={20} onClick={() => { setForm({ ...form, associations: [...form.associations.filter((field, i) => { return index !== i })] }) }} />
                                            </div>
                                        </div>
                                        </div>
                                    })}
                                    <div className="col-sm-12 col-md-12 mt-4">
                                        <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                            <div className="dt-buttons">
                                                <button className="dt-button add-new btn btn-primary mb-3 mb-md-0" onClick={() => { setForm({ ...form, associations: [...form.associations, { leadField: "", amocrmField: 0 }] }) }} ><span><Plus size={15} /> Поле лида</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div className="row mx-1 mt-5">
                                    <div className="col-sm-12 col-md-4" style={{ alignItems: "center", display: "flex", }}>
                                        <div className="col-md-12"><span className="fw-bold">2.Значения дефолтных полей </span><br />(эти значения не изменяются)</div>
                                    </div>
                                </div>

                                <div className="row">
                                    {form.defaultValues.map((defaultValue, index) => {
                                        return <div key={"defaultValues_" + index}><div className="row" style={{ position: "relative" }} key={"associations_" + index}>
                                            <div className="col-md-6">
                                                <label className="form-label" htmlFor={"leadField_" + index}>Поле в Amocrm</label>
                                                <select value={form.defaultValues[index].field} onChange={(e) => { defaultValueOptionClick(index, parseInt(e.target.value)) }} name={"leadField_" + index} id={"leadField_" + index} className="form-select" key={"leadField_" + index}>
                                                    <option disabled value="0">{"Выберите поле в amocrm"}</option>
                                                    {amocrmFields.map((field) => {
                                                        return <option value={field.id}>{field.name}</option>
                                                    })}
                                                </select>
                                                <Trash style={{ position: "absolute", right: 0, top: 30, color: "red" }} size={20} onClick={() => { setForm({ ...form, defaultValues: [...form.defaultValues.filter((field, i) => { return index !== i })] }) }} />
                                            </div>

                                            <div className="col-md-6">
                                                <label className="form-label" htmlFor={"leadField_" + index}>Значение поля</label>
                                                {getDefaultFieldType(index) === "select" ? <select onChange={(e) => { }} value={form.defaultValues[index].value.id} name={"leadField_" + index} id={"leadField_" + index} className="form-select mb-3" key={"leadField_" + index}>
                                                    <option disabled value="">{"Значение поля"}</option>
                                                    {amocrmFields.filter((field) => field.id === defaultValue.field)[0].enums!.map((item) => {
                                                        return <option value={item.id} onClick={() => { defaultValueChangeValue(index, {enum_id: item.id, value: item.value}) }}>{item.value}</option>
                                                    })}
                                                </select> :
                                                    <input type={getDefaultFieldType(index)} className={"form-control"} name={"field"} placeholder={"Значение поля"} value={defaultValue.value} onChange={(e) => { defaultValueChangeValue(index, e.target.value) }} />}
                                            </div>
                                        </div>

                                        </div>
                                    })}
                                    <div className="col-sm-12 col-md-12 mt-4">
                                        <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                            <div className="dt-buttons">
                                                <button className="dt-button add-new btn btn-primary mb-3 mb-md-0" onClick={() => { setForm({ ...form, defaultValues: [...form.defaultValues, { field: 0, value: "" }] }) }} ><span><Plus size={15} /> Дефолтное поле</span></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div className="d-flex justify-content-end">
                                    {<button key="submit" className={"btn btn-primary d-grid"} onClick={(e) => { e.preventDefault(); submit() }}>Сохранить</button>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
