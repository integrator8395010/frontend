import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"
import { Menu } from "../../components/menu"
import "./style.css"
import { Check, Plus } from "tabler-icons-react"
import { StepTitle } from "../kviz-add/components/step-title"
import { SiteSettings } from "./components/site-settings"
import { PlansEdit } from "./components/plans-edit"
import { AdvantagesEdit } from "./components/advantages-edit"
import { PhotosEdit } from "./components/photos-edit"

export const KvizEdit = () => {
    let { id, kvizId, stageId } = useParams();
    const navigate = useNavigate()


    const [stage, setStage] = useState(0)


    const sites = useTypedSelector(({ sites }) => {
        return sites
    })

    const setActiveStep = (index: number) => {
        navigate("/quiz/"+id+"/edit/"+kvizId+"/"+(index-1))
    }

    const stepsList = [
        {
            title: "Основные параметры",
            subTitle: "Основные параметры квиза",
        },
        {
            title: "Преимущества",
            subTitle: "Список преимуществ",
        },
        {
            title: "Планировки",
            subTitle: "Список планировок",
        },
        {
            title: "Фото",
            subTitle: "Фото в галлерее",
        },
    ]

    useEffect(()=>{
        if (stageId) {
            setStage(parseInt(stageId)+1)
        }
    },[stageId])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-4">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Квизы /</span> Редактировать квиз</h4>
                        </div>
                    </div>
                    <div className="col-12 mb-4">
                        <div className="bs-stepper wizard-vertical vertical mt-2">
                            <div className="bs-stepper-header">
                                {
                                    stepsList.map((step, index) => {
                                        return <StepTitle index={index} active={stage == index} title={step.title} subTitle={step.subTitle} clickFunction={setActiveStep} />
                                    })
                                }
                            </div>
                            <div className="bs-stepper-content">
                                <div>
                                    {(() => {
                                        switch (stage) {
                                            case 0:
                                                return <SiteSettings />
                                            case 1:
                                                return <AdvantagesEdit />
                                            case 2:
                                                return <PlansEdit />
                                            case 3:
                                                return <PhotosEdit />
                                        }
                                    })()}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
