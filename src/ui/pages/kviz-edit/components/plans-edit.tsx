import { useContext, useEffect } from "react"
import { Link, useParams } from "react-router-dom";
import { UseCasesContext } from "../../../../context/useCases"
import { useTypedSelector } from "../../../../hooks/useTypedSelector"

export const PlansEdit = () => {
    let { id, kvizId } = useParams();
    let useCases = useContext(UseCasesContext)

    const plans = useTypedSelector(({ plans }) => {
        return plans
    })

    useEffect(() => {
        useCases?.planUseCase.PlansOfKviz(kvizId!)
    }, [])

    const deletePlan = (id: UniqueId) =>{
        useCases?.planUseCase.DeletePlan(id)
    }

    return (
        <div id="account-details-1" className={"content active dstepper-block"}>
            <div className="row mx-1">
                <div className="col-sm-12 col-md-4">
                    <h6 className="mb-0">Планировки</h6>
                    <small>Редактировать планировки</small>
                </div>
                <div className="col-sm-12 col-md-8">
                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                        <div className="dt-buttons py-3 mb-4">
                            <Link to={"/plan/add/"+id+"/"+kvizId} className="dt-button add-new btn btn-primary mb-3 mb-md-0" >
                                <span>Добавить планировку</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row g-3">
                {
                    plans?.plans?.map((plan) => {
                        return <div className="col-md-6 col-lg-3 mb-3">
                            <div className="card h-100">
                                <div className="card-body">
                                    <h6 className="card-title">{plan.Title()}</h6>
                                    <ul>
                                        <li>Общая площадь: {plan.TotalArea()} м<sup>2</sup></li>
                                        <li>Площадь кухни: {plan.KitchenArea()} м<sup>2</sup></li>
                                        <li>Площадь ванной: {plan.BathRoomArea()} м<sup>2</sup></li>
                                    </ul>
                                    <img className="img-fluid d-flex mx-auto my-4 rounded" src={process.env.REACT_APP_BACKEND_URL + "/file-store/" + plan.Photo()} alt="Card image cap" style={{ width: "400px" }} />
                                    <button className="btn btn-outline-primary waves-effect" onClick={()=>{deletePlan(plan.Id())}} >Удалить</button>
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}