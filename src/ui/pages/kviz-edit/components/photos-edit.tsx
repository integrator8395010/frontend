import { useContext, useEffect } from "react"
import { Link, useParams } from "react-router-dom";
import { UseCasesContext } from "../../../../context/useCases"
import { useTypedSelector } from "../../../../hooks/useTypedSelector"

export const PhotosEdit = () => {
    let { id, kvizId } = useParams();
    let useCases = useContext(UseCasesContext)

    const photos = useTypedSelector(({ photos }) => {
        return photos
    })

    useEffect(() => {
        useCases?.photosUseCase.PhotosOfKviz(kvizId!)
    }, [])

    const deletePhoto = (id: UniqueId) =>{
        useCases?.photosUseCase.DeletePhoto(id)
    }

    return (
        <div id="account-details-1" className={"content active dstepper-block"}>
            <div className="row mx-1">
                <div className="col-sm-12 col-md-4">
                    <h6 className="mb-0">Фото</h6>
                    <small>Редактировать фото</small>
                </div>
                <div className="col-sm-12 col-md-8">
                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                        <div className="dt-buttons py-3 mb-4">
                            <Link to={"/photo/add/"+id+"/"+kvizId} className="dt-button add-new btn btn-primary mb-3 mb-md-0" >
                                <span>Добавить фото</span>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
            <div className="row g-3">
                {
                    photos?.photos?.map((photo) => {
                        return <div className="col-md-6 col-lg-3 mb-3">
                            <div className="card h-100">
                                <div className="card-body">
                                    <img className="img-fluid d-flex mx-auto my-4 rounded" src={process.env.REACT_APP_BACKEND_URL + "/file-store/" + photo.Photo()} alt="Card image cap" style={{ width: "400px" }} />
                                    <button className="btn btn-outline-primary waves-effect" onClick={()=>{deletePhoto(photo.Id())}} >Удалить</button>
                                </div>
                            </div>
                        </div>
                    })
                }
            </div>
        </div>
    )
}