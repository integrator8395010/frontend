import { useContext, useEffect, useState } from "react"
import { FiledType, Form } from "../../../components/form"
import { UseCasesContext } from "../../../../context/useCases"
import { useTypedSelector } from "../../../../hooks/useTypedSelector"
import { useParams } from "react-router-dom"

export const SiteSettings = () => {
    let { id, kvizId } = useParams();

    const [sitesList, setSitesList] = useState<{ title: string, value: string }[]>([])
    let useCases = useContext(UseCasesContext)

    const sites = useTypedSelector(({ sites }) => {
        return sites
    })

    const kvizes = useTypedSelector(({ kvizes }) => {
        return kvizes
    })

    const [form, setForm] = useState<{
        siteId: UniqueId,
        name: string,
        templateId: UniqueId,
        background: File | undefined,
        mainColor: string,
        secondaryColor: string,
        subTitle: string,
        subTitleItems: string,
        phoneStepTitle: string,
        footerTitle: string,
        phone: string,
        politics: boolean,
        roistat: string,
        advantagesTitle: string,
        photosTitle: string,
        plansTitle: string,
        resultStepText: string,
        qoopler: boolean,
        yandex: string,
        google: string,
        mail: string,
        vk: string,
        dmpOne: string,
        validatePhone: boolean

    }>({

        siteId: "",
        name: "",
        templateId: "",
        background: undefined,
        mainColor: "",
        secondaryColor: "",
        subTitle: "",
        subTitleItems: "",
        phoneStepTitle: "",
        footerTitle: "",
        phone: "",
        politics: false,
        roistat: "",
        advantagesTitle: "",
        photosTitle: "",
        plansTitle: "",
        resultStepText: "",
        qoopler: false,
        yandex: "",
        google: "",
        mail: "",
        vk: "",
        dmpOne: "",
        validatePhone: false,
    })

    const submit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        useCases?.kvizUseCase.UpdateKviz(kvizId!, form.name, form.siteId, form.templateId, form.mainColor, form.secondaryColor, form.subTitle, form.subTitleItems, form.phoneStepTitle, form.footerTitle, form.phone, form.politics, form.roistat, form.advantagesTitle, form.photosTitle, form.plansTitle, form.resultStepText, form.qoopler, form.dmpOne, form.validatePhone, form.background)
    }

    useEffect(() => {
        useCases?.siteUseCase.SitesOfProject(id!)
    }, [])

    useEffect(() => {
        if (!sites?.sites) {
            return
        }
        let selectList: { title: string, value: string }[] = [
        ]
        sites.sites.map((site) => {
            selectList.push({
                title: site.Url() + " / " + site.Name(),
                value: site.Id()
            })
        })
        setSitesList(selectList)
    }, [sites?.sites])

    useEffect(() => {
        useCases?.kvizUseCase.KvizesOfProject(id!)
    }, [kvizId])

    useEffect(() => {
        let kviz = kvizes?.kviz?.filter((kviz) => kviz.Id() === kvizId)
        if (kviz && kviz?.length > 0) {
            setForm({
                siteId: kviz[0].SiteId(),
                name: kviz[0].Name(),
                templateId: kviz[0].TemplateId(),
                mainColor: kviz[0].MainColor(),
                secondaryColor: kviz[0].SecondaryColor(),
                subTitle: kviz[0].SubTitle(),
                subTitleItems: kviz[0].SubTitleItems(),
                phoneStepTitle: kviz[0].PhoneStepTitle(),
                footerTitle: kviz[0].FooterTitle(),
                phone: kviz[0].Phone(),
                politics: kviz[0].Politics(),
                roistat: kviz[0].Roistat(),
                advantagesTitle: kviz[0].AdvantagesTitle(),
                photosTitle: kviz[0].PhotosTitle(),
                plansTitle: kviz[0].PlansTitle(),
                resultStepText: kviz[0].ResultStepText(),
                qoopler: kviz[0].Qoopler(),
                yandex: "",//kviz[0].Yandex(),
                google: "",//kviz[0].Google(),
                mail: "",//kviz[0].Mail(),
                vk: "",//kviz[0].Vk(),
                dmpOne: kviz[0].DmpOne(),
                validatePhone: kviz[0].ValidatePhone(),
                background: undefined,
            })
            console.log("set values")
        }
    }, [kvizes?.kviz])

    const updateField = (name: string, value: any) => {
        setForm({...form, [name]: value})
    }

    return (
        <div id="account-details-1" className={"content active dstepper-block"}>
            <div className="content-header mb-3">
                <h6 className="mb-0">Основные параметры</h6>
                <small>Основные параметры квиза</small>
            </div>
            <div className="row g-3">
                <Form
                    state={{
                        loading: false,
                        error: "",
                    }}

                    submit={submit}

                    fields={[
                        {
                            name: "siteId",
                            title: "Домен квиза",
                            placeholder: "Выберите домен квиза",
                            type: FiledType.Select,
                            options: sitesList,
                            value: form.siteId,
                        },
                        {
                            name: "name",
                            title: "Название квиза",
                            placeholder: "Введите название квиза",
                            type: FiledType.Text,
                            value: form.name,
                        },
                        {
                            name: "tamplateId",
                            title: "Шаблон квиза",
                            placeholder: "Введите шаблон квиза",
                            type: FiledType.Text,
                            value: form.templateId,
                        },
                        {
                            name: "background",
                            title: "Фоновая картинка главного блока",
                            placeholder: "Приложите фоновую картинку главного блока",
                            type: FiledType.File,
                            accept: "image/gif, image/jpeg, image/png"
                        },
                        {
                            name: "mainColor",
                            title: "Основной цвет",
                            placeholder: "Выберите основной цвет",
                            type: FiledType.Color,
                            value: form.mainColor,
                        },
                        {
                            name: "secondaryColor",
                            title: "Вторичный цвет",
                            placeholder: "Выберите вторичный цвет",
                            type: FiledType.Color,
                            value: form.secondaryColor,
                        },
                        {
                            name: "subTitle",
                            title: "Подзаголовок",
                            placeholder: "Введите подзаголовок",
                            type: FiledType.Text,
                            value: form.subTitle,
                        },
                        {
                            name: "subTitleItems",
                            title: "Подзаголовок список",
                            placeholder: "Введите список подзаголовка",
                            type: FiledType.Text,
                            value: form.subTitleItems,
                        },
                        {
                            name: "phoneStepTitle",
                            title: "Тайтл шага с номером телефона",
                            placeholder: "Введите тайтл шага с номером телефона",
                            type: FiledType.Text,
                            value: form.phoneStepTitle,
                        },
                        {
                            name: "footerTitle",
                            title: "Тайтл футера",
                            placeholder: "Введите тайтл футера",
                            type: FiledType.Text,
                            value: form.footerTitle,
                        },
                        {
                            name: "phone",
                            title: "Номер для обратного звонка",
                            placeholder: "Введите номер для обратного звонка",
                            type: FiledType.Text,
                            value: form.phone,
                        },
                        {
                            name: "politics",
                            title: "Показывать политику конфиденциальности на сайте",
                            placeholder: "Выберите отображать ли политику конфиденциальности",
                            type: FiledType.Select,
                            value: form.politics,
                            options: [
                                {
                                    title: "Да",
                                    value: true
                                },
                                {
                                    title: "Нет",
                                    value: false
                                },
                            ],

                        },
                        {
                            name: "resultStepText",
                            title: "Тайтл финального шага",
                            placeholder: "Введите тайтл финального шага",
                            type: FiledType.Text,
                            value: form.resultStepText,
                        },
                        {
                            name: "roistat",
                            title: "Ройстат",
                            placeholder: "Введите номер счетчика ройстата",
                            type: FiledType.Text,
                            value: form.roistat,
                        },
                        {
                            name: "qoopler",
                            title: "WantResult на сайте",
                            placeholder: "Выберите установить ли WantResult на сайте",
                            type: FiledType.Select,
                            value: form.qoopler,
                            options: [
                                {
                                    title: "Да",
                                    value: true
                                },
                                {
                                    title: "Нет",
                                    value: false
                                },
                            ]
                        },
                        {
                            name: "dmpOne",
                            title: "DmpOne",
                            placeholder: "Введите номер счетчика DmpOne",
                            type: FiledType.Text,
                            value: form.dmpOne,
                        },
                        {
                            name: "validatePhone",
                            title: "Проверка номеров через сервис smsc",
                            placeholder: "Выберите включить ли проверку номеров через сервис smsc",
                            type: FiledType.Select,
                            value: form.validatePhone,
                            options: [
                                {
                                    title: "Да",
                                    value: true
                                },
                                {
                                    title: "Нет",
                                    value: false
                                },
                            ]
                        },
                    ]}
                    btnSmall={true}
                    submitBtnTitle={"Далее"}
                    updateForm={updateField}
                />
            </div>
        </div>
    )
}