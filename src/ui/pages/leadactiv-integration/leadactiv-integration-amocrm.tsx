import axios from "axios";
import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases";
import { LeadactivAmoSecrets } from "../../../domain/integration/leadactiv-amo-secrets";
import { LeadactivIntegration } from "../../../domain/integration/leadactiv-integration";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { Menu } from "../../components/menu";
import { AmoButton } from "./components"

export const LeadactivIntegrationAmocrm = () => {
    let { id, siteId } = useParams();
    const [state, setState] = useState(Math.random().toString(36).substring(2))
    const navigate = useNavigate()

    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    useEffect(() => {
        useCases?.integrationUseCase.ReadLeadactivAmoSecrets()
    }, [])

    const amoCrmIntegrationClick = () => {
        var url_array = [
            'https://www.amocrm.ru/oauth/',
            '?state=', state,
            '&mode=', 'popup',
            '&origin=', window.location.href,
        ];

        url_array.push('&name=', 'Софт для интеграций (integratior.leadactiv.ru)');
        url_array.push('&description=', 'Софт для интеграций (integratior.leadactiv.ru)');
        url_array.push('&redirect_uri=', process.env.REACT_APP_INTEGRATION_URL + '/integration/amocrm/leadactiv/code');
        url_array.push('&secrets_uri=', process.env.REACT_APP_INTEGRATION_URL + '/integration/amocrm/leadactiv/secret');
        url_array.push('&logo=', 'https://leadactiv.ru/img/f_logo.svg');
        let final_scopes = ["crm", "notifications"]
        final_scopes.forEach(function (scope) {
            url_array.push('&scopes[]=', scope)
        });

        centerAuthWindow(
            url_array.join(''),
            'Предоставить доступ для интеграции'
        );
    }

    const centerAuthWindow = function (url: string, title: string) {
        var w = 750;
        var h = 580;
        var dual_screen_left = window.screenLeft !== undefined ? window.screenLeft : 100;
        var dual_screen_top = window.screenTop !== undefined ? window.screenTop : 100;

        var width = window.innerWidth;

        var height = window.innerHeight;

        var left = ((width / 2) - (w / 2)) + dual_screen_left;
        var top = ((height / 2) - (h / 2)) + dual_screen_top;

        window.open(url, title, 'scrollbars, status, resizable, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);
    };

    const receiveOAuthMessage = (event: any) => {
        var oauth_scripts: any = document.querySelectorAll('.amocrm_oauth');

        oauth_scripts.forEach(function (oauth_script: any) {
            if (event.data.client_id && oauth_script.dataset.clientId && event.data.client_id === oauth_script.dataset.clientId) {
                oauth_script.dataset.error = event.data.error;
                if (oauth_script.dataset.errorCallback) {
                    try {
                        var errorCallback = eval(oauth_script.dataset.errorCallback);
                        if (typeof errorCallback === 'function') {
                            errorCallback(event.data);
                        }
                    } catch (e) {
                        //noop
                    }
                }
            }
        });
    }

    function receiveNewLocation(event: any) {
        if (event.data.url) {
            openLink(event.data.url)
        }

    }

    const openLink = (url: string) => {
        var link = document.createElement("a")
        link.href = url
        link.target = "_blank"
        link.click()
    }

    useEffect(() => {
        window.addEventListener('message', receiveOAuthMessage, false);
        window.addEventListener('message', receiveNewLocation, false);
    }, [])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция Amocrm</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Добавление/Редактирование интеграции Amocrm</h5>
                            </div>
                            <div className="card-body">
                                {integrations?.leadactivAmoSecrets instanceof LeadactivAmoSecrets ? <>
                                    <p>Интеграция с leadactiv.ru установленна</p>
                                    <AmoButton click={amoCrmIntegrationClick} title={"Переустановить интеграцию"} />
                                </> : <AmoButton click={amoCrmIntegrationClick} title={"Установить интеграцию"} />}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
