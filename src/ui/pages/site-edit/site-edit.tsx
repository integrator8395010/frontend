import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { FiledType, Form } from "../../components/form"
import { Menu } from "../../components/menu"

export const SiteEdit = () => {
    const navigate = useNavigate();
    let { id, siteId } = useParams();

    let useCases = useContext(UseCasesContext)
    const [form, setForm] = useState<{
        id: string,
        name: string,
        url: string,
    }>({
        id: "",
        name: "",
        url: "",
    })

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (form.name !== "" && form.url !== "") {
            useCases?.siteUseCase.UpdateSite(siteId!, form.name!, id!, form.url!, ()=>{navigate(-1)})
        }
    }

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    const readSite = async () => {
        if (siteId && siteId != "") {
            let response = await useCases?.siteUseCase.SiteById(siteId!)
            if (response instanceof Error) {

            } else {
                setForm({id: response!.Id(), name: response!.Name(), url: response!.Url()})
            }
        }
    }

    useEffect(()=>{
        console.log(form)
    },[form])

    useEffect(()=>{
        setForm({id: "", name: "", url:""})
        readSite()
    },[siteId])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Домены /</span> Редактировать домен</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Редактирование домена</h5>
                            </div>
                            <div className="card-body">
                                {form.id=== "" ? <>Домен с таким id не найден</>:
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "name",
                                            title: "Название",
                                            placeholder: "Введите название",
                                            type: FiledType.Text,
                                            value: form.name,
                                        },
                                        {
                                            name: "url",
                                            title: "URL домена",
                                            placeholder: "Введите url",
                                            type: FiledType.Text,
                                            value: form.url,
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateForm}
                                />}
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
