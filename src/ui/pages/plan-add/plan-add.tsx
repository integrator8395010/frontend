import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"

export const PlanAdd = () => {
    let { id, kvizId } = useParams();
    const navigate = useNavigate();
    let useCases = useContext(UseCasesContext)
    const [form, setForm] = useState<{
        id: UniqueId,
        kvizId: UniqueId,
        title: string,
        totalArea: number,
        livingArea: number,
        bedRoomArea: number,
        bathRoomArea: number,
        kitchenArea: number,
        photo: string,
        rooms: number,
        price: number,
        file: File | null,
    }>({
        id: "",
        kvizId: "",
        title: "",
        totalArea: 0,
        livingArea: 0,
        bedRoomArea: 0,
        bathRoomArea: 0,
        kitchenArea: 0,
        photo: "",
        rooms: 0,
        price: 0,
        file: null,
    })

    const kvizes = useTypedSelector(({ kvizes }) => {
        return kvizes
    })

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (form.file !== null) {
            useCases?.planUseCase.CreatePlan(form.file, form.bathRoomArea, form.bedRoomArea, form.kitchenArea, kvizId, form.livingArea, form.photo, form.price, form.rooms, form.title, form.totalArea, ()=>{navigate(-1)})
        }
    }

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    useEffect(()=>{
        console.log(form)
    },[form])

    return (
        <div className="layout-page" >
            {/*<Menu />*/}
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Планировки /</span> Добавить планировку</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Добавление планировки</h5>
                            </div>
                            <div className="card-body">
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "file",
                                            title: "Фото планировки",
                                            placeholder: "Приложите лого проекта",
                                            type: FiledType.File,
                                            accept: "image/gif, image/jpeg, image/png",
                                            value: form.file,
                                        },
                                        {
                                            name: "title",
                                            title: "Тайтл планировки",
                                            placeholder: "Введите тайтл планировки",
                                            type: FiledType.Text,
                                            value: form.title,
                                        },
                                        {
                                            name: "totalArea",
                                            title: "Общая площадь",
                                            placeholder: "Введите общую площадь",
                                            type: FiledType.Number,
                                            value: form.totalArea,
                                        },
                                        {
                                            name: "livingArea",
                                            title: "Жилая площадь",
                                            placeholder: "Введите жилую площадь",
                                            type: FiledType.Number,
                                            value: form.livingArea,
                                        },
                                        {
                                            name: "bedRoomArea",
                                            title: "Площадь спальни",
                                            placeholder: "Введите площадь спальни",
                                            type: FiledType.Number,
                                            value: form.bedRoomArea,
                                        },
                                        {
                                            name: "bathRoomArea",
                                            title: "Площадь ванной",
                                            placeholder: "Введите площадь ванной",
                                            type: FiledType.Number,
                                            value: form.bathRoomArea,
                                        },
                                        {
                                            name: "kitchenArea",
                                            title: "Площадь кухни",
                                            placeholder: "Введите площадь кухни",
                                            type: FiledType.Number,
                                            value: form.kitchenArea,
                                        },
                                        {
                                            name: "rooms",
                                            title: "Количество комнат",
                                            placeholder: "Введите количество комнат",
                                            type: FiledType.Number,
                                            value: form.rooms,
                                        },
                                        {
                                            name: "price",
                                            title: "Стоимость",
                                            placeholder: "Введите цену",
                                            type: FiledType.Number,
                                            value: form.price,
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateForm}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
