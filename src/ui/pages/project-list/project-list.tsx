import { useContext, useEffect } from "react"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Link } from "react-router-dom"

export const ProjectList = () => {
    
    let useCases = useContext(UseCasesContext)

    const projects = useTypedSelector(({ projects }) => {
        return projects
    })

    useEffect(() => {
        useCases?.projectsUseCase.GetProjectList()
    }, [])

    return (
        <div className="layout-page" >
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-4">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Список проектов /</span> Проекты</h4>
                        </div>
                        <div className="col-sm-12 col-md-8">
                            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                <div className="dt-buttons py-3 mb-4">
                                    <Link to={"/project/add"} className="dt-button add-new btn btn-primary mb-3 mb-md-0" >
                                        <span>Добавить проект</span>
                                    </Link>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="row mb-5">
                        {projects?.project && projects?.project.map((project) => {
                            return <div className="col-md-6 col-lg-2 mb-3">
                                <div className="card h-100">
                                    <div className="card-body">
                                        <h5 className="card-title">{project.Name()}</h5>
                                        <img className="img-fluid d-flex mx-auto my-4 rounded" src={process.env.REACT_APP_BACKEND_URL+"/file-store/"+project.Photo()} alt="Card image cap" style={{ width: "400px" }} />
                                        <Link to={"/dashboard/"+project.Id()} className="btn btn-outline-primary waves-effect" >Перейти</Link>
                                    </div>
                                </div>
                            </div>
                        })}
                    </div>
                </div>
            </div>
        </div>
    )

}