import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"

export const AdvantageAdd = () => {
    let { id, kvizId } = useParams();
    let useCases = useContext(UseCasesContext)
    const navigate = useNavigate()
    const [form, setForm] = useState<{
        title: string,

        file: File | null,
    }>({
        title: "",
        file: null,
    })

    const kvizes = useTypedSelector(({ kvizes }) => {
        return kvizes
    })

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (form.file !== null) {
            
            useCases?.advantageUseCase.CreateAdvantage(form.file, kvizId, form.title, ()=>{navigate(-1)})
        }
    }

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    useEffect(()=>{
        console.log(form)
    },[form])

    return (
        <div className="layout-page" >
            {/*<Menu />*/}
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Преимущества /</span> Добавить преимущества</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Добавление преимущества</h5>
                            </div>
                            <div className="card-body">
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "file",
                                            title: "Фото преимущества",
                                            placeholder: "Приложите фото преимущества",
                                            type: FiledType.File,
                                            accept: "image/gif, image/jpeg, image/png",
                                            value: form.file,
                                        },
                                        {
                                            name: "title",
                                            title: "Тайтл преимущества",
                                            placeholder: "Введите тайтл преимущества",
                                            type: FiledType.Text,
                                            value: form.title,
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateForm}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
