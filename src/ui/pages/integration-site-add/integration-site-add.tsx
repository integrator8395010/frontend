import React, { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { ProjectAmocrmIntegration } from "../../../domain/integration/project-amocrm-integration"
import { ProjectBitrixIntegration } from "../../../domain/integration/project-bitrix-integration"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"
import { IntegrationCard } from "./components"

export const IntegrationSiteAdd = () => {
    let { id } = useParams();
    const [selectedSite, setSelectedSite] = useState<string>("")
    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    useEffect(() => {
        useCases?.integrationUseCase.ReadSitesWithoutIntegrationsOfProject(id!)
        useCases?.integrationUseCase.ReadProjectIntegration(id!)
    }, [id])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Добавить интеграции</h4>
                        </div>
                        <div className="card mb-4">
                            <div className="card-header">
                                <h5 className="mb-0">Добавление интеграции</h5>
                            </div>
                            <div className="card-body">
                                <div className="col-md-6">
                                    <label className="form-label" htmlFor="multicol-email"><h6>Выберите домен</h6></label>
                                    <select value={selectedSite}  onChange={(e) => setSelectedSite(e.target.value)} name={"siteId"} className="form-select mb-3">
                                        <option disabled value="">{"Выберите домен"}</option>
                                        {integrations?.sitesWithoutIntegrations && integrations?.sitesWithoutIntegrations.map((site) => {
                                            return <option value={site.Id()}>{site.Url()}</option>
                                        })}
                                    </select>
                                </div>
                                {selectedSite ?
                                    <React.Fragment>
                                        <hr className="my-4 mx-n4"></hr>
                                        <div className="row">
                                            <IntegrationCard activated={false} title={"Интеграция с почтой"} subTitle={"Отключено"} description={"Тут можно указать почты клиента"} type={"mail"} siteId={selectedSite} />
                                            {(() => {
                                                switch (true) {
                                                    case integrations?.projectIntegration instanceof ProjectAmocrmIntegration:
                                                        return <IntegrationCard activated={false} title={"Amocrm клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с amocrm клиента"} type={"amocrm"} siteId={selectedSite} />
                                                    case integrations?.projectIntegration instanceof ProjectBitrixIntegration:
                                                        return <IntegrationCard activated={false} title={"Bitrix клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с bitrix клиента"} type={"bitrix"} siteId={selectedSite} />
                                                }
                                            })()}
                                            <IntegrationCard activated={false} title={"Amocrm лидактива"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с amocrm лидактива"} type={"leadactiv"} siteId={selectedSite} />
                                        </div>
                                    </React.Fragment> : <div></div>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
