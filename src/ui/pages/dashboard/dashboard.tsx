import { useContext, useEffect, useState } from "react";
import { Menu } from "../../components/menu"
import { useParams } from "react-router-dom";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { UseCasesContext } from "../../../context/useCases";
import { LeadsStatistics } from "../../../domain/lead/statistics";
import { BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer, PieChart, Pie, RadialBarChart, RadialBar, AreaChart, Area } from 'recharts';


export const Dashboard = () => {
    let { id, siteId } = useParams();
    const [statisticsList, setStatisticsList] = useState<LeadsStatistics>()
    

    const [form, setForm] = useState<{
        url: string
        from: string
        to: string
    }>({
        url: "",
        from: "",
        to: "",
    })

    const [pieChartDate, setPieChartDate] = useState<{ name: string, value: number }[]>([])
    const [lineChartData, setLineChartData] = useState<{ date: string, site: number, marquiz: number, flexbe: number, tilda:number }[]>()

    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    useEffect(() => {
        let dateNow = new Date()
        dateNow.setDate(dateNow.getDate() - 6)
        let start = dateNow.toISOString().split('T')[0] + "T00:00:00Z"
        dateNow.setDate(dateNow.getDate() + 7)
        let end = dateNow.toISOString().split('T')[0] + "T23:59:59Z"
        setForm({
            ...form,
            from: start,
            to: end,
        })
    }, [])

    const dateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        let append = "T23:59:59Z"
        if (e.target.name === "dateStart") {
            append = "T00:00:00Z"
        }

        setForm({
            ...form,
            [e.target.name]: e.target.value + append
        })
    }

    useEffect(() => {
        useCases?.integrationUseCase.ReadSitesWithIntegrationsOfProject(id!)
    }, [id])

    const updateClick = async () => {
        let sites = await useCases!.leadUseCase.GetLeadsStatisticsOfProject(id!, form.url, form.from, form.to)
        if (!(sites instanceof Error)) {
            setStatisticsList(sites)
        }
    }

    useEffect(() => {
        prepareDataForPieChart()
        prepareDataForLineChart()
    }, [statisticsList])

    useEffect(() => {
        if (form.from !== "" && form.to !== "" && id) {
            updateClick()
        }
    }, [form])

    const formatDate = (date: Date) => {
        let day = date.getDate() > 10 ? date.getDate().toString() : "0" + date.getDate().toString()
        let month = date.getMonth() > 10 ? date.getMonth().toString() : "0" + date.getMonth().toString()
        return day + "." + month
    }

    const prepareDataForDaysChart = (): { Дата: string, Лиды: number, Спам: number }[] => {
        const data: { Дата: string, Лиды: number, Спам: number }[] = []

        if (statisticsList) {
            statisticsList.Days().forEach((day, key) => {
                data.push({
                    Дата: formatDate(key),
                    Лиды: day.LeadsCount(),
                    Спам: day.SpamLeadsCount(),
                })
            })
        }
        return data
    }

    const prepareDataForLineChart = () => {
        const data: { name: string, value: number, fill: string }[] = []

        if (statisticsList) {
            let leadsByCat = {
                site: 0,
                marquiz: 0,
                tilda: 0,
                flexbe: 0,
            }
            statisticsList.Days().forEach((day) => {
                leadsByCat.site += day.SiteLeadsCount()
                leadsByCat.marquiz += day.MarquizLeadsCount()
                leadsByCat.tilda += day.TildaLeadsCount()
                leadsByCat.flexbe += day.FlexbeLeadsCount()
            })

            data.push({
                name: "Флексби",
                value: leadsByCat.flexbe,
                fill: '#82ca9d',
            })

            data.push({
                name: "Тильда",
                value: leadsByCat.tilda,
                fill: '#8dd1e1',
            })


            data.push({
                name: "Марквиз",
                value: leadsByCat.marquiz,
                fill: '#83a6ed',
            })

            data.push({
                name: "Сайт",
                value: leadsByCat.site,
                fill: '#a4de6c',
            })


        }
        setPieChartDate(data)
    }

    const prepareDataForPieChart = () => {
        const data: { date: string, site: number, marquiz: number, flexbe: number, tilda:number }[] = []


        if (statisticsList) {
            statisticsList.Days().forEach((day, date) => {
            
            let leadsByCat = {
                date: formatDate(date),
                site: day.SiteLeadsCount(),
                marquiz: day.MarquizLeadsCount(),
                tilda: day.TildaLeadsCount(),
                flexbe: day.FlexbeLeadsCount(),
            }
            data.push(leadsByCat)
            

        })


        }
        setLineChartData(data)
    }

    const COLORS = ['#82ca9d', '#8dd1e1', '#83a6ed', '#a4de6c'];

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({ cx, cy, midAngle, innerRadius, outerRadius, percent, index }: { cx: number, cy: number, midAngle: number, innerRadius: number, outerRadius: number, percent: number, index: number }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.5;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text x={x} y={y} fill="white" textAnchor={x > cx ? 'start' : 'end'} dominantBaseline="central">
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };




    return (<div className="layout-page" >
        <Menu />
        <div className="content-wrapper" >
            <div className="container-xxl flex-grow-1 container-p-y">
                <div className="row mx-1">
                    <div className="col-sm-12 col-md-12">
                        <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Статистика /</span> Статистика по проекту</h4>
                    </div>
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-12 col-sm-6 col-lg-4">
                                    <label className="form-label">Дата с:</label>
                                    <input type="date" value={form?.from.split('T')[0]} name="from" onChange={dateChange} className="form-control dt-input dt-full-name" />
                                </div>
                                <div className="col-12 col-sm-6 col-lg-4">
                                    <label className="form-label">Дата по:</label>
                                    <input type="date" value={form?.to.split('T')[0]} name="to" onChange={dateChange} className="form-control dt-input dt-full-name" />
                                </div>
                                <div className="col-12 col-sm-6 col-lg-4">
                                    <label className="form-label">Домен:</label>
                                    <select value={form.url} onChange={(e) => setForm({ ...form, [e.target.name]: e.target.value })} name={"siteId"} className="form-select mb-3">
                                        <option disabled value="">{"Все домены"}</option>
                                        {integrations?.sitesWithIntegrations && integrations?.sitesWithIntegrations.map((site) => {
                                            return <option value={site.Url()}>{site.Url()} ({site.Name()})</option>
                                        })}
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="card mb-4">
                        <div className="card-body">
                            <div className="col-sm-12 col-md-12">
                                <h5 className="fw-bold py-3 mb-4">По дням</h5>
                            </div>
                            <div className="row" style={{ height: "400px", width: "100%" }}>
                                <ResponsiveContainer width="100%" height="100%">
                                    <BarChart
                                        width={500}
                                        height={300}
                                        data={prepareDataForDaysChart()}
                                        margin={{
                                            top: 20,
                                            right: 30,
                                            left: 20,
                                            bottom: 5,
                                        }}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="Дата" />
                                        <YAxis />
                                        <Tooltip />
                                        <Legend />
                                        <Bar dataKey="Лиды" stackId="a" fill="#8884d8" />
                                        <Bar dataKey="Спам" stackId="a" fill="#eb5959" />
                                    </BarChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-body">
                            <div className="col-sm-12 col-md-12">
                                <h5 className="fw-bold py-3 mb-4">По источникам</h5>
                            </div>
                            <div className="row">
                                <span className="badge" >Add-on Available</span>
                            </div>
                            <div className="row" style={{ height: "400px", width: "100%" }}>
                                <ResponsiveContainer width="100%" height="100%">
                                    <AreaChart
                                        width={500}
                                        height={400}
                                        data={lineChartData}
                                        margin={{
                                            top: 10,
                                            right: 30,
                                            left: 0,
                                            bottom: 0,
                                        }}
                                    >
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <XAxis dataKey="date" />
                                        <YAxis />
                                        <Tooltip />
                                        <Area type="monotone" dataKey="site" stackId="1" stroke="#ffc658" fill="#ffc658" />
                                        <Area type="monotone" dataKey="tilda" stackId="1" stroke="#8dd1e1" fill="#8dd1e1" />
                                        <Area type="monotone" dataKey="flexbe" stackId="1" stroke="#82ca9d" fill="#82ca9d" />
                                        <Area type="monotone" dataKey="marquiz" stackId="1" stroke="#83a6ed" fill="#83a6ed" />
                                    </AreaChart>
                                </ResponsiveContainer>
                            </div>
                            <div className="row" style={{ height: "400px", width: "100%" }}>
                                <ResponsiveContainer width="50%" height="100%">
                                    <PieChart width={400} height={400}>
                                        <Pie
                                            data={pieChartDate}
                                            cx="40%"
                                            cy="50%"
                                            labelLine={false}
                                            label={renderCustomizedLabel}
                                            outerRadius={140}
                                            fill="#8884d8"
                                            dataKey="value"
                                        >
                                            {pieChartDate.map((entry, index) => (
                                                <Cell key={`cell-${index}`} fill={COLORS[index % COLORS.length]} />
                                            ))}
                                        </Pie>
                                    </PieChart>
                                </ResponsiveContainer>
                                <ResponsiveContainer width="50%" height="100%">
                                    <RadialBarChart cx="50%" cy="50%" innerRadius="10%" outerRadius="90%" barSize={10} data={pieChartDate}>
                                        <RadialBar
                                            label={{ position: 'insideStart', fill: '#fff' }}
                                            background
                                            dataKey="value"
                                        />
                                        <Legend iconSize={10} layout="vertical" verticalAlign="middle" wrapperStyle={{
                                            top: '50%',
                                            right: 0,
                                            transform: 'translate(0, -50%)',
                                            lineHeight: '24px',
                                        }} />
                                    </RadialBarChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    )
}
