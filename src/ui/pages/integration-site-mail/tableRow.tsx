import { useContext, useState } from "react";
import { Site } from "../../../domain/site/site";
import { ChevronRight, DotsVertical, Edit, Trash } from 'tabler-icons-react'
import { UseCasesContext } from "../../../context/useCases";
import { Kviz } from "../../../domain/kviz/kviz";
import { Link, useParams } from "react-router-dom";

export const TableRow = (props: { mail: string, deleteItem: (deleteItem: string) => void, last: boolean }) => {
    let { id } = useParams();
    const [showMenu, setShowMenu] = useState(false)
    let useCases = useContext(UseCasesContext)

    const formatDate = (input: string) => {
        const today = new Date(input);
        const yyyy = today.getFullYear();
        let mm = (today.getMonth() + 1).toString(); // Months start at 0!
        let dd = today.getDate().toString();

        if (dd.length == 1) dd = '0' + dd;
        if (mm.length == 1) mm = '0' + mm;

        return dd + '.' + mm + '.' + yyyy;
    }
    return (<tr>
        <td>{props.mail}</td>
        <td>
            <div className="dropdown">
                <Trash onClick={()=>props.deleteItem(props.mail)} style={{color: "red"}} />
            </div>
        </td>
    </tr>)
}
