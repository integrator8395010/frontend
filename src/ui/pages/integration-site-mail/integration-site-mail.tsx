import { useContext, useEffect, useState } from "react"
import { Link, useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { MailIntegration } from "../../../domain/integration/mail-integration"
import { Site } from "../../../domain/site/site"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"
import { TableRow } from "./tableRow"

export const IntegrationSiteMail = () => {
    let { id, siteId } = useParams();
    const [site, setSite] = useState<Site | null>()
    const [newMail, setNewMail] = useState("")
    const [form, setForm] = useState<{
        id: string,
        list: string[],
    }>({
        id: "",
        list: [],
    })

    const navigate = useNavigate()

    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    const sites = useTypedSelector(({ sites }) => {
        return sites
    })

    useEffect(() => {
        useCases?.integrationUseCase.ReadIntegrationsOfSite(siteId!)
        useCases?.siteUseCase.SitesOfProject(id!)
    }, [siteId])

    useEffect(() => {
        if (integrations?.mailIntegration?.Recipients()) {
            setForm({ id: integrations.mailIntegration.Id(), list: [...integrations?.mailIntegration?.Recipients()] })
        }
    }, [integrations?.mailIntegration])

    useEffect(() => {
        sites?.sites?.forEach((site) => {
            if (site.Id() === siteId) {
                setSite(site)
            }
        })
    }, [sites])

    const addMailClick = () => {
        if (newMail === "") {
            return
        }
        if (form.id !== "") {
            useCases?.integrationUseCase.UpdateMailIntegration(form.id, siteId!, [...integrations!.mailIntegration!.Recipients(),newMail], () => {navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })})
            return
        }

        useCases?.integrationUseCase.CreateMailIntegration(siteId!, [newMail], () => {navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })})
    }

    const deleteMailClick = (deleteItem: string) => {
        let newList =  integrations!.mailIntegration!.Recipients().filter((mail)=>{return mail !== deleteItem})
        useCases?.integrationUseCase.UpdateMailIntegration(form.id, siteId!, newList, ()=> {})
    }

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграция с почтой /</span> Добавить интеграцию с почтой</h4>
                        </div>
                        <div className="card">
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-sm-12 col-md-4">
                                        <span className="py-3 mb-4"><h5 className="fw-bold ">Список получателей</h5>{site ? site.Name() : <></>}   </span>

                                    </div>
                                    <div className="col-sm-12 col-md-8">
                                        <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                            <div className="row">
                                                <div className="col-sm-12 col-md-8">
                                                    <input
                                                        type={"text"}
                                                        className="form-control"
                                                        name={"mail"}
                                                        placeholder={"Введите почту"}
                                                        style={{width:"100%"}}
                                                        value={newMail}
                                                        onChange={(e) => {setNewMail(e.target.value) }}
                                                    />
                                                </div>
                                                <div style={{width: "33%"}}>
                                                    <button className="dt-button add-new btn btn-primary" onClick={addMailClick} ><span>Добавить</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <table className="table mt-1">
                                    <thead>
                                        <tr>
                                            <th style={{ width: "80%" }}>Почта</th>
                                            <th>Действия</th>
                                        </tr>
                                    </thead>
                                    <tbody className="table table-hover">
                                        {form.list.map((item, index) => {
                                            return <TableRow mail={item} deleteItem={deleteMailClick} last={form.list!.length - 1 == index ? true : false} />
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
