import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"

export const PhotoAdd = () => {
    let { id, kvizId } = useParams();
    const navigate = useNavigate();
    let useCases = useContext(UseCasesContext)
    const [form, setForm] = useState<{
        file: File | null,
    }>({
        file: null,
    })

    const kvizes = useTypedSelector(({ kvizes }) => {
        return kvizes
    })

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (form.file !== null) {
            useCases?.photosUseCase.CreatePhoto(form.file, kvizId!, () => {navigate(-1)})
        }
    }
    

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    useEffect(()=>{
        console.log(form)
    },[form])

    return (
        <div className="layout-page" >
            {/*<Menu />*/}
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Фото /</span> Добавить фото</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Добавление фото</h5>
                            </div>
                            <div className="card-body">
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "file",
                                            title: "Фото",
                                            placeholder: "Приложите фото",
                                            type: FiledType.File,
                                            accept: "image/gif, image/jpeg, image/png",
                                            value: form.file,
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateForm}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
