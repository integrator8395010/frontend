import React, { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { ProjectAmocrmIntegration } from "../../../domain/integration/project-amocrm-integration"
import { ProjectBitrixIntegration } from "../../../domain/integration/project-bitrix-integration"
import { Site } from "../../../domain/site/site"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"
import { IntegrationCard } from "./components"

export const IntegrationSiteEdit = () => {
    let { id, siteId } = useParams();
    const [site, setSite] = useState<Site | null>()

    const sites = useTypedSelector(({ sites }) => {
        return sites
    })

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })
    let useCases = useContext(UseCasesContext)

    useEffect(() => {
        useCases?.integrationUseCase.ReadProjectIntegration(id!)
        useCases?.siteUseCase.SitesOfProject(id!)
        useCases?.integrationUseCase.ReadIntegrationsOfSite(siteId!)
        
    }, [siteId])

    useEffect(() => {
        sites?.sites?.forEach((site) => {
            if (site.Id() === siteId) {
                setSite(site)
            }
        })
    }, [sites])

    const declOfNum = (n:number, text_forms:string[]) => {  
        n = Math.abs(n) % 100; 
        var n1 = n % 10;
        if (n > 10 && n < 20) { return  n +" "+ text_forms[2]; }
        if (n1 > 1 && n1 < 5) { return  n +" "+ text_forms[1]; }
        if (n1 == 1) { return  n +" "+ text_forms[0]; }
        return n +" "+ text_forms[2];
    }

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции сайта /</span> Редактирование интеграции</h4>
                        </div>
                        <div className="card mb-4">
                            <div className="card-header">
                                <h5 className="mb-0">{site?.Name()}</h5>
                                <p> {site?.Url()}</p>
                            </div>
                            <div className="card-body">
                                {siteId ? <div className="row">
                                    <IntegrationCard activated={integrations?.mailIntegration && integrations.mailIntegration.Recipients().length > 0?true:false} title={"Интеграция с почтой"} subTitle={integrations?.mailIntegration?declOfNum(integrations.mailIntegration.Recipients().length, ["почта", "почты", "получателей"]):"Отключено"} description={"Тут можно указать почты клиента"} type={"mail"} siteId={siteId!} />
                                    {(() => {
                                        switch (true) {
                                            case integrations?.projectIntegration instanceof ProjectAmocrmIntegration:
                                                return  <IntegrationCard activated={integrations?.amocrmIntegration?true:false} title={"Amocrm клиента"} subTitle={integrations?.amocrmIntegration?"Подключен":"Отключено"} description={"Тут можно настроить интеграцию с amocrm клиента"} type={"amocrm"} siteId={siteId!} />
                                            case integrations?.projectIntegration instanceof ProjectBitrixIntegration:
                                                return <IntegrationCard activated={integrations?.bitrixIntegration?true:false} title={"Bitrix клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с bitrix клиента"} type={"bitrix"} siteId={siteId!} />
                                        }
                                    })()}
                                    <IntegrationCard activated={integrations?.leadactivIntegration?true:false} title={"Amocrm лидактива"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с amocrm лидактива"} type={"leadactiv"} siteId={siteId!} />
                                </div> : <></>}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
