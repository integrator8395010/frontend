import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom";
import { UseCasesContext } from "../../../context/useCases";
import { ProjectAmocrmIntegration } from "../../../domain/integration/project-amocrm-integration";
import { ProjectBitrixIntegration } from "../../../domain/integration/project-bitrix-integration";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { Menu } from "../../components/menu"
import { IntegrationCard } from "./components"

export const ProjectIntegration = () => {
    let { id } = useParams();
    const [type, setType] = useState("amocrm")
    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    useEffect(() => {
        useCases?.integrationUseCase.ReadProjectIntegration(id!)
    }, [])


    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция проекта</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Выберите тип интеграции</h5>
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    {(() => {
                                        switch (true) {
                                            case integrations?.projectIntegration instanceof ProjectAmocrmIntegration:
                                                return <IntegrationCard activated={true} title={"Amocrm клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с amocrm клиента для проекта"} type={"amocrm"} projectId={id!} />
                                            case integrations?.projectIntegration instanceof ProjectBitrixIntegration:
                                                return <IntegrationCard activated={true} title={"Bitrix клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с bitrix клиента для проекта"} type={"bitrix"} projectId={id!} />
                                            default:
                                                return <>
                                                    <IntegrationCard activated={false} title={"Bitrix клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с bitrix клиента для проекта"} type={"bitrix"} projectId={id!} />
                                                    <IntegrationCard activated={false} title={"Amocrm клиента"} subTitle={"Отключено"} description={"Тут можно настроить интеграцию с amocrm клиента для проекта"} type={"amocrm"} projectId={id!} />
                                                </>
                                        }
                                    })()}

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}