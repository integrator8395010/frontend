import { useNavigate, useParams } from "react-router-dom"

export const IntegrationCard = ({ activated, title, subTitle, description, type, projectId }: { activated: boolean, title: string, subTitle: string, description: string, type:string, projectId:string }) => {
    let { id } = useParams();

    const navigate = useNavigate()
    const cardClick = () => {
        navigate("/integration/add/project/"+id!+"/"+type+"/"+projectId)
    }
    return (
        <div onClick={cardClick} style={{cursor: "pointer"}} className="col-md-6 col-lg-6">
            <div className="card mb-4">
                <div className="card-body">
                    <div className="row">
                        <div className="col-md-6 col-lg-10">
                            <h5 className="card-title">{title}</h5>
                        </div>
                        <div className="col-md-6 col-lg-2">
                            <label className="switch switch-lg">
                                <input type="checkbox" checked={activated} className="switch-input" />
                                <span className="switch-toggle-slider">
                                    <span className="switch-on">
                                        <i className="ti ti-check"></i>
                                    </span>
                                    <span className="switch-off">
                                        <i className="ti ti-x"></i>
                                    </span>
                                </span>
                            </label>
                        </div>
                    </div>
                    <div className="card-subtitle text-muted mb-3">{subTitle}</div>
                    <p className="card-text">
                        {description}
                    </p>
                </div>
            </div>
        </div>
    )
}