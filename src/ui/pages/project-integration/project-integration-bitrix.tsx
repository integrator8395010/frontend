import { FormEvent, useContext, useEffect, useState } from "react"
import { set } from "react-hook-form";
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { ProjectBitrixIntegration } from "../../../domain/integration/project-bitrix-integration";
import { useTypedSelector } from "../../../hooks/useTypedSelector";
import { FiledType, Form } from "../../components/form";
import { Menu } from "../../components/menu";

export const ProjectIntegrationBitrix = () => {
    let { id, siteId } = useParams();
    const [form, setForm] = useState<{
        id: string,
        baseUrl: string
    }>({
        id: "",
        baseUrl: ""
    })

    let useCases = useContext(UseCasesContext)

    const navigate = useNavigate()

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    const submit = (e: FormEvent) => {
        e.preventDefault()
        if (form.id === "") {
            useCases?.integrationUseCase.CreateProjectBitrixIntegration(id!, form.baseUrl, ()=>{navigate(-1)})
        } else {
            useCases?.integrationUseCase.UpdateProjectBitrixIntegration(form.id, id!, form.baseUrl, ()=>{navigate(-1)})
        }
    }

    const updateField = (name: string, value: string) => {
        setForm({ ...form, [name]: value })
    }

    useEffect(() => {
        useCases?.integrationUseCase.ReadProjectBitrixIntegration(id!)
    }, [])

    useEffect(() => {
        if (integrations?.projectIntegration instanceof ProjectBitrixIntegration) {
            setForm({ ...form, id: integrations?.projectIntegration.Id(), baseUrl: integrations?.projectIntegration.BaseUrl() })
        }
    }, [integrations?.projectIntegration])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция Bitrix24</h4>
                        </div>
                        <div className="card">
                            <div className="row mx-1 mt-3">
                                <div className="col-sm-12 col-md-8 mt-4">
                                    <h5 className="mb-0">Добавление/Редактирование интеграции Bitrix24</h5>
                                </div>
                                {form.id !== "" ?<div className="col-sm-12 col-md-4">
                                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                        <div className="dt-buttons py-3 mb-4">
                                            <button className="dt-button add-new btn btn-danger mb-3 mb-md-0" onClick={(e)=>{e.preventDefault();  useCases?.integrationUseCase.DeleteProjectBitrixIntegration(form.id, ()=>{navigate(-1)})}} >
                                                <span>Удалить интеграцию</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>:<></>}
                            </div>
                            <div className="card-body">
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "baseUrl",
                                            title: "URL вебхука",
                                            placeholder: "Введите url вебхука",
                                            type: FiledType.Text,
                                            value: form.baseUrl,
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateField}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    )
}
