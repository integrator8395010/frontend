import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"

export const IntegrationSiteLeadactiv = () => {
    let { id, siteId } = useParams();
    let useCases = useContext(UseCasesContext)
    const [amocrmUsers, setLeadactivUsers] = useState<{ id: number, name: string }[]>([])
    const navigate = useNavigate()
    const [form, setForm] = useState<{
        id: string,
        responsible: number,
    }>({
        id: "",
        responsible: 0,
    })

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    const submit = () => {
        if (form.id) {
            useCases?.integrationUseCase.UpdateLeadactivIntegration(form.id, siteId!, form.responsible, ()=>{navigate(-1)})
        } else {
            useCases?.integrationUseCase.CreateLeadactivIntegration(siteId!, form.responsible, ()=>{navigate(-1)})
        }
    }

    useEffect(() => {
        useCases?.integrationUseCase.ReadLeadactivIntegration(siteId!)
    }, [siteId])
    
    const readUsers = async () => {
        let users = []
        let response = await useCases?.integrationUseCase.ReadUsersOfLeadactiv(1)
        let responseDecoded = JSON.parse(response)
        let totalItems = responseDecoded._total_items
        users.push(...responseDecoded._embedded.users)
        let page = 2
        while (users.length < totalItems) {
            let response = await useCases?.integrationUseCase.ReadUsersOfLeadactiv(page)
            let responseDecoded = JSON.parse(response)
            users.push(...responseDecoded._embedded.users)
            page+=1
        }
        setLeadactivUsers(users)
    }

    useEffect(() => {
        readUsers()
    }, [])

    

    useEffect(() => {
        if (integrations?.leadactivIntegration) {
            setForm({
                ...form,
                id: integrations.leadactivIntegration.Id(),
                responsible: integrations.leadactivIntegration.Responsible(),
            })
        }
    }, [integrations?.amocrmIntegration])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция leadactiv</h4>
                        </div>
                        <div className="card">
                            <div className="row mx-1 mt-3">
                                <div className="col-sm-12 col-md-8 mt-4">
                                    <h5 className="mb-0">{form.id === "" ?"Редактирование":"Добавление"} интеграции leadactiv</h5>
                                </div>
                                {form.id !== "" ?<div className="col-sm-12 col-md-4">
                                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                        <div className="dt-buttons py-3 mb-4">
                                            <button className="dt-button add-new btn btn-danger mb-3 mb-md-0" onClick={(e)=>{e.preventDefault();  useCases?.integrationUseCase.DeleteLeadactivIntegration(form.id, ()=>{navigate(-1)})}} >
                                                <span>Удалить интеграцию</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>:<></>}
                            </div>
                            <div className="card-body">
                                <div className="row">
                                    <div className="col-md-6">
                                        <label className="form-label" htmlFor={"responsible"}>Ответственный за сделки</label>
                                        <select value={form.responsible} onChange={(e) => { setForm({ ...form, responsible: parseInt(e.target.value) })}} name={"responsible"} id={"responsible"} className="form-select" key={"responsible"}>
                                            <option disabled value="0">{amocrmUsers.length == 0 ? "Загрузка..." : "Выберите ответственного за сделки"}</option>
                                            {amocrmUsers.map((user) => {
                                                return <option value={user.id}>{user.name}</option>
                                            })}
                                        </select>
                                    </div>
                                </div>
                                <hr />
                                <div className="d-flex justify-content-end">
                                    {<button key="submit" className={"btn btn-primary d-grid"} onClick={(e) => { e.preventDefault(); submit() }}>Сохранить</button>}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
