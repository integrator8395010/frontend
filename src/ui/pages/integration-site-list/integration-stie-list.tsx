import { useContext, useEffect } from "react"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Link, useParams } from "react-router-dom"
import { TableRow } from "./tableRow"
import { Menu } from "../../components/menu"

export const IntegrationSiteList = () => {
    let { id } = useParams();
    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    useEffect(() => {
        useCases?.integrationUseCase.ReadSitesWithIntegrationsOfProject(id!)
    }, [])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-4">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Список сайтов с интеграциями /</span> Список</h4>
                        </div>
                        <div className="col-sm-12 col-md-8">
                            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                <div className="dt-buttons py-3 mb-4">
                                    <Link to={"/integration/site/add/" + id} className="dt-button add-new btn btn-primary mb-3 mb-md-0" ><span>Добавить интеграцию</span></Link>
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <div className="table-responsive text-nowrap">
                                <table className="table mt-1">
                                    <thead>
                                        <tr>
                                            <th style={{width: "40%"}}>Название</th>
                                            <th>Дата создания</th>
                                            <th>Действия</th>
                                        </tr>
                                    </thead>
                                    <tbody className="table table-hover">
                                        {integrations?.sitesWithIntegrations && integrations?.sitesWithIntegrations.map((item, index) => {
                                            return <TableRow site={item} last={integrations?.sitesWithIntegrations!.length - 1 == index?true:false} />
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    )

}