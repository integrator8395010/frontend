import { useContext, useState } from "react";
import { Site } from "../../../domain/site/site";
import { ChevronRight, DotsVertical, Edit, Trash } from 'tabler-icons-react'
import { UseCasesContext } from "../../../context/useCases";
import { Kviz } from "../../../domain/kviz/kviz";
import { Link, useParams } from "react-router-dom";
import { Lead } from "../../../domain/lead";

export const TableRow = (props: { lead: Lead, last: boolean }) => {
    let { id } = useParams();
    const [showMenu, setShowMenu] = useState(false)
    let useCases = useContext(UseCasesContext)

    const formatDate = (input: string) => {
        const today = new Date(input);
        console.log(today)
        return ""
        /*const yyyy = today.getFullYear();
        let mm = (today.getMonth() + 1).toString(); // Months start at 0!
        let dd = today.getDate().toString();

        if (dd.length == 1) dd = '0' + dd;
        if (mm.length == 1) mm = '0' + mm;

        return dd + '.' + mm + '.' + yyyy;*/
    }
    return (<tr>
        <td>{props.lead.Name()}</td>
        <td>{props.lead.Phone()}</td>
        <td>{props.lead.Source()}</td>
        <td>{props.lead.CreatedAt().split(' +')[0]}</td>
        <td>
            <div className="dropdown">
                <Link to={"/integration/site/edit/"+id+"/"+props.lead.Id()} className="btn p-0 dropdown-toggle hide-arrow">
                    <ChevronRight />
                </Link>
            </div>
        </td>
    </tr>)
}
