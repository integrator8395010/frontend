import { useContext, useEffect, useState } from "react"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Link, useParams } from "react-router-dom"
import { TableRow } from "./tableRow"

import { Menu } from "../../components/menu"
import { LeadFilter } from "../../../domain/lead"

export const LeadListPage = () => {
    let { id } = useParams();
    let useCases = useContext(UseCasesContext)
    const [filter, setFilter] = useState<{
        from: string,
        to: string,
        url: string,
        phone: string,
        source: string,
    }>({
      from: "",
      to: "", 
      url: "",
      phone: "",
      source: "",
    })

    useEffect(() => {
        let dateNow = new Date()
        dateNow.setDate(dateNow.getDate() - 5)
        let start = dateNow.toISOString().split('T')[0]+"T00:00:00Z"
        dateNow.setDate(dateNow.getDate() + 6)
        let end = dateNow.toISOString().split('T')[0]+"T23:59:59Z"
        setFilter({
            ...filter,
            from: start,
            to: end,  
        })
        let newFilter = new LeadFilter(id!, filter.url!, filter.phone, filter.source, start, end)
        useCases?.leadUseCase.LeadsOfProject(newFilter, 0, 50)

    }, [])


    const leads = useTypedSelector(({ leads }) => {
        return leads
    })

    useEffect(() => {
        
    }, [])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-4">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Список сайтов с интеграциями /</span> Список</h4>
                        </div>
                        <div className="col-sm-12 col-md-8">
                            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                <div className="dt-buttons py-3 mb-4">
                                    <Link to={"/lead/site/add/" + id} className="dt-button add-new btn btn-primary mb-3 mb-md-0" ><span>Добавить интеграцию</span></Link>
                                </div>
                            </div>
                        </div>

                        <div className="card">
                            <div className="table-responsive text-nowrap">
                                <table className="table mt-1">
                                    <thead>
                                        <tr>
                                            <th style={{width: "40%"}}>Имя</th>
                                            <th>Номер</th>
                                            <th>Источник</th>
                                            <th>Действия</th>
                                        </tr>
                                    </thead>
                                    <tbody className="table table-hover">
                                        {leads?.leads && leads?.leads.map((item, index) => {
                                            return <TableRow lead={item} last={leads?.leads!.length - 1 == index?true:false} />
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    )

}