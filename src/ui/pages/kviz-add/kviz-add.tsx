import { useContext, useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"
import { Menu } from "../../components/menu"
import "./style.css"
import { StepTitle } from "./components/step-title"
import { Advantages } from "./components/advantages"
import { Plans } from "./components/plans"
import { Photos } from "./components/photos"
import { Check, Plus } from "tabler-icons-react"

export const KvizAdd = () => {
    let { id } = useParams();
    let useCases = useContext(UseCasesContext)
    const [sitesList, setSitesList] = useState<{ title: string, value: string }[]>([])
    const [stage, setStage] = useState(0)
    const [form, setForm] = useState<{
        site: {
            siteId: UniqueId,
            name: string,
            templateId: UniqueId,
            background: number[],
            mainColor: string,
            secondaryColor: string,
            subTitle: string,
            subTitleItems: string,
            phoneStepTitle: string,
            footerTitle: string,
            phone: string,
            politics: boolean,
            roistat: string,
            advantagesTitle: string,
            photosTitle: string,
            plansTitle: string,
            resultStepText: string,
            qoopler: boolean,
            yandex: string,
            google: string,
            mail: string,
            vk: string,
            dmpOne: string,
            validatePhone: boolean
        },
        advantages: { title: string, photo: number[] }[],
        photos: { photo: number[] }[],
        plans: { title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number }[],
    }>({
        site: {
            siteId: "",
            name: "",
            templateId: "",
            background: [],
            mainColor: "",
            secondaryColor: "",
            subTitle: "",
            subTitleItems: "",
            phoneStepTitle: "",
            footerTitle: "",
            phone: "",
            politics: false,
            roistat: "",
            advantagesTitle: "",
            photosTitle: "",
            plansTitle: "",
            resultStepText: "",
            qoopler: false,
            yandex: "",
            google: "",
            mail: "",
            vk: "",
            dmpOne: "",
            validatePhone: false,
        },
        advantages: [],
        photos: [],
        plans: [],
    })

    const sites = useTypedSelector(({ sites }) => {
        return sites
    })

    const submit = (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
        e.preventDefault()
        if (form.site.background !== null) {
            let photos: number[][] = []
            form.photos.forEach((photo) => {
                photos.push(photo.photo)
            })
            console.log(form)
            useCases?.kvizUseCase.CreateKvizComplex(form.site, form.advantages, photos, form.plans)
        }
    }

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    useEffect(() => {
        console.log(form)
    }, [form])

    useEffect(() => {
        useCases?.siteUseCase.SitesOfProject(id!)
    }, [])

    useEffect(() => {
        if (!sites?.sites) {
            return
        }
        let selectList: { title: string, value: string }[] = [
        ]
        sites.sites.map((site) => {
            selectList.push({
                title: site.Url() + " / " + site.Name(),
                value: site.Id()
            })
        })
        setSitesList(selectList)
    }, [sites?.sites])

    const setActiveStep = (index: number) => {
        setStage(index)
    }

    const stepsList = [
        {
            title: "Основные параметры",
            subTitle: "Основные параметры квиза",
        },
        {
            title: "Счетчики и сервисы",
            subTitle: "Счетчики и сервисы",
        },
        {
            title: "Преимущества",
            subTitle: "Список преимуществ",
        },
        {
            title: "Планировки",
            subTitle: "Список планировок",
        },
        {
            title: "Фото",
            subTitle: "Фото в галлерее",
        },
        {
            title: "Интеграция",
            subTitle: "Интеграция",
        },
    ]

    const updateSite = (key: string, value: any) => {
        setForm({
            ...form, site: {
                ...form.site,
                [key]: value
            }
        })
    }

    const updateAdvantages = (advantages: { title: string, photo: number[] }[]) => {
        setForm({ ...form, advantages: advantages })
    }

    const updatePlans = (plans: { title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number }[]) => {
        setForm({ ...form, plans: plans })
    }

    const updatePhotos = (photos: { photo: number[] }[]) => {
        setForm({ ...form, photos: photos })
    }

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-4">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Квизы /</span> Добавить квиз</h4>
                        </div>
                    </div>
                    <div className="col-12 mb-4">
                        <div className="bs-stepper wizard-vertical vertical mt-2">
                            <div className="bs-stepper-header">
                                {
                                    stepsList.map((step, index) => {
                                        return <StepTitle index={index} active={stage == index} title={step.title} subTitle={step.subTitle} clickFunction={setActiveStep} />
                                    })
                                }
                            </div>
                            <div className="bs-stepper-content">
                                {/* main */}
                                <div>
                                    <div id="account-details-1" className={stage == 0 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Основные параметры</h6>
                                            <small>Основные параметры квиза</small>
                                        </div>
                                        <div className="row g-3">
                                            <Form
                                                state={{
                                                    loading: false,
                                                    error: "",
                                                }}

                                                submit={submit}

                                                fields={[
                                                    {
                                                        name: "siteId",
                                                        title: "Домен квиза",
                                                        placeholder: "Выберите домен квиза",
                                                        type: FiledType.Select,
                                                        options: sitesList,
                                                        value: form.site.siteId,
                                                    },
                                                    {
                                                        name: "name",
                                                        title: "Название квиза",
                                                        placeholder: "Введите название квиза",
                                                        type: FiledType.Text,
                                                        value: form.site.name,
                                                    },
                                                    {
                                                        name: "tamplateId",
                                                        title: "Шаблон квиза",
                                                        placeholder: "Введите шаблон квиза",
                                                        type: FiledType.Text,
                                                        value: form.site.templateId,
                                                    },
                                                    {
                                                        name: "background",
                                                        title: "Фоновая картинка главного блока",
                                                        placeholder: "Приложите фоновую картинку главного блока",
                                                        type: FiledType.File,
                                                        bytes: true,
                                                        accept: "image/gif, image/jpeg, image/png"
                                                    },
                                                    {
                                                        name: "mainColor",
                                                        title: "Основной цвет",
                                                        placeholder: "Выберите основной цвет",
                                                        type: FiledType.Color,
                                                        value: form.site.mainColor,
                                                    },
                                                    {
                                                        name: "secondaryColor",
                                                        title: "Вторичный цвет",
                                                        placeholder: "Выберите вторичный цвет",
                                                        type: FiledType.Color,
                                                        value: form.site.secondaryColor,
                                                    },
                                                    {
                                                        name: "subTitle",
                                                        title: "Подзаголовок",
                                                        placeholder: "Введите подзаголовок",
                                                        type: FiledType.Text,
                                                        value: form.site.subTitle,
                                                    },
                                                    {
                                                        name: "subTitleItems",
                                                        title: "Подзаголовок список",
                                                        placeholder: "Введите список подзаголовка",
                                                        type: FiledType.Text,
                                                        value: form.site.subTitleItems,
                                                    },
                                                    {
                                                        name: "phoneStepTitle",
                                                        title: "Тайтл шага с номером телефона",
                                                        placeholder: "Введите тайтл шага с номером телефона",
                                                        type: FiledType.Text,
                                                        value: form.site.phoneStepTitle,
                                                    },
                                                    {
                                                        name: "footerTitle",
                                                        title: "Тайтл футера",
                                                        placeholder: "Введите тайтл футера",
                                                        type: FiledType.Text,
                                                        value: form.site.footerTitle,
                                                    },
                                                    {
                                                        name: "phone",
                                                        title: "Номер для обратного звонка",
                                                        placeholder: "Введите номер для обратного звонка",
                                                        type: FiledType.Text,
                                                        value: form.site.phone,
                                                    },
                                                    {
                                                        name: "politics",
                                                        title: "Показывать политику конфиденциальности на сайте",
                                                        placeholder: "Выберите отображать ли политику конфиденциальности",
                                                        type: FiledType.Select,
                                                        value: form.site.politics,
                                                        options: [
                                                            {
                                                                title: "Да",
                                                                value: true
                                                            },
                                                            {
                                                                title: "Нет",
                                                                value: false
                                                            },
                                                        ],

                                                    },
                                                    {
                                                        name: "resultStepText",
                                                        title: "Тайтл финального шага",
                                                        placeholder: "Введите тайтл финального шага",
                                                        type: FiledType.Text,
                                                        value: form.site.resultStepText,
                                                    },
                                                ]}
                                                btnSmall={true}
                                                submitBtnTitle={"Далее"}
                                                updateForm={updateSite}
                                            />
                                        </div>
                                    </div>
                                </div>
                                {/* counters */}
                                <div>
                                    <div id="account-details-1" className={stage == 1 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Счетчики и стороние сервисы</h6>
                                            <small>Настройка счетчиков и стороних сервисов</small>
                                        </div>
                                        <div className="row g-3">
                                            <Form
                                                state={{
                                                    loading: false,
                                                    error: "",
                                                }}

                                                submit={submit}

                                                fields={[
                                                    {
                                                        name: "roistat",
                                                        title: "Ройстат",
                                                        placeholder: "Введите номер счетчика ройстата",
                                                        type: FiledType.Text,
                                                        value: form.site.roistat,
                                                    },
                                                    {
                                                        name: "qoopler",
                                                        title: "WantResult на сайте",
                                                        placeholder: "Выберите установить ли WantResult на сайте",
                                                        type: FiledType.Select,
                                                        value: form.site.qoopler,
                                                        options: [
                                                            {
                                                                title: "Да",
                                                                value: true
                                                            },
                                                            {
                                                                title: "Нет",
                                                                value: false
                                                            },
                                                        ]
                                                    },
                                                    {
                                                        name: "dmpOne",
                                                        title: "DmpOne",
                                                        placeholder: "Введите номер счетчика DmpOne",
                                                        type: FiledType.Text,
                                                        value: form.site.dmpOne,
                                                    },
                                                    {
                                                        name: "validatePhone",
                                                        title: "Проверка номеров через сервис smsc",
                                                        placeholder: "Выберите включить ли проверку номеров через сервис smsc",
                                                        type: FiledType.Select,
                                                        value: form.site.validatePhone,
                                                        options: [
                                                            {
                                                                title: "Да",
                                                                value: true
                                                            },
                                                            {
                                                                title: "Нет",
                                                                value: false
                                                            },
                                                        ]
                                                    },
                                                ]}
                                                btnSmall={true}
                                                submitBtnTitle={"Далее"}
                                                updateForm={updateSite}
                                            />
                                        </div>
                                    </div>
                                </div>
                                {/* advantages */}
                                <div>
                                    <div id="account-details-1" className={stage == 2 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Преимущуства</h6>
                                            <small>Список преимуществ</small>
                                        </div>
                                        <Advantages list={form.advantages} updateAdvantages={updateAdvantages} updateSite={updateSite} />
                                    </div>
                                </div>

                                {/* plans */}
                                <div>
                                    <div id="account-details-1" className={stage == 3 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Планировки</h6>
                                            <small>Список планировок</small>
                                        </div>
                                        <Plans list={form.plans} updatePlans={updatePlans} updateSite={updateSite} />
                                    </div>
                                </div>

                                {/* photo */}
                                <div>
                                    <div id="account-details-1" className={stage == 4 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Фото</h6>
                                            <small>Фото в галлерее</small>
                                        </div>
                                        <Photos list={form.photos} updatePhotos={updatePhotos} updateSite={updateSite} />
                                    </div>
                                </div>

                                {/* integration */}
                                <div>
                                    <div id="account-details-1" className={stage == 5 ? "content active dstepper-block" : "content"}>
                                        <div className="content-header mb-3">
                                            <h6 className="mb-0">Интеграция</h6>
                                            <small>Интеграция</small>
                                        </div>
                                        <div className="row g-3">
                                            <div className="row d-flex justify-content-center">
                                                <button type="submit" onClick={(e) =>submit(e)} className="btn btn-primary me-sm-3 me-1 waves-effect waves-light" style={{ maxWidth: "300px" }}><Check /> Добавить</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
