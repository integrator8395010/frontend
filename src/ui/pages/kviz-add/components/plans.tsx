import { PlanItem } from "./plan-item"
import { Plus } from 'tabler-icons-react';
import { useEffect, useState } from "react";

export const Plans = ({ list, updatePlans, updateSite }: {
    list: {title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number}[], updatePlans: (plans: {title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number}[]) => void, updateSite: (key: string, value: any) => void
}) => {
    const [plansList, setPlansList] = useState<{title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number}[]>([])

    const addPlan = () => {
        setPlansList([...plansList, {title: "", photo: [], bathRoomArea: 0, totalArea: 0, bedRoomArea: 0, kitchenArea: 0, livingArea: 0, price: 0, rooms: 0}])
    }

    const deletePlan = (index: number) => {
        setPlansList([...plansList.filter((item, ind) => ind !==index)])
    }

    const updatePlan = (index: number, value: {title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number}) => {
        let newList:{title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number}[] = []
        plansList.forEach((plan, ind) => {
            if (ind !== index) {
                newList.push(plan)
            } else {
                newList.push(value)
            }
        })
        setPlansList([...newList])
    }

    useEffect(()=>{
        if (plansList !== list) {
            updatePlans([...plansList])
        }
    },[plansList])


    return (<div>
        <div className="mb-3">
            <label htmlFor={""} className="form-label">{"Заголовок блока планировок"}</label>
            <input type="text" className="form-control" name="plansTitle" placeholder="Введите тайтл блока планировок" onChange={(e) => { updateSite(e.target.name, e.target.value)}} />
        </div>
        {plansList.map((plan, index) => {
            return <PlanItem index={index} plan={plan} updatePlan={updatePlan} deletePlan={deletePlan} />
        })}
        <div className="row d-flex justify-content-center">
                <button type="submit" onClick={()=>{addPlan()}} className="btn btn-primary me-sm-3 me-1 waves-effect waves-light" style={{maxWidth: "300px"}}><Plus /> Добавить</button>
        </div>
    </div>
    )
}