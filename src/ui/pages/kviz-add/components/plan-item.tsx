import { ChangeEvent, Fragment } from "react"
import { Trash } from 'tabler-icons-react';
import { Buffer } from 'buffer';

export const PlanItem = ({ index, plan, updatePlan, deletePlan }: {
    index: number, plan: { title: string, photo: number[], bathRoomArea: number, totalArea: number, bedRoomArea: number, kitchenArea: number, livingArea: number, price: number, rooms: number }, updatePlan: (index: number, value: {
        title: string;
        photo: number[];
        bathRoomArea: number;
        totalArea: number;
        bedRoomArea: number;
        kitchenArea: number;
        livingArea: number;
        price: number;
        rooms: number;
    }) => void, deletePlan: (index: number) => void
}) => {

    const updateInput = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.type === "file") {
            if (event.target.files && event.target.files?.length > 0) {
                let name = event.target.name
                const reader = new FileReader();
                reader.onload = (event) => {
                    if (event.target?.result instanceof ArrayBuffer) {
                        console.log(toBuffer(event.target.result))
                        updatePlan(index, { ...plan, [name]: toBuffer(event.target.result).toJSON().data })
                    }
                };
                reader.readAsArrayBuffer(event.target.files[0]);
                return
            }
        }
        updatePlan(index, { ...plan, [event.target.name]: event.target.type === "number" ? parseInt(event.target.value) : event.target.value })
    }

    const toBuffer = (arrayBuffer: ArrayBuffer):Buffer => {
        const buffer = Buffer.alloc(arrayBuffer.byteLength);
        const view = new Uint8Array(arrayBuffer);
        for (let i = 0; i < buffer.length; ++i) {
            buffer[i] = view[i];
        }
        return buffer;
    }


    return (
        <Fragment>
            <div className="row">
                <div className="col-md-6">
                    <h6>Планировка - {index + 1}</h6>
                </div>
                <div className="col-md-6">
                    <Trash size={20} style={{ color: "#DC143C", cursor: "pointer" }} onClick={() => { deletePlan(index) }} />
                </div>
            </div>
            <div className="row g-3">
                <div className="col-md-12">
                    <label className="form-label" htmlFor="multicol-email">Обложка планировки</label>
                    <div className="input-group input-group-merge">
                        <input type="file" onChange={updateInput} id="multicol-email" className="form-control" placeholder="обложка" name="photo" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-username">Тайтл планировки</label>
                    <input type="text" onChange={updateInput} id="multicol-username" className="form-control" name="title" placeholder="Введите тайтл планировки" />
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Общая площадь</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Общая площадь" name="totalArea" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Площадь ванной</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Общая площадь" name="bathRoomArea" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Площадь спальни</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Общая площадь" name="bedRoomArea" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Площадь кухни</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Площадь кухни" name="kitchenArea" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Площадь гостинной</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Площадь кухни" name="livingArea" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Стоимость квартиры</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Площадь кухни" name="price" />
                    </div>
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Количество комнат</label>
                    <div className="input-group input-group-merge">
                        <input type="number" onChange={updateInput} id="multicol-email" className="form-control" placeholder="Площадь кухни" name="rooms" />
                    </div>
                </div>
            </div>
            <hr className="my-4 mx-n4" />
        </Fragment>
    )
}