import { ChangeEvent, ChangeEventHandler, Fragment } from "react"
import { Trash } from 'tabler-icons-react';
import {Buffer} from 'buffer';

export const AdvantageItem = ({ index, advantage, updateAdvantage, deleteAdvantage }: {
    index: number, advantage: { title: string, photo: number[] }, updateAdvantage: (index: number, value: {
        title: string;
        photo: number[];
    }) => void, deleteAdvantage: (index: number) => void
}) => {

    const updateInput = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.type === "file") {
            let name = event.target.name
            if (event.target.files && event.target.files?.length > 0) {
                const reader = new FileReader();
                reader.onload = (event) => {
                    if (event.target?.result instanceof ArrayBuffer) {
                        updateAdvantage(index, { ...advantage, [name]: toBuffer(event.target.result).toJSON().data })
                    }
                };
                reader.readAsArrayBuffer(event.target.files[0]);
               
            }
            return
        }
        updateAdvantage(index, { ...advantage, [event.target.name]: event.target.value })
    }

    const toBuffer = (arrayBuffer: ArrayBuffer):Buffer => {
        const buffer = Buffer.alloc(arrayBuffer.byteLength);
        const view = new Uint8Array(arrayBuffer);
        for (let i = 0; i < buffer.length; ++i) {
            buffer[i] = view[i];
        }
        return buffer;
    }

    return (
        <Fragment>
            <div className="row">
                <div className="col-md-6">
                    <h6>Преимущество - {index + 1}</h6>
                </div>
                <div className="col-md-6">
                    <Trash size={20} style={{ color: "#DC143C", cursor: "pointer" }} onClick={() => { deleteAdvantage(index) }} />
                </div>
            </div>
            <div className="row g-3">
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-username">Тайтл преимущества</label>
                    <input type="text" id="multicol-username" onChange={updateInput} className="form-control" value={advantage.title} name="title" placeholder="Введите тайтл преимущества" />
                </div>
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Обложка</label>
                    <div className="input-group input-group-merge">
                        <input type="file" id="multicol-email" onChange={updateInput} className="form-control" placeholder="обложка" name="photo" />
                    </div>
                </div>
            </div>
            <hr className="my-4 mx-n4" />
        </Fragment>
    )
}