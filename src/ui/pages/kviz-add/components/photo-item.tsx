import { ChangeEvent, Fragment } from "react"
import { Trash } from 'tabler-icons-react';
import {Buffer} from 'buffer';

export const PhotoItem = ({ index, updatePhoto, deletePhoto }: { index: number, updatePhoto: (index: number, value:  any) => void, deletePhoto: (index: number) => void }) => {
    const updateInput = (event: ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files?.length > 0) {
            //updatePhoto(index, { photo: event.target.files[0] })
        }

        if (event.target.type === "file") {
            let name = event.target.name
            if (event.target.files && event.target.files?.length > 0) {
                const reader = new FileReader();
                reader.onload = (event) => {
                    if (event.target?.result instanceof ArrayBuffer) {
                        updatePhoto(index, toBuffer(event.target.result).toJSON().data)
                    }
                };
                reader.readAsArrayBuffer(event.target.files[0]);
               
            }
            return
        }
    }

    const toBuffer = (arrayBuffer: ArrayBuffer):Buffer => {
        const buffer = Buffer.alloc(arrayBuffer.byteLength);
        const view = new Uint8Array(arrayBuffer);
        for (let i = 0; i < buffer.length; ++i) {
            buffer[i] = view[i];
        }
        return buffer;
    }

    return (
        <Fragment>
            <div className="row">
                <div className="col-md-6">
                    <h6>Фото - {index + 1}</h6>
                </div>
                <div className="col-md-6">
                    <Trash size={20} style={{color: "#DC143C", cursor: "pointer"}} onClick={() => {deletePhoto(index)}} />
                </div>
            </div>
            <div className="row g-3">
                <div className="col-md-6">
                    <label className="form-label" htmlFor="multicol-email">Фото</label>
                    <div className="input-group input-group-merge">
                        <input type="file" onChange={updateInput} id="multicol-email" className="form-control" name="photo" />
                    </div>
                </div>
            </div>
            <hr className="my-4 mx-n4" />
        </Fragment>
    )
}