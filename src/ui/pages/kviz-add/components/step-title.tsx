
export const StepTitle = ({index, active, title, subTitle, clickFunction}:{index: number, active: boolean, title: string, subTitle: string, clickFunction: (index: number) => void}) => {
    return ( <div className={active ? "step active" : "step"} onClick={() => { clickFunction(index) }} data-target="#account-details-1">
            <button type="button" className="step-trigger" aria-selected={active ? "true" : "false"}>
                <span className="bs-stepper-circle">{index+1}</span>
                <span className="bs-stepper-label">
                    <span className="bs-stepper-title">{title}</span>
                    <span className="bs-stepper-subtitle">{subTitle}</span>
                </span>
            </button>
        </div>
    )
}