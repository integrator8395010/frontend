import { Plus } from 'tabler-icons-react';
import { useEffect, useState } from "react";
import { PhotoItem } from "./photo-item";

export const Photos = ({ list, updatePhotos, updateSite }: {
    list: { photo: number[] }[], updatePhotos: (advantages: {
        photo: number[];
    }[]) => void, updateSite: (key: string, value: any) => void
}) => {
    const [photosList, setPhotosList] = useState<{ photo: number[] }[]>([])

    const addPhoto = () => {
        setPhotosList([...photosList, { photo: [], }])
    }

    const deletePhoto = (index: number) => {
        setPhotosList([...photosList.filter((item, ind) => ind !== index)])
    }

    const updatePhoto = (index: number, value: number[]) => {
        let newList:{ photo: number[] }[] = []
        photosList.forEach((photo, ind) => {
            if (ind !== index) {
                newList.push(photo)
            } else {
                newList.push({photo: value})
            }
        })
        setPhotosList([...newList])
    }

    useEffect(()=>{
        if (photosList !== list) {
            updatePhotos([...photosList])
        }
    },[photosList])

    return (<div>
        <div className="mb-3">
            <label htmlFor={""} className="form-label">{"Заголовок тайтла фото"}</label>
            <input type="text" className="form-control" name="photosTitle" placeholder="Введите тайтл блока фото" onChange={(e) => { updateSite(e.target.name, e.target.value)}} />
        </div>
        {photosList.map((photo, index) => {
            return <PhotoItem index={index} updatePhoto={updatePhoto} deletePhoto={deletePhoto} />
        })}
        <div className="row d-flex justify-content-center">
            <button type="submit" onClick={() => { addPhoto() }} className="btn btn-primary me-sm-3 me-1 waves-effect waves-light" style={{ maxWidth: "300px" }}><Plus /> Добавить</button>
        </div>
    </div>
    )
}