import { AdvantageItem } from "./advantage-item"
import { Plus } from 'tabler-icons-react';
import { useEffect, useState } from "react";

export const Advantages = ({ list, updateAdvantages, updateSite }: {
    list: { title: string, photo: number[] }[], updateAdvantages: (advantages: {
        title: string;
        photo: number[];
    }[]) => void, updateSite: (key: string, value: any) => void
}) => {
    const [advantagesList, setAdvantagesList] = useState<{ title: string, photo: number[] }[]>([])

    const addAdvantage = () => {
        setAdvantagesList([...advantagesList, { title: "", photo: [], }])
    }

    const deleteAdvantage = (index: number) => {
        setAdvantagesList([...advantagesList.filter((item, ind) => ind !== index)])
    }

    const updateAdvantage = (index: number, value: { title: string, photo: number[] }) => {
        let newList:{ title: string, photo: number[] }[] = []
        advantagesList.forEach((advantage, ind) => {
            if (ind !== index) {
                newList.push(advantage)
            } else {
                newList.push(value)
            }
        })
        setAdvantagesList([...newList])
    }

    useEffect(()=>{
        if (advantagesList !== list) {
            updateAdvantages([...advantagesList])
        }
    },[advantagesList])

    return (<div>
        <div className="mb-3">
            <label htmlFor={""} className="form-label">{"Заголовок блока преимуществ"}</label>
            <input type="text" className="form-control" name="advantagesTitle" placeholder="Введите тайтл блока преимущетсва" onChange={(e) => { updateSite(e.target.name, e.target.value)}} />
        </div>
        {advantagesList.map((advantage, index) => {
            return <AdvantageItem index={index} advantage={advantage} updateAdvantage={updateAdvantage} deleteAdvantage={deleteAdvantage} />
        })}
        <div className="row d-flex justify-content-center">
            <button type="submit" onClick={() => { addAdvantage() }} className="btn btn-primary me-sm-3 me-1 waves-effect waves-light" style={{ maxWidth: "300px" }}><Plus /> Добавить</button>
        </div>
    </div>
    )
}