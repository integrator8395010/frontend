import { useContext, useState } from "react";
import { Site } from "../../../domain/site/site";
import { DotsVertical, Edit, Trash } from 'tabler-icons-react'
import { UseCasesContext } from "../../../context/useCases";
import { Link, useParams } from "react-router-dom";

export const TableRow = (props: { site: Site, last: boolean }) => {
    const {id} = useParams()
    const [showMenu, setShowMenu] = useState(false)
    let useCases = useContext(UseCasesContext)

    const formatDate = (input: string) => {
        const today = new Date(input);
        const yyyy = today.getFullYear();
        let mm = (today.getMonth() + 1).toString(); // Months start at 0!
        let dd = today.getDate().toString();

        if (dd.length == 1) dd = '0' + dd;
        if (mm.length == 1) mm = '0' + mm;

        return dd + '.' + mm + '.' + yyyy;
    }
    return (<tr>
        <td>{props.site.Name()}</td>
        <td>{props.site.Url()}</td>
        <td>{formatDate(props.site.CreatedAt())}</td>
        <td>
            <div className="dropdown">
                <button type="button" onClick={()=>{setShowMenu(!showMenu)}} className="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown" aria-expanded="false">
                    <DotsVertical />
                </button>
                <div className={showMenu?"dropdown-menu show":"dropdown-menu "} style={showMenu ? props.last ? { position: "absolute", inset: "auto auto 0px -20%", margin: "0px", transform: "translate3d(0px, -20.5px, 0px)" }: {position: "absolute", marginLeft: "-40%",}: {display:"none"}}>
                    <Link className="dropdown-item d-flex" to={"/domains/edit/"+id!+"/"+props.site.Id()}><Edit className="me-1" size={20} /> Редактировать</Link>
                    <a className="dropdown-item d-flex" href="#" onClick={(e)=>{e.preventDefault(); useCases?.siteUseCase.DeleteSite(props.site.Id()); setShowMenu(false)}}><Trash className="me-1" size={20} /> Удалить</a>
                </div>
            </div>
        </td>
    </tr>)
}
