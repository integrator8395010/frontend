import { useContext, useEffect, useState } from "react"
import { useNavigate, useParams } from "react-router-dom"
import { Plus, Trash } from "tabler-icons-react"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { Menu } from "../../components/menu"
import axios from "axios"
import { LeadFieldNamesList } from "../../../domain/lead/lead"
import { BitrixAssociation, BitrixDefaultValue } from "../../../domain/integration/types"


export const IntegrationSiteBitrix = () => {
    let { id, siteId } = useParams();
   
    const [bitrixFields, setBitrixFields] = useState<{ formLabel: string, title: string, type: string, items: { ID: string, VALUE: any }[] | null }[]>([])
    const navigate = useNavigate()
    const [form, setForm] = useState<{
        id: string,
        sendType: string,
        associations: { leadField: string, bitrixField: string }[],
        defaultValues: { field: string, value: any }[],
        responsible: number,
    }>({
        id: "",
        sendType: "",
        associations: [],
        defaultValues: [],
        responsible: 0,
    })
    let useCases = useContext(UseCasesContext)

    const integrations = useTypedSelector(({ integrations }) => {
        return integrations
    })

    const submit = () => {
        let associations: BitrixAssociation[] = []
        form.associations.forEach((association) => {
            associations.push(new BitrixAssociation(association.leadField, association.bitrixField))
        })

        let defaultValues: BitrixDefaultValue[] = []
        form.defaultValues.forEach((defaultValue) => {
            defaultValues.push(new BitrixDefaultValue(defaultValue.field, defaultValue.value))
        })
        if (form.id) {
            useCases?.integrationUseCase.UpdateBitrixIntegration(form.id, siteId!, form.responsible, form.sendType, associations, defaultValues, ()=>{{navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })}})
        } else {
            useCases?.integrationUseCase.CreateBitrixIntegration(siteId!, form.responsible, form.sendType, associations, defaultValues, ()=>{{navigate("/integration/site/edit/"+id!+"/"+siteId!, { replace: true })}})
        }
    }

    useEffect(() => {
        useCases?.integrationUseCase.ReadProjectIntegration(id!)
        useCases?.integrationUseCase.ReadIntegrationsOfSite(siteId!)
    }, [siteId])


    useEffect(() => {
        if (integrations?.projectIntegration?.BaseUrl() != "" && form.sendType != "") {
            axios.get(integrations?.projectIntegration?.BaseUrl() + "/" + "crm." + form.sendType + ".fields").then((response) => {
                if (response.data) {

                    let bitrixFields: {
                        formLabel: string;
                        title: string;
                        type: string;
                        items: { ID: string, VALUE: any }[] | null;
                    }[] = []

                    Object.keys(response.data.result).forEach((key: any) => {
                        if (key.indexOf("UF_") !== -1 || key.indexOf("UTM") !== -1 || key.indexOf("STAGE_ID") !== -1 || key.indexOf("CATEGORY_ID") !== -1) {
                            let field = response.data.result[key]
                            if (key.indexOf("UTM") !== -1 || key.indexOf("STAGE_ID") !== -1 || key.indexOf("CATEGORY_ID") !== -1) {
                                field.formLabel = field.title
                                field.title = key
                            }
                            bitrixFields.push(field)
                        }
                    })
                    setBitrixFields(bitrixFields)
                }
            })
        }
    }, [integrations?.projectIntegration, form.sendType])

    const entityTypeChange = (value:string) => { 
        if (form.sendType != value) {
            setForm({ ...form, sendType: value, associations: [], defaultValues: [], })
        }
        
    }
    const leadValueOptionClick = (index: number, value: string) => {
        let newValues = form.associations
        newValues[index].leadField = value

        setForm({ ...form, associations: [...newValues] })
    }

    const bitrixValueOptionClick = (index: number, value: string) => {
        let newValues = form.associations
        newValues[index].bitrixField = value

        setForm({ ...form, associations: [...newValues] })
    }

    const defaultValueOptionClick = (index: number, value: string) => {
        let newValues = form.defaultValues
        console.log(value)
        newValues[index].field = value

        setForm({ ...form, defaultValues: [...newValues] })
    }

    const defaultValueChangeValue = (index: number, value: any) => {
        let newValues = form.defaultValues
        let fieldType = getDefaultFieldType(index)
        switch (fieldType) {
            case "double":
                newValues[index].value = parseFloat(value)
                break;
            case "int":
                newValues[index].value = parseInt(value)
                break;
            default:
                newValues[index].value = value
        }
        

        setForm({ ...form, defaultValues: [...newValues] })
    }

    const getDefaultFieldType = (index: number) => {
        if (bitrixFields) {
            let field = bitrixFields.filter((field) => { return field.title == form.defaultValues[index].field })[0]
            if (!field) {
                return "text"
            }
            return field.type
        }
    }

    useEffect(() => {
        if (integrations?.bitrixIntegration) {
            let associations: { leadField: string, bitrixField: string }[] = [];
            let defaultValues: { field: string, value: any }[] = [];
            integrations.bitrixIntegration.Associations() && integrations.bitrixIntegration.Associations().forEach((item) => {
                associations.push({
                    leadField: item.LeadField(),
                    bitrixField: item.BitrixField(),
                })
            })
            integrations.bitrixIntegration.DefaultValues() && integrations.bitrixIntegration.DefaultValues().forEach((item) => {
                defaultValues.push({
                    field: item.Field(),
                    value: item.Value(),
                })
            })
            console.log(associations)
            setForm({
                ...form,
                id: integrations.bitrixIntegration.Id(),
                responsible: integrations.bitrixIntegration.Responsible(),
                sendType: integrations.bitrixIntegration.SendType(),
                associations: associations,
                defaultValues: defaultValues,
            })
        }
    }, [integrations?.bitrixIntegration])

    return (
        <div className="layout-page" >
            <Menu />
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Интеграции /</span> Интеграция Bitrix24</h4>
                        </div>
                        <div className="card">
                            <div className="row mx-1 mt-3">
                                <div className="col-sm-12 col-md-8 mt-4">
                                    <h5 className="mb-0">{form.id === "" ?"Редактирование":"Добавление"} интеграции Bitrix24</h5>
                                </div>
                                {form.id !== "" ?<div className="col-sm-12 col-md-4">
                                    <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                        <div className="dt-buttons py-3 mb-4">
                                            <button className="dt-button add-new btn btn-danger mb-3 mb-md-0" onClick={(e)=>{e.preventDefault();  useCases?.integrationUseCase.DeleteBitrixIntegrationOfSite(form.id, ()=>{navigate(-1)})}} >
                                                <span>Удалить интеграцию</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>:<></>}
                            </div>
                            <div className="card-body">
                                <div className="row align-items-end">
                                    <div className="col-md-6">
                                        <label className="form-label" htmlFor={"pipelineId"}>Тип сущности</label>
                                        <select value={form.sendType} onChange={(e) => { entityTypeChange(e.target.value) }} name={"sendType"} id={"sendType"} className="form-select" key={"pipelineId"}>
                                            <option disabled value="">{"Выберите тип сущности"}</option>
                                            <option value="lead">{"Лид"}</option>
                                            <option value="deal">{"Сделка"}</option>
                                        </select>
                                    </div>
                                    <div className="col-md-6">
                                        <label className="form-label" htmlFor={"responsible"}>Ответственный пользователь</label>
                                        <input type="number" className={"form-control"} name={"responsible"} placeholder={"Введите id ответственного"} value={form.responsible === 0 ? undefined : form.responsible} onChange={(e) => { setForm({ ...form, responsible: parseInt(e.target.value) }) }} />
                                    </div>
                                    <div className="row mx-1 mt-4">
                                        <div className="col-sm-12 col-md-4" style={{ alignItems: "center", display: "flex", }}>
                                            <h6>1.Передача полей из лида</h6>
                                        </div>
                                    </div>
                                    <div className="row">
                                        {form.associations && form.associations.map((association, index) => {
                                            return <><div className="row" style={{ position: "relative" }} key={"associations_" + index}>
                                                <div className="col-md-6">
                                                    <label className="form-label" htmlFor={"leadField_" + index}>Поле Лида</label>
                                                    <select onChange={(e) => { leadValueOptionClick(index, e.target.value) }} value={form.associations[index].leadField} name={"leadField_" + index} id={"leadField_" + index} className="form-select mb-3" key={"leadField_" + index}>
                                                        <option disabled value="">{"Выберите поле лида"}</option>
                                                        {LeadFieldNamesList.map((name) => {
                                                            return <option value={name}>{name}</option>
                                                        })}
                                                    </select>
                                                </div>
                                                <div className="col-md-6">
                                                    <label className="form-label" htmlFor={"leadField_" + index}>Поле в Bitrix</label>
                                                    <select onChange={(e) => { bitrixValueOptionClick(index, e.target.value) }} value={form.associations[index].bitrixField} name={"leadField_" + index} id={"leadField_" + index} className="form-select mb-3" key={"leadField_" + index}>
                                                        <option disabled value="">{"Выберите поле в bitrix"}</option>
                                                        {bitrixFields.map((field) => {
                                                            if (field.items || field.title === "STAGE_ID" || field.title === "CATEGORY_ID") {
                                                                return
                                                            }
                                                            return <option value={field.title}>{field.formLabel} ({field.title})</option>
                                                        })}
                                                    </select>
                                                    <Trash style={{ position: "absolute", right: 0, top: 30, color: "red" }} size={20} onClick={() => { setForm({ ...form, associations: [...form.associations.filter((field, i) => { return index !== i })] }) }} />
                                                </div>
                                            </div>
                                            </>
                                        })}
                                        <div className="col-sm-12 col-md-12 mb-4">
                                            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                                <div className="dt-buttons">
                                                    <button className="dt-button add-new btn btn-primary mb-3 mb-md-0" onClick={() => { setForm({ ...form, associations: [...form.associations, { leadField: "", bitrixField: "" }] }) }} ><span><Plus size={15} /> Поле лида</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="row mx-1 mt-5">
                                        <div className="col-sm-12 col-md-4" style={{ alignItems: "center", display: "flex", }}>
                                            <div className="col-md-12"><span className="fw-bold">2.Значения дефолтных полей </span><br />(эти значения не изменяются)</div>
                                        </div>
                                    </div>

                                    <div className="row">
                                        {form.defaultValues.map((defaultValue, index) => {
                                            return <><div className="row" style={{ position: "relative" }} key={"associations_" + index}>
                                                <div className="col-md-6">
                                                    <label className="form-label" htmlFor={"leadField_" + index}>Поле в Bitrix</label>
                                                    <select onChange={(e) => { defaultValueOptionClick(index, e.target.value) }} value={form.defaultValues[index].field} name={"leadField_" + index} id={"leadField_" + index} className="form-select mb-3" key={"leadField_" + index}>
                                                        <option disabled value="">{"Выберите поле в bitrix"}</option>
                                                        {bitrixFields.map((field) => {
                                                            return <option value={field.title} >{field.formLabel}</option>
                                                        })}
                                                    </select>
                                                    <Trash style={{ position: "absolute", right: 0, top: 30, color: "red" }} size={20} onClick={() => { setForm({ ...form, defaultValues: [...form.defaultValues.filter((field, i) => { return index !== i })] }) }} />
                                                </div>
                                                {defaultValue.field !== "" && bitrixFields.length > 0 ? bitrixFields.filter((field) => { return field.title === defaultValue.field}).length > 0 && bitrixFields.filter((field) => { return field.title === defaultValue.field})[0].items ?
                                                    <div className="col-md-6">
                                                        <label className="form-label" htmlFor={"leadField_" + index}>Значение поля</label>
                                                        <select onChange={(e) => { defaultValueChangeValue(index, e.target.value) }} value={form.defaultValues[index].value} name={"leadField_" + index} id={"leadField_" + index} className="form-select mb-3" key={"leadField_" + index}>
                                                            <option disabled value="">{"Значение поля"}</option>
                                                            {bitrixFields.length > 0 && bitrixFields.filter((field) => field.title === defaultValue.field)[0].items!.map((item) => {
                                                                return <option value={item.ID}>{item.VALUE}</option>
                                                            })}
                                                        </select>
                                                    </div> : <div className="col-md-6">
                                                        <label className="form-label" htmlFor={"leadField_" + index}>Значение поля</label>
                                                        <input type={getDefaultFieldType(index)} className={"form-control"} name={"field"} placeholder={"Введите значение поля"} value={defaultValue.value} onChange={(e) => { defaultValueChangeValue(index, e.target.value) }} />
                                                    </div> : <></>}
                                            </div>
                                            </>
                                        })}
                                        <div className="col-sm-12 col-md-12 mb-4">
                                            <div className="dt-action-buttons text-xl-end text-lg-start text-md-end text-start d-flex align-items-center justify-content-md-end justify-content-center flex-wrap me-1">
                                                <div className="dt-buttons">
                                                    <button className="dt-button add-new btn btn-primary mb-3 mb-md-0" onClick={() => { setForm({ ...form, defaultValues: [...form.defaultValues, { field: "", value: "" }] }) }} ><span><Plus size={15} /> Дефолтное поле</span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr />
                                    <div className="d-flex justify-content-end">
                                        {<button key="submit" className={"btn btn-primary d-grid"} onClick={(e) => { e.preventDefault(); submit() }}>Сохранить</button>}
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
