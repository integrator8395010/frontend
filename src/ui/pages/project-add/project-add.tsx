import { useContext, useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import { UseCasesContext } from "../../../context/useCases"
import { useTypedSelector } from "../../../hooks/useTypedSelector"
import { FiledType, Form } from "../../components/form"

export const ProjectAdd = () => {
    let useCases = useContext(UseCasesContext)
    const navigate = useNavigate();
    const [form, setForm] = useState<{
        name: string,
        file: File | null,
    }>({
        name: "",
        file: null,
    })

    const projects = useTypedSelector(({ projects }) => {
        return projects
    })

    const submit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault()
        if (form.file !== null) {
            useCases?.projectsUseCase.CreateProject(form.name!, form.file!, () => {navigate(-1)})
        }
    }

    const updateForm = (name: string, value: any) => {
        setForm({
            ...form,
            [name]: value,
        })
    }

    useEffect(()=>{
        console.log(form)
    },[form])

    return (
        <div className="layout-page" >
            {/*<Menu />*/}
            <div className="content-wrapper" >
                <div className="container-xxl flex-grow-1 container-p-y">
                    <div className="row mx-1">
                        <div className="col-sm-12 col-md-12">
                            <h4 className="fw-bold py-3 mb-4"><span className="text-muted fw-light">Проекты /</span> Добавить проект</h4>
                        </div>
                        <div className="card">
                            <div className="card-header d-flex justify-content-between align-items-center">
                                <h5 className="mb-0">Добавление проекта</h5>
                            </div>
                            <div className="card-body">
                                <Form
                                    state={{
                                        loading: false,
                                        error: "",
                                    }}

                                    submit={submit}

                                    fields={[
                                        {
                                            name: "name",
                                            title: "Название проекта",
                                            placeholder: "Введите название проекта",
                                            type: FiledType.Text,
                                        },
                                        {
                                            name: "file",
                                            title: "Лого проекта",
                                            placeholder: "Приложите лого проекта",
                                            type: FiledType.File,
                                            accept: "image/gif, image/jpeg, image/png"
                                        },
                                    ]}
                                    btnSmall={true}
                                    submitBtnTitle={"Сохранить"}
                                    updateForm={updateForm}
                                />
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
